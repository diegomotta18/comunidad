<?php
namespace Tests\Feature;

use App\Mail\EmailVerification;
use App\Models\Address;
use App\Models\Person;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ProfileUserTest extends  TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_update_data_profile_user()
    {
       $user = $this->defaultUser();

        $this->actingAs($user);
        $persona = $user->person()->first();
       $response =  $this->put(route('person.update_perfil', $persona->id),[
            'email' => "fabian@gmail.com",
            'nombres'=> $persona->first_name,
            'apellido'=> $persona->last_name,
            'documento'=> $persona->identification,
            'fecha_de_nacimiento' => $persona->identification,
            'telefono' => $persona->telephone,
            'extranjero' => "on",
            'sexo' => 'Masculino'
        ]);
        $response->assertStatus(302);
    }

    public function test_register_user(){

        $this->createAddress();
        $user = User::create([
            'role' => "user",
            'name' => 'Diego',
            'email' =>'diego.mas.001@gmail.com',
            'password' => bcrypt('secret#*'),
        ]);

        $person =Person::create([
            'first_name' => 'Diego',
            'last_name' => 'Motta',
            'identification' => '354563518',
            'birthday' => Carbon::now(),
            'telephone' => 15432213,
            'sex' =>  'Masculino',
            'user_id' => $user->id,
            'email' => $user->email,
            'extranjero' => true
        ]);

        $nacionalidad = 'Argentino';
        if($nacionalidad == 'Argentino'){
            $address = Address::create([
                'country_id' => 1,
                'state_id' => 1,
                'district_id' => 1,
            ]);
            $address->person()->save($person);
        }
        elseif($nacionalidad == 'Extranjero'){
            $person->extranjero = true;
            $person->save();
        }

        $user->registration_token = str_random(80);
        $user->name = $person->first_name. " ". $person->last_name;
        $user->person_id = $person->id;
        $user->save();
        $user->person()->save($person);

        $this->assertDatabaseHas('users',$user->first()->toArray());


    }
}
