<?php

/**
 * Created by PhpStorm.
 * User: diego
 * Date: 2/10/17
 * Time: 23:19
 */

namespace Tests;

use App\Models\Person;
use App\User;
use Carbon\Carbon;


trait TestsHelper
{

    protected $defaultUser;


    public function defaultUser(array $attributes = [])
    {
        if ($this->defaultUser) {
            return $this->defaultUser;
        }


        $user = factory(User::class)->create([
            'email' => 'user@user.com',
            'name' => 'user',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);

        $knownDate = Carbon::create(2001, 5, 21, 12);          // create testing date
        Carbon::setTestNow($knownDate);
        $person =Person::create([
            'first_name' => 'pepe',
            'last_name' => 'argento',
            'identification' => 45678765,
            'birthday' => Carbon::getTestNow(),
            'telephone' => 14543212312,
            'sex' =>  'Masculino',
            'user_id' => $user->id,
            'email' => $user->email,
            'extranjero' => true,
        ]);
        $user->person()->save($person);
        $user->person_id = $person->id;
        $user->save();
        return $this->defaultUser = $user;
    }

    public function createUser(array $attributes = [])
    {
        $user = factory(User::class)->create($attributes);
        return $user;
    }

    public function createAddress(){
        \App\Models\Country::create(['name'=>'Argentina']);

        \App\Models\State::create(['country_id'=>1,'name'=>'Buenos Aires']);//1
        \App\Models\State::create(['country_id'=>1,'name'=>'Catamarca']);//2
        \App\Models\State::create(['country_id'=>1,'name'=>'Chaco']);//3
        \App\Models\State::create(['country_id'=>1,'name'=>'Chubut']);//4
        \App\Models\State::create(['country_id'=>1,'name'=>'Córdoba']);//5
        \App\Models\State::create(['country_id'=>1,'name'=>'Corrientes']);//6
        \App\Models\State::create(['country_id'=>1,'name'=>'Entre Rios']);//7
        \App\Models\State::create(['country_id'=>1,'name'=>'Formosa']);//8
        \App\Models\State::create(['country_id'=>1,'name'=>'Jujuy']);//9
        \App\Models\State::create(['country_id'=>1,'name'=>'La Pampa']);//10
        \App\Models\State::create(['country_id'=>1,'name'=>'La Rioja']);//11
        \App\Models\State::create(['country_id'=>1,'name'=>'Mendoza']);//12
        \App\Models\State::create(['country_id'=>1,'name'=>'Misiones']);//13
        \App\Models\State::create(['country_id'=>1,'name'=>'Neuquén']);//14

        \App\Models\State::create(['country_id'=>1,'name'=>'Río Negro']);//15
        \App\Models\State::create(['country_id'=>1,'name'=>'Salta']);//16
        \App\Models\State::create(['country_id'=>1,'name'=>'San Juan']);//17
        \App\Models\State::create(['country_id'=>1,'name'=>'San Luis']);//18
        \App\Models\State::create(['country_id'=>1,'name'=>'Santa Cruz']);//19
        \App\Models\State::create(['country_id'=>1,'name'=>'Santa Fe']);//20
        \App\Models\State::create(['country_id'=>1,'name'=>'Santiago del Estero']);//21
        \App\Models\State::create(['country_id'=>1,'name'=>'Tierra del Fuego']);//22
        \App\Models\State::create(['country_id'=>1,'name'=>'Tucumán']);//23

        \App\Models\District::create(['state_id' => 1, 'name'  => 'Capital Federal']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Almirante Brown']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Rodriguez']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Presidente Peron']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Vicente Lopez']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Nicolas']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Berazategui']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Florencio Varela']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Merlo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General San Martin']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Quilmes']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tigre']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Moreno']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Miguel']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Jose Clemente Paz']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'La Matanza']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tres De Febrero']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Malvinas Argentinas']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Ezeiza']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Lomas De Zamora']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Ituzaingo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Fernando']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Moron']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Hurlingham']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tres Lomas']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Villa Gesell']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'De La Costa']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pinamar']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Punta Indio']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Esteban Echeverria']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Zarate']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Hipolito Yrigoyen']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Villarino']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Viamonte']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tres Arroyos']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Trenque Lauquen']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tornquist']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tordillo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tapalque']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Tandil']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Suipacha']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Vicente']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Pedro']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Monte']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Isidro']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Cayetano']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Antonio De Areco']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'San Andres De Giles']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Salto']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Salliquelo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Saladillo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Saavedra']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Roque Perez']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Rojas']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Rivadavia']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Rauch']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Ramallo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Pueyrredon']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Puan']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pilar']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pila']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pergamino']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pellegrini']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Pehuajo']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Olavarria']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Necochea']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Navarro']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Monte Hermoso']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Mercedes']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Marcos Paz']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Mar Chiquita']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Maipu']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Magdalena']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Lujan']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Lobos']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Loberia']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Lincoln']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Leandro N Alem']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Las Heras']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Las Flores']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Laprida']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Lanus']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Lamadrid']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'La Plata']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Junin']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Guamini']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Villegas']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Pinto']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Paz']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Madariaga']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Lavalle']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Guido']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Belgrano']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'General Arenales']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Exaltacion De La Cruz']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Escobar']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Ensenada']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Dolores']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Daireaux']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Coronel Suarez']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Coronel Pringles']);
        \App\Models\District::create(['state_id' => 1, 'name'  => 'Coronel Dorrego']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Coronel Rosales']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Colon']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Chivilcoy']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Chascomus']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Chacabuco']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Castelli']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Patagones']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Carmen De Areco']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Carlos Tejedor']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Carlos Casares']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Capitan Sarmiento']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Campana']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Coronel Brandsen']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Bragado']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Bolivar']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Berisso']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Benito Juarez']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Bartolome Mitre']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Baradero']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Balcarce']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Bahia Blanca']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Azul']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Ayacucho']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Avellaneda']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Florentino Ameghino']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'General Alvear']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'General Alvarado']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Alberti']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Adolfo Gonzales Chaves']);
        \App\Models\District::create(['state_id' => 1, 'name' => 'Adolfo Alsina']);
        \App\Models\District::create(['state_id' => 1, 'name' => '9 De Julio']);
        \App\Models\District::create(['state_id' => 1, 'name' => '25 De Mayo']);

        \App\Models\District::create(['state_id' => 2, 'name'  => 'Valle Viejo']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Tinogasta' ]);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Santa Rosa']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Santa Maria']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Poman']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Paclin' ]);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'La Paz' ]);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Fray Mamerto Esquiu' ]);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'El Alto']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Capital']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Capayan' ]);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Belen']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Antofagasta De La Sierra']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Andalgala']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Ancasti']);
        \App\Models\District::create(['state_id' => 2, 'name'  => 'Ambato']);

        \App\Models\District::create(['state_id' => 3, 'name'  => 'Tapenaga']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Sargento Cabral']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'San Lorenzo']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'San Fernando']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Quitilipi']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Presidente De La Plaza']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'O Higgins']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Mayor Luis J Fontana']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Maipu']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Libertad']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Libertador Gral San Martin']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Independencia']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'General Guemes']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'General Donovan']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'General Belgrano']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Fray Justo Santa Maria De Oro']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Comandante Fernandez']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Chacabuco']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Bermejo']);
        \App\Models\District::create(['state_id' => 3, 'name'  => 'Almirante Brown']);
        \App\Models\District::create(['state_id' => 3, 'name'  => '9 De Julio']);
        \App\Models\District::create(['state_id' => 3, 'name'  => '25 De Mayo']);
        \App\Models\District::create(['state_id' => 3, 'name'  => '12 De Octubre']);
        \App\Models\District::create(['state_id' => 3, 'name'  => '1 De Mayo']);

        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Telsen']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Tehuelches']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Sarmiento']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Rio Senguer']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Rawson']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Paso De Indios']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Martires']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Gastre']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Gaiman']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Futaleufu']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Florentino Ameghino']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Escalante']);
        \App\Models\District::create(['state_id' => 4, 'name'  =>  'Cushamen']);

        \App\Models\District::create(['state_id' => 5, 'name' => 'Union']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Tulumba']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Totoral']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Tercero Arriba']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Sobremonte']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Santa Maria']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'San Justo']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'San Javier']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'San Alberto']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Rio Segundo']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Rio Seco']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Rio Primero']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Rio Cuarto']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Punilla']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Pocho']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Minas']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Marcos Juarez']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Juarez Celman']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Ischilin']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'General San Martin']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'General Roca']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Cruz Del Eje']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Colon']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Capital']);
        \App\Models\District::create(['state_id' => 5, 'name' => 'Calamuchita']);

        \App\Models\District::create(['state_id' => 6, 'name'  => 'Sauce']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Santo Tome']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'San Roque']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'San Miguel']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'San Martin']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'San Luis Del Palmar']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'San Cosme']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Saladas']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Paso De Los Libres']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Monte Caseros']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Mercedes']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Mburucuya']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Lavalle']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Ituzaingo']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Itati']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Goya']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'General Paz']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'General Alvear']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Esquina']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Empedrado']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Curuzu Cuatia']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Concepcion']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Capital']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Beron De Astrada']);
        \App\Models\District::create(['state_id' => 6, 'name'  => 'Bella Vista']);

        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Villaguay']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Victoria']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Uruguay']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Tala']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Parana']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Nogoya']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'La Paz']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Islas Del Ibicuy']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Gualeguaychu']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Gualeguay']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Feliciano']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Federal']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Federacion']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Diamante']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Concordia']);
        \App\Models\District::create(['state_id' => 7, 'name'  =>  'Colon']);

        \App\Models\District::create(['state_id' => 8, 'name' => 'Ramon Lista']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Pirane']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Pilcomayo']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Pilagas']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Matacos']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Laishi']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Formosa']);
        \App\Models\District::create(['state_id' => 8, 'name' => 'Bermejo']);

        \App\Models\District::create(['state_id' => 9, 'name'  => 'Yavi']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Valle Grande']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Tumbaya']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Tilcara']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Susques']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Santa Catalina']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Santa Barbara']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'San Pedro']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'San Antonio']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Rinconada']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Palpala']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Ledesma']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Humahuaca']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'El Carmen']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Dr Manuel Belgrano']);
        \App\Models\District::create(['state_id' => 9, 'name'  => 'Cochinoca']);

        \App\Models\District::create(['state_id' => 10, 'name'  =>'Utracan']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Trenel']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Toay']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Realico']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Rancul']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Quemu Quemu']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Puelen']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Maraco']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Loventue']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Limay Mahuida']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Lihuel Calel']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Hucal']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Guatrache']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Curaco']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Conhelo']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Chical Co']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Chapaleufu']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Chalileo']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Catrilo']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Capital']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Caleu Caleu']);
        \App\Models\District::create(['state_id' => 10, 'name'  =>'Atreuco']);

        \App\Models\District::create(['state_id' => 11, 'name'  => 'Vinchina']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General Sarmiento']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Sanagasta']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'San Blas De Los Sauces']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Independencia']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General San Martin']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General Ocampo']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General Lamadrid']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General Juan Facundo Quiroga']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'General Belgrano']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Gobernador Gordillo']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Famatina']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Coronel Felipe Varela']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Chilecito']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Chamical']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Castro Barros']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Capital']);
        \App\Models\District::create(['state_id' => 11, 'name'  => 'Arauco']);

        \App\Models\District::create(['state_id' => 12, 'name' => 'Tupungato']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Tunuyan']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Santa Rosa']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'San Rafael']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'San Martin']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'San Carlos']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Rivadavia']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Malargue']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Maipu']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Lujan De Cuyo']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Lavalle']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Las Heras']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'La Paz']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Junin']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Guaymallen']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'General Alvear']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Godoy Cruz']);
        \App\Models\District::create(['state_id' => 12, 'name' => 'Capital']);



        \App\Models\District::create(['state_id' => 14, 'name'  => 'Zapala']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Picunches']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Picun Leufu']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Pehuenches']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Minas']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Los Lagos']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Loncopue']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Lacar']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Huiliches']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Confluencia']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Collon Cura']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Chos Malal']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Catan Lil']);
        \App\Models\District::create(['state_id' => 14, 'name'  => 'Alumine']);

        \App\Models\District::create(['state_id' => 15, 'name'  => 'Valcheta']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'San Antonio']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Pilcaniyeu']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Pichi Mahuida']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'General Roca']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'El Cuy']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Conesa']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Bariloche']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Avellaneda']);
        \App\Models\District::create(['state_id' => 15, 'name'  => 'Adolfo Alsina']);
        \App\Models\District::create(['state_id' => 15, 'name'  => '9 De Julio']);
        \App\Models\District::create(['state_id' => 15, 'name'  => '25 De Mayo']);
        \App\Models\District::create(['state_id' => 15, 'name'  =>  'Viedma']);

        \App\Models\District::create(['state_id' => 16, 'name'  => 'La Candelaria']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Santa Victoria']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'San Carlos']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Rosario De Lerma']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Rosario De La Frontera']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Rivadavia']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Oran']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Molinos']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Metan']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Los Andes']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'La Poma']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'La Caldera']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Iruya']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Guachipas']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'General Jose De San Martin']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'General Martin M Guemes']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Chicoana']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Cerrillos']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Capital']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Cafayate']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Cachi']);
        \App\Models\District::create(['state_id' => 16, 'name'  => 'Anta']);


        \App\Models\District::create(['state_id' => 17, 'name'  => 'Zonda']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Valle Fertil']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Ullum']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Sarmiento']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Santa Lucia']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'San Martin']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Rivadavia']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Rawson']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Pocito']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Jachal']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Iglesia']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Chimbas']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Caucete']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Capital']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Calingasta']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Angaco']);
        \App\Models\District::create(['state_id' => 17, 'name'  => 'Albardon']);
        \App\Models\District::create(['state_id' => 17, 'name'  => '9 De Julio']);
        \App\Models\District::create(['state_id' => 17, 'name'  => '25 De Mayo']);

        \App\Models\District::create(['state_id' => 18, 'name'  => 'Libertador Gral San Martin']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'La Capital']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Junin']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'General Pedernera']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Gobernador Dupuy']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Coronel Pringles']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Chacabuco']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Belgrano']);
        \App\Models\District::create(['state_id' => 18, 'name'  => 'Ayacucho']);

        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Rio Gallegos']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Rio Chico']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Magallanes']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Lago Buenos Aires']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Lago Argentino']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Guer Aike']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Deseado']);
        \App\Models\District::create(['state_id' => 19, 'name'  =>  'Corpen Aike']);

        \App\Models\District::create(['state_id' => 20, 'name'  => 'Vera']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Martin']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Lorenzo']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Justo']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Jeronimo']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Javier']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'San Cristobal']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Rosario']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'General Obligado']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'General Lopez']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Las Colonias']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'La Capital']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Iriondo']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Garay']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Constitucion']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Castellanos']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Caseros']);
        \App\Models\District::create(['state_id' => 20, 'name'  => 'Belgrano']);
        \App\Models\District::create(['state_id' => 20, 'name'  => '9 De Julio']);

        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Mitre']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Silipica']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Sarmiento']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'General San Martin']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Salavina']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Robles']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Rivadavia']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Rio Hondo']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Quebrachos']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Pellegrini']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Ojo De Agua']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Moreno']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Loreto']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Brigadier Juan F Ibarra']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Jimenez']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Guasayan']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'General Taboada']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Figueroa']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Copo']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Choya']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Capital']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Belgrano']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Banda']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Avellaneda']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Atamisqui']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Alberdi']);
        \App\Models\District::create(['state_id' => 21, 'name'  =>  'Aguirre']);

        \App\Models\District::create(['state_id' => 22, 'name' =>'Ushuaia']);
        \App\Models\District::create(['state_id' => 22, 'name' =>'Rio Grande']);
        \App\Models\District::create(['state_id' => 22, 'name' =>'Malvinas']);
        \App\Models\District::create(['state_id' => 22, 'name' =>'Antartida']);

        \App\Models\District::create(['state_id' => 23, 'name' => 'Yerba Buena']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Trancas']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Tafi Viejo']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Tafi Del Valle']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Simoca']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Rio Chico']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Monteros']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Lules']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Leales']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'La Cocha']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Juan B Alberdi']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Graneros']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Famailla']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Cruz Alta']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Chicligasta']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Capital']);
        \App\Models\District::create(['state_id' => 23, 'name' => 'Burruyacu']);
    }

}