<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 19/10/17
 * Time: 09:54
 */

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CreateUserTest extends DuskTestCase
{

    public function test_a_admin_can_create_user()
    {
        $user = $this->godUser();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visitRoute('users.index')
                ->assertSee('Usuarios')
                ->click('#nuevo-user')
                ->assertRouteIs('users.create');
        });

    }
}
