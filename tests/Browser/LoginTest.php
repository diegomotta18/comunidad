<?php

namespace Tests\Browser;

use App\Models\Person;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {

        $user = factory(User::class)->create([
            'email' => 'root@root.com',
            'name' => 'root',
            'role' => 'user',
            'password' => bcrypt('root')
        ]);



        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'root')
                ->press('Ingresar')
                ->assertPathIs('/adminhome');
        });
    }
}
