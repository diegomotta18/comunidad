    /**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadProductsTable  = function () {
        var table = $('#products').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processsing mode.
            "order": [], //Initial no order.
            "ajax": "/user/products_clt",
            "columns": [
                { "render": function (data, type, JsonResultRow, meta) {
                    var img = JsonResultRow.avatar;
                    return '<img width="100px" src="/storage/'+ img + '" />';
                    //return '<img src="/storage'+JsonResultRow.avatar.replace('public/','')+'">';
                    //return '<img width="100px" src="/storage/zhjxeRjhgR2WW0BygFfrGkXSi7vo85oEe9CTQ9Rc.png">';
                }},

                {data: 'nombre'},
                {data: 'category.nombre'},

                {data: 'precio_descontado'},
                {data: 'precio'},

                {data: 'total'},

                {data: 'action', name: 'action', orderable: false, searchable: false, width: '20%'}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });
        $("#products tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/products/"+data.id+"/show" ;
        });


        $("#products tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            console.log(data.id);
            $("#frmDeleteProducto #id").val(data.id);

        });

        $("#products tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/products/"+data.id+"/edit";
        });

    }
    loadProductsTable();


    $('#frmDeleteProducto #btn_eliminar_producto').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'delete',
            url: '/products/'+ $('#frmDeleteProducto #id').val(),
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (dataObj) {
                console.log(dataObj);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.success("Se ha eliminado el producto");
            loadProductsTable();

        });
    });


});
/**
 * Created by diego on 29/8/17.
 */
