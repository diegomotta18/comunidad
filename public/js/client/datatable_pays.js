/**
 * Created by diego on 1/9/17.
 */
/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadPaysTable  = function () {
        var table = $('#pays').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processsing mode.
            "order": [], //Initial no order.
            "ajax": "/pays",
            "columns": [

                { "render": function (data, type, JsonResultRow, meta) {
                    if (JsonResultRow.checkin){
                        return '<span style="font-size: 50px;color: #5cb85c;text-align: center;" class="glyphicon glyphicon-ok-circle"></span>';

                    }else{
                        return '<span style="font-size: 50px;color: #f8ac59;text-align: center;" class="glyphicon glyphicon-time"></span>';

                    }
                }},{data: 'order_id'},
                { "render": function (data, type, JsonResultRow, meta) {
                    return  '<p>'+JsonResultRow.user.email + '</p>'+
                            '<p>' +JsonResultRow.user.first_name + " "+JsonResultRow.user.last_name + '</p>'+
                            '<p>'+ "D.N.I " +JsonResultRow.user.identification + '</p>'

                }},
                {data: 'product.nombre'},
                { "render": function (data, type, JsonResultRow, meta) {
                    return  "$ "+ JsonResultRow.unit_price
                }},
                {data: 'quantity'},
                { "render": function (data, type, JsonResultRow, meta) {
                    return  "$ "+ JsonResultRow.total_amount;
                }},
                { "render": function (data, type, JsonResultRow, meta) {

                    switch(JsonResultRow.pay_status) {
                        case 'aprobado':
                            return '<span class="label label-success">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'pendiente':
                            return '<span class="label label-warning">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'cancelado':
                            return '<span class="label label-danger">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'en proceso':
                            return '<span class="label label-primary">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'en mediación':
                            return '<span class="label label-info">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'devuelto':
                            return '<span class="label label-default">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'rechazado':
                            return '<span class="label" style="background-color:red;">'+JsonResultRow.pay_status+'</span>';
                            break;
                        case 'contracargo':
                            return '<span class="label" style="background-color:#777;">'+JsonResultRow.pay_status+'</span>';
                            break;
                    }
                }},

                {data: 'created_at'},

                { "render": function (data, type, JsonResultRow, meta) {
                    console.log(JsonResultRow.pay_status);
                    if (JsonResultRow.pay_status === 'aprobado' && !    (JsonResultRow.checkin)){
                        return   '<div class="btn-group" role="group" aria-label="...">'+
                            '<a type="button"  data-toggle="modal" data-target="#modalDelete"   class="btn btn-md btn-info check"><i class="fa fa-check"></i> </a></div>';
                    }else{
                        return   '<div class="btn-group" role="group" aria-label="...">'+
                            '<a disabled="true" type="button" class="btn btn-md btn-info"><i class="fa fa fa-check"></i> </a></div>';
                    }

                }},

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });

        $("#pays tbody").on("click", "a.check", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmCheckin #id").val(data.id);
            $("#frmCheckin #pago_id").val(data.pago_id);

            $("#frmCheckin #nombre").val(data.user.first_name+ " "+ data.user.last_name);
            $("#frmCheckin #identificacion").val(data.user.identification);
            $("#frmCheckin #orden").val(data.order_id);
            $("#frmCheckin #producto").val(data.product.nombre);
            $("#frmCheckin #total").val("$ "+data.total_amount);
            $("#frmCheckin #fecha").val(data.created_at);

        });


    }
    loadPaysTable();

    $('#frmCheckin #btn_check').click(function (e) {
        e.preventDefault();
        console.log($("#frmCheckin #id").val());
        $.ajax({
            method: 'post',
            url: '/pay/checkin/'+$("#frmCheckin #id").val(),
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (error) {
                console.log(error);
            },
        }).done( function (data) {
                // toastr.options.timeOut = 5000;
                // toastr.success('Se ha realizar el check-in del descuento con orden nº '+$("#frmCheckin #orden").val());
                loadPaysTable();
            }
        );
    });
});
/**
 * Created by diego on 6/9/17.
 */
