/**
 * Created by diego on 23/5/17.
 */
app.factory('StockServ', ['$q', '$filter', '$timeout', '$http', 'api', function ($q, $filter, $timeout, $http, api) {

    //this would be the service to call your server, a standard bridge between your model an $http

    // the database (normally on your server)
    var stocks = [];

    function getStocks() {
        return ($http({
                url: api + 'adm/stocks/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };


    stocks = getStocks();

    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    function getPage(start, number, params) {
        var deferred = $q.defer();
        var output = [];
        stocks.then(function (stock) {
            angular.forEach(stock, function (value, key) {
                output.push(value);
            });
            var filtered = params.search.predicateObject ? $filter('filter')(output, params.search.predicateObject) : stocks;
            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }

            if (params.search.predicateObject) {
                var result = filtered.slice(start, start + number);

                $timeout(function () {
                    //note, the server passes the information about the  data set size
                    deferred.resolve({
                        data: result,
                        all: filtered,
                        items: result.length,
                        numberOfPages: Math.ceil(filtered.length / number)
                    });
                }, 1500);
            }
            else {
                filtered.then(function (data) {

                    result = data.slice(start, start + number);
                    $timeout(function () {
                        //note, the server passes the information about the data set size
                        deferred.resolve({
                            data: result,
                            all: data,
                            items: result.length,
                            numberOfPages: Math.ceil(data.length / number)
                        });
                    }, 1500);
                });
            }

        });

        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}]);

app.factory('ProductServ', ['$q', '$filter', '$timeout', '$http', 'api', function ($q, $filter, $timeout, $http, api) {

    //this would be the service to call your server, a standard bridge between your model an $http

    // the database (normally on your server)
    var products = [];

    function getProducts() {

        return ($http({
                url: api + 'adm/products/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );
    }


    products = getProducts();

    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    function getPage(start, number, params) {
        var deferred = $q.defer();
        var output = [];
        console.log(products);
        products.then(function (product) {
            angular.forEach(product, function (value, key) {
                output.push(value);
            });
            var filtered = params.search.predicateObject ? $filter('filter')(output, params.search.predicateObject) : products;
            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }

            if (params.search.predicateObject) {
                var result = filtered.slice(start, start + number);
                $timeout(function () {
                    //note, the server passes the information about the  data set size
                    deferred.resolve({
                        data: result,
                        all: filtered,
                        items: result.length,
                        numberOfPages: Math.ceil(filtered.length / number)
                    });
                }, 1500);
            }
            else {

                filtered.then(function (data) {
                    result = data.slice(start, start + number);
                    $timeout(function () {
                        //note, the server passes the information about the data set size
                        deferred.resolve({
                            data: result,
                            all: data,
                            items: result.length,
                            numberOfPages: Math.ceil(data.length / number)
                        });
                    }, 1500);
                });
            }

        });

        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}]);


app.factory('CompanyServ', ['$q', '$filter', '$timeout', '$http', 'api', function ($q, $filter, $timeout, $http, api) {

    //this would be the service to call your server, a standard bridge between your model an $http

    // the database (normally on your server)
    var companies = [];

    function getCompanies() {
        return ($http({
                url: api + 'adm/company/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };


    companies = getCompanies();

    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    function getPage(start, number, params) {
        var deferred = $q.defer();
        var output = [];
        companies.then(function (company) {
            angular.forEach(company, function (value, key) {
                output.push(value);
            });
            var filtered = params.search.predicateObject ? $filter('filter')(output, params.search.predicateObject) : companies;
            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }

            if (params.search.predicateObject) {
                var result = filtered.slice(start, start + number);

                $timeout(function () {
                    //note, the server passes the information about the  data set size
                    deferred.resolve({
                        data: result,
                        all: filtered,
                        items: result.length,
                        numberOfPages: Math.ceil(filtered.length / number)
                    });
                }, 1500);
            }
            else {
                filtered.then(function (data) {
                    result = data.slice(start, start + number);
                    $timeout(function () {
                        //note, the server passes the information about the data set size
                        deferred.resolve({
                            data: result,
                            all: data,
                            items: result.length,
                            numberOfPages: Math.ceil(data.length / number)
                        });
                    }, 1500);
                });
            }

        });

        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}]);

app.factory('PayServ', ['$q', '$filter', '$timeout', '$http', 'api', function ($q, $filter, $timeout, $http, api) {

    //this would be the service to call your server, a standard bridge between your model an $http

    // the database (normally on your server)
    var pays = [];

    function getPays() {
        return ($http({
                url: api + 'adm/pays/all',
                method: "GET",

            }).then(function (data, status, headers, config) {

                return data.data;

            }, function (err) {
                error = err;
            })
        );

    }

    pays = getPays();

    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    function getPage(start, number, params) {
        var deferred = $q.defer();
        var output = [];
        pays.then(function (company) {
            angular.forEach(company, function (value, key) {
                output.push(value);
            });
            var filtered = params.search.predicateObject ? $filter('filter')(output, params.search.predicateObject) : pays;
            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }

            if (params.search.predicateObject) {
                var result = filtered.slice(start, start + number);

                $timeout(function () {
                    //note, the server passes the information about the  data set size
                    deferred.resolve({
                        data: result,
                        all: filtered,
                        items: result.length,
                        numberOfPages: Math.ceil(filtered.length / number)
                    });
                }, 1500);
            }
            else {
                filtered.then(function (data) {
                    console.log(data);
                    result = data.slice(start, start + number);
                    $timeout(function () {
                        //note, the server passes the information about the data set size
                        deferred.resolve({
                            data: result,
                            all: data,
                            items: result.length,
                            numberOfPages: Math.ceil(data.length / number)
                        });
                    }, 1500);
                });
            }

        });

        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}]);



