app.factory('AddressFact', ['$q', '$http', 'api', function ($q, $http, api) {
    var dataFactory = {};


    dataFactory.getCountries = function () {
        return ($http({
                url: api + '/countries',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );
    };
    dataFactory.getStates = function (id) {
        return ($http({
                url: api  + "states/" + id,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getDistricts = function (id) {
        return ($http({
                url: api + 'districs/' + id,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };
    dataFactory.getTowns = function (id) {
        return ($http({
                url: api + 'towns/' + id,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    return dataFactory;
}]);

