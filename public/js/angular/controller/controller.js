/**
 * Created by diego on 23/5/17.
 */
app.controller('BuyCont', ['$scope', function ($scope) {

    $scope.calculateDiscount = function (percent, price) {
        porcentage = percent / 100;
        $scope.price_discount = price - (price * porcentage);
    };

    $scope.changeValuesFormEditBuy = function (buy) {
        $scope.precio_total = buy.price;
        $scope.descuento = buy.percent;
        $scope.precio_descontado = buy.price_discount;

    }
}]);
app.controller('QuestionCont', ['$scope', function ($scope) {
    $scope.formCreate = true;
    $scope.formUpdate = false;
    $scope.showFormUpdate = function () {
        $scope.formCreate = false;
        $scope.formUpdate = true;
    };

    $scope.cancelFormUpdate = function () {
        $scope.formCreate = true;
        $scope.formUpdate = false;
    };

    $scope.items = [];

    $scope.add = function () {
        $scope.items.push({
            option: ""
        });
    };


    $scope.init = function () {
        $scope.cancelFormUpdate();
    };

    $scope.init();
}]);

app.controller('AddressCont', ['$scope', 'AddressFact', '$filter', function ($scope, AddressFact) {
    $scope.countries = [];
    $scope.states = [];
    $scope.districts = [];
    $scope.towns = [];
    $scope.country = {id: ''};
    $scope.state = {id: ''};
    $scope.district = {id: ''};


    $scope.changeCountry = function (country_id, state_id, district_id) {
        $scope.country.id = country_id;
        $scope.getStates(country_id);
        $scope.state.id = state_id;
        $scope.getDistricts(state_id);
        $scope.district.id = district_id;


    }
    $scope.getCountries = function () {

        AddressFact.getCountries().then(function (data) {
            $scope.countries = data;
        });
    }

    $scope.getStates = function (id) {
        AddressFact.getStates(id).then(function (data) {
            $scope.states = data;
        });
    }


    $scope.getDistricts = function (id) {
        AddressFact.getDistricts(id).then(function (data) {
            $scope.districts = data;
        });
    }

    $scope.getTowns = function (id) {
        AddressFact.getTowns(id).then(function (data) {
            $scope.towns = data;
        });
    }
    $scope.init = function () {
        $scope.getCountries();
    }

    $scope.init();

}]);

app.directive('stFilteredCollection', function () {
    return {
        require: '^stTable',
        link: function (scope, element, attr, ctrl) {
            scope.$watch(ctrl.getFilteredCollection, function (val) {
                scope.filteredCollection = val;
            })
        }
    }
});
app.filter('strReplace', function () {
    return function (input, from, to) {
        input = input || '';
        from = from || '';
        to = to || '';
        return input.replace(new RegExp(from, 'g'), to);
    };
});

app.directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
});

app.directive('stSelectDistinct', [function () {
    return {
        restrict: 'E',
        require: '^stTable',
        scope: {
            collection: '=',
            predicate: '@',
            predicateExpression: '='
        },
        template: '<select ng-model="selectedOption" ng-change="optionChanged(selectedOption)" ng-options="opt for opt in distinctItems"></select>',
        link: function (scope, element, attr, table) {
            var getPredicate = function () {
                var predicate = scope.predicate;
                if (!predicate && scope.predicateExpression) {
                    predicate = scope.predicateExpression;
                }
                return predicate;
            }

            scope.$watch('collection', function (newValue) {
                var predicate = getPredicate();
                if (newValue) {
                    var temp = [];
                    scope.distinctItems = [''];

                    angular.forEach(scope.collection, function (item) {
                        var value = item[predicate];

                        if (value && value.trim().length > 0 && temp.indexOf(value) === -1) {
                            temp.push(value);
                        }
                    });
                    temp.sort();

                    scope.distinctItems = scope.distinctItems.concat(temp);
                    scope.selectedOption = scope.distinctItems[0];
                    scope.optionChanged(scope.selectedOption);
                }
            }, true);

            scope.optionChanged = function (selectedOption) {
                var predicate = getPredicate();

                var query = {};

                query.distinct = selectedOption;

                if (query.distinct === 'All') {
                    query.distinct = '';
                }

                table.search(query, predicate);
            };
        }
    }
}]);