
jQuery(document).ready(function($){

    $('input[name="periodo_de_participacion"]').daterangepicker({
        "showDropdowns": true,
        "autoApply": true,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
        "opens": "center"

    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });




    $('input[name="fecha_de_resultado"]').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "locale": {
            "format": "DD/MM/YYYY",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ], "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ]
        }
    });

    $('input[name="fecha_de_nacimiento"]').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "minDate": "01/01/1900",
        "locale": {
            "format": "DD/MM/YYYY",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ], "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ]}
    });
    $("#pais").attr('disabled','disabled');  // checked
    $("#provincia").attr('disabled',true);  // checked
    $("#localidad").attr('disabled',true);  // checked
    $("#argentino").click(function () {

        var checked = $(this).is(':checked');
        if (checked) {
            $("#pais").attr('disabled',false);  // checked
            $("#provincia").attr('disabled',false);  // checked
            $("#localidad").attr('disabled',false);  // checked
            $("#extranjero").prop('checked', false);
        }else{
            $("#pais").attr('disabled','disabled');  // checked
            $("#provincia").attr('disabled',true);  // checked
            $("#localidad").attr('disabled',true);  // checked
            $("#extranjero").prop('checked', false);
        }
    });
    $("#extranjero").click(function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $("#pais").attr('disabled',true);  // checked
            $("#provincia").attr('disabled',true);  // checked
            $("#localidad").attr('disabled',true);  // checked
            $("#argentino").prop('checked', false)
        }else{
            $("#pais").attr('disabled','disabled');  // checked
            $("#provincia").attr('disabled',true);  // checked
            $("#localidad").attr('disabled',true);  // checked
        }

    });


    if($('#argentino').is(':checked')){
        $("#pais").attr('disabled',false);  // checked
        $("#provincia").attr('disabled',false);  // checked
        $("#localidad").attr('disabled',false);  // checked
        $("#extranjero").prop('checked', false);
    }

});

/**
 * Created by diego on 28/9/17.
 */
