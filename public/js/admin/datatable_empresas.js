/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var loadEmpresasTable  = function () {
        var table = $('#empresas').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/adm/empresas",
            "columns": [
                {data: 'nombre'},
                {data: 'telefono'},
                {data: 'email'},
                {data: 'action', name: 'action', orderable: false, searchable: false, width: '20%'}
            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });


        $("#empresas tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/adm/company/"+data.id+"/edit" ;
        });

        $("#empresas tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/adm/company/"+data.id ;

        });


        $("#empresas tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDeleteEmpresa #id").val(data.id);
            loadEmpresasTable();

        });
    }
    loadEmpresasTable();
    $('#frmDeleteEmpresa #btn_eliminar_empresa').click(function (e) {
        e.preventDefault();
        data =  $('#frmDeleteEmpresa').serialize();
        $.ajax({
            method: 'delete',
            url: '/adm/empresas/'+ $("#frmDeleteEmpresa #id").val(),
            data: data,
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (data) {
                console.log(data);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.warning("Se ha eliminado la empresa");
            loadEmpresasTable();
        });
    });


});
/**
 * Created by diego on 18/8/17.
 */
