/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadParticipantsTable  = function () {
        console.log($("#buy_id").val());
        var table = $('#participantsb').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/particantsdtb/"+$("#buy_id").val(),
            "columns": [
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'identification', name: 'identification'},
                {data: 'birthday', name: 'birthday'},
                {data: 'telephone', name: 'telephone'},
                {data: 'email', name: 'email'}
            ],

            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });





    }
    loadParticipantsTable();

});