/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadDrawsTable  = function () {
        var table = $('#draws').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/drawsdt",
            "columns": [
                { "render": function (data, type, JsonResultRow, meta) {
                    var img = JsonResultRow.avatar;
                    return '<img width="100px" src="/storage/'+ img +'" />';
                }},
                {data: 'name'},
                { "render": function (data, type, JsonResultRow, meta) {

                    return $("<div/>").html(JsonResultRow.description).text();
                }},
                {data: 'created_at'},
                { "render": function (data, type, JsonResultRow, meta) {
                    var fecha_init = JsonResultRow.date_init;
                    var fecha_fin = JsonResultRow.date_finish;
                    return fecha_init + " - " + fecha_fin;

                }},
                {data: 'date_result'},
                { "render": function (data, type, JsonResultRow, meta) {
                    // console.log(JsonResultRow.status);
                    if(JsonResultRow.status===false){
                        return '<a  class="active btn  btn-danger">Inactivo</a>'
                    }else{
                        return '<a class="desactive btn btn-primary">Activo</a>'
                    }

                }},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#draws tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/draws/"+data.id +"/edit";

        });


        $("#draws tbody").on("click", "a.assing", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/draws/"+data.id+"/questions" ;
        });

        $("#draws tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/draws/"+data.id ;
        });

        $("#draws tbody").on("click", "a.participants", function () {
            var data = table.row($(this).parents("tr")).data();

            window.location.href = "/draw/"+data.id+"/participants" ;
        });

        $("#draws tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDeleteDraw #id").val(data.id);

        });

        $("#draws tbody").on("click", "a.active", function (e) {
            e.preventDefault();
            var data = table.row($(this).parents("tr")).data();
            $.ajax({
                method: 'post',
                url: '/draws/active/'+data.id,
                success: function (response) {
                    $('#draws').DataTable().ajax.reload()
                },
                error: function (error) {
                    console.log(error);
                },

            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.success("Se ha activado el concurso");
            });

        });

        $("#draws tbody").on("click", "a.desactive", function (e) {
            e.preventDefault();
            var data = table.row($(this).parents("tr")).data();
            $.ajax({
                method: 'post',
                url: '/draws/desactive/'+data.id,
                success: function (response) {
                    $('#draws').DataTable().ajax.reload()
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.error("Se ha desactivado el concurso");
            });

        });

    }
    loadDrawsTable();

    $('#frmDeleteDraw #btn_eliminar_draw').click(function (e) {
        e.preventDefault();
        console.log($("#frmDeleteDraw #id").val());
        $.ajax({
            method: 'delete',
            url: '/draws/'+$("#frmDeleteDraw #id").val(),
            success: function (response) {
                $('#modalDelete').modal('toggle');
                $('.modal-backdrop').remove();
                loadDrawsTable();
            },
            error: function (error) {
                console.log(error);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.danger("Se ha eliminado la categoria");
        });
    });



});/**
 * Created by diego on 17/5/17.
 */
/**
 * Created by diego on 27/9/17.
 */
