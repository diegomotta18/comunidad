/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadBuyTable  = function () {
        var table = $('#buys').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/buysdt",
            "columns": [
                { "render": function (data, type, JsonResultRow, meta) {
                    var img =JsonResultRow.avatar;
                    return '<img width="100px" src="/storage/'+ img +'" />';
                }},
                {data: 'name'},
                {data: 'price_total'},
                {data: 'price_discount'},
                {data: 'created_at'},

                { "render": function (data, type, JsonResultRow, meta) {
                    if (JsonResultRow.date_init !== null){
                        var fecha_init = JsonResultRow.date_init;
                        var fecha_fin = JsonResultRow.date_finish;
                        return fecha_init + " - " + fecha_fin;
                    }else{
                        return JsonResultRow.date_specific;
                    }

                }},

                { "render": function (data, type, JsonResultRow, meta) {
                    // console.log(JsonResultRow.status);
                    if(JsonResultRow.status===false){
                        return '<a  class="active btn  btn-danger">Inactivo</a>'
                    }else{
                        return '<a class="desactive btn btn-primary">Activo</a>'
                    }

                }},
                {data: 'action', name: 'action', orderable: false, searchable: false},

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#buys tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/buys/"+data.id +"/edit";

        });


        $("#buys tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/buys/"+data.id ;
        });

        $("#buys tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDeleteBuy #id").val(data.id);

        });

        $("#buys tbody").on("click", "a.participantsb", function () {
            var data = table.row($(this).parents("tr")).data();

            window.location.href = "/buy/"+data.id+"/participants" ;
        });

        $("#buys tbody").on("click", "a.active", function (e) {
            e.preventDefault();
            var data = table.row($(this).parents("tr")).data();
            $.ajax({
                method: 'post',
                url: '/buys/active/'+data.id,
                success: function (response) {
                    $('#buys').DataTable().ajax.reload()
                },
                error: function (error) {
                    console.log(error);
                },

            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.success("Se ha activado el beneficio");
            });

        });

        $("#buys tbody").on("click", "a.desactive", function (e) {
            e.preventDefault();
            var data = table.row($(this).parents("tr")).data();
            $.ajax({
                method: 'post',
                url: '/buys/desactive/'+data.id,
                success: function (response) {
                    $('#buys').DataTable().ajax.reload()
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.error("Se ha desactivado el beneficio");
            });

        });

    }
    loadBuyTable();

    $('#frmDeleteBuy #btn_eliminar_buy').click(function (e) {
        e.preventDefault();
        console.log($("#frmDeleteBuy #id").val());
        $.ajax({
            method: 'delete',
            url: '/buys/'+$("#frmDeleteBuy #id").val(),
            success: function (response) {
                $('#modalDelete').modal('toggle');
                $('.modal-backdrop').remove();
                loadBuyTable();
            },
            error: function (error) {
                console.log(error);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.error("Se ha eliminado el beneficio");
        });
    });
});/**
 * Created by diego on 17/5/17.
 */
/**
 * Created by diego on 27/9/17.
 */
