/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadStocksTable  = function () {
        var table = $('#stocks').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/adm/stocks",
            "columns": [
                {data: 'modelo'},
                {data: 'unidad'},
                {data: 'umbral'},
                {data: 'product.nombre'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#stocks tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/adm/stocks/"+data.id+"/show" ;
        });


        $("#stocks tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDeleteStock #id").val(data.id);

        });

        $("#stocks tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/adm/stocks/"+data.id+"/edit";
        });

    }
    loadStocksTable();

    $('#frmDeleteStock #btn_eliminar_stock').click(function (e) {
        e.preventDefault();
        data =  $('#frmDeleteStock').serialize();
        $.ajax({
            method: 'delete',
            url: '/adm/stocks/'+ $("#frmDeleteStock #id").val(),
            data: data,
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (data) {
                console.log(data);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.warning("Se ha eliminado la empresa");
            loadEmpresasTable();
        });
    });


});/**
 * Created by diego on 17/5/17.
 */
/**
 * Created by diego on 30/8/17.
 */
