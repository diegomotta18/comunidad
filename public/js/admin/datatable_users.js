/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadUserTable  = function () {
        var table = $('#usuarios').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/users",
            "columns": [
                {data:'name'},
                { "render": function (data, type, JsonResultRow, meta) {
                    if(JsonResultRow.person === null){
                        return '';
                    }else{
                        return JsonResultRow.person.first_name+ ' '+ JsonResultRow.person.last_name;
                    }
                }},
                {data: 'email'},
                {data: 'role'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#usuarios tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/users/"+data.id+"/edit" ;

        });

        $("#usuarios tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/users/"+data.id ;

        });

        $("#usuarios tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDelete #id").val(data.id);
            $("#frmDelete #email").val(data.email);
            $("#frmDelete #nombre").val(data.name);

        });

    }
    loadUserTable();



    $('#frmDelete #btn_eliminar').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'delete',
            url: '/users/'+$("#frmDelete #id").val(),
            success: function (response) {
                $('#modalDelete').modal('toggle');
                $('.modal-backdrop').remove();
            },
            error: function (data) {
                console.log(data);
            },
        }).done( function (data) {
                toastr.options.timeOut = 5000;
                toastr.warning('Se ha eliminado el usuario '+$("#frmDelete #email").text());
                loadUserTable();
            }
        );
    });


});/**
 * Created by diego on 17/5/17.
 */
