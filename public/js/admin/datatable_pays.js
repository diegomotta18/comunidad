/**
 * Created by diego on 1/9/17.
 */
/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadPaysTable  = function () {
        var table = $('#pays').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processsing mode.
            "order": [], //Initial no order.
            "ajax": "/adm/pays",
            "columns": [
                { "render": function (data, type, JsonResultRow, meta) {
                    var img = JsonResultRow.avatar.replace('public/','');
                    return '<img width="100px" src="/storage/'+ img + '" />';
                    //return '<img src="/storage'+JsonResultRow.avatar.replace('public/','')+'">';
                    //return '<img width="100px" src="/storage/zhjxeRjhgR2WW0BygFfrGkXSi7vo85oEe9CTQ9Rc.png">';
                }},

                {data: 'pago_id'},
                {data: 'order_id'},

                {data: 'user.email'},
                {data: 'category.nombre'},

                {data: 'product.nombre'},
                {data: 'unit_price'},
                {data: 'quantity'},
                {data: 'total_amount'},

                {data: 'action', name: 'action', orderable: false, searchable: false, width: '20%'}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });
        $("#pays tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/adm/pay/"+data.id+"/show" ;
        });




    }
    loadPaysTable();




});
