/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadCategoriesTable  = function () {
        var table = $('#categories').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/adm/categories",
            "columns": [
                {data: 'nombre'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#categories tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();

            window.location.href = "/adm/categories/"+data.id +"/edit";

        });


        $("#categories tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmDeleteCategory #id").val(data.id);
            $("#frmDeleteCategory #nombre").val(data.nombre);

        });

    }
    loadCategoriesTable();

    $('#frmDeleteCategory #btn_eliminar_category').click(function (e) {
        e.preventDefault();
        data =  $('#frmDeleteCategory').serializeArray();
        dataArray =  $('#frmDeleteCategory').serializeArray();
        dataObj = {};
        $(dataArray).each(function(i, field){
            dataObj[field.name] = field.value;
        });
        $.ajax({
            method: 'delete',
            url: '/adm/categories/destroy',
            data: data,
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (dataObj) {
                console.log(dataObj);
            },
        }).done(function () {
            toastr.options.timeOut = 5000;
            toastr.options.closeButton = true;
            toastr.success("Se ha eliminado la categoria");
            loadCategoriesTable();

        });
    });

});/**
 * Created by diego on 17/5/17.
 */
