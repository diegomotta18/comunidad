/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var loadProductsVisitTable  = function () {
        var table = $('#productsvisit').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            pagingType: "numbers",
            language: {
                url: "/js/viewer/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/products",
            "columns": [

                { "render": function (data, type, JsonResultRow, meta) {

                    return'<td style="border: 1px solid #fff;">'
                           + '<div class="slide" style="margin: 0px;">'
                        + '<div class="productImage" style="margin: 0px;" onclick="showDetail(' +JsonResultRow.id+')">'
                        + '<img class="col-xs-12" src="/storage/'+JsonResultRow.avatar+'">'
                        + '<div class="productMasking">'
                        + '<ul class="btn-group" role="group">'
                        + '</ul>'
                        + '</div>'
                        +' <div class="row"> '
                        + '<h3><span class="label pull-right" style="background-color:'+JsonResultRow.category.color+'">' + JsonResultRow.category.nombre + '</span></h3> '
                        + '</div> '
                        + '<div class="row">'
                            + '<h4><span class="label label-danger-filled pull-right">'+  JsonResultRow.descuento +' % de descuento</span></h4>'
                        + '</div>'
                        + '<div class="row">'
                        + '<div class="col-xs-12">'
                        + '<h5 class="title" align="justify"><b style="color: black;">'+ JsonResultRow.nombre + '</b>'
                        + '</h5>'
                        + '</div>'
                        + '</div>'
                        + '<div class="row">'
                        + '<div class="col-xs-5">'
                        + '<h4 style="color: #8c8c8c; text-align: center;">Antes</h4>'
                        + '<h2 class="title" style="color: #8c8c8c;text-align: center;">'
                         + '<s> $ '+ JsonResultRow.precio +'</s></h2>'
                        + '</div>'
                        + '<div class="col-sm-5">'
                        + '<h4 style="color: #8c8c8c;text-align: center">Pagas</h4>'
                        + '<h2 class="title" style="color:#FF7814;text-align: center;">'
                        + '<b> $'+JsonResultRow.precio_descontado +' </b>'
                        + '</h2> </div> </div> </div> </a> </div> </td>';
                }},
                // { "render": function (data, type, JsonResultRow, meta) {
                //     return '<img width="100px" src="/storage/'+JsonResultRow.avatar.replace('public','')+'">';
                // }},


            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



    }
    loadProductsVisitTable();




});

function showDetail(id) {
    location.href= '/products/'+ id + '/detail';
}
/**
 * Created by diego on 21/8/17.
 */
