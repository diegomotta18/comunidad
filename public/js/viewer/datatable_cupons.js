/**
 * Created by diego on 1/9/17.
 */
/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadCuponsTable  = function () {
        var table = $('#cupons').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/viewer/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processsing mode.
            "order": [], //Initial no order.
            "ajax": "/cupons",
            "columns": [

                {data: 'id'},
                {data: 'paynotification.order_id'},
                {data: 'product.nombre'},
                { "render": function (data, type, JsonResultRow, meta) {
                    return "$ " +JsonResultRow.product.precio_descontado;
                }},
                { "render": function (data, type, JsonResultRow, meta) {
                    return  JsonResultRow.product.fecha_inicio + " - "+ JsonResultRow.product.fecha_fin ;
                }},
                { "render": function (data, type, JsonResultRow, meta) {
                        return   '<div class="btn-group" role="group" aria-label="...">'+
                                 '<a type="button"  class="btn btn-md btn-info print"><i class="fa fa-print"></i> </a></div>';

                }},

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });
        $("#cupons tbody").on("click", "a.print", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/download/"+data.id ;
        });




    }
    loadCuponsTable();




});
