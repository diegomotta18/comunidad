<?php

namespace App\Policies;

use App\Models\Buy;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BeneficiosPolicies
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */


    public function __construct()
    {
        //

    }

    public function edit(User $user){
       return  $user->hasRole('admin') ||  $user ->hasRole('god') ? true : false;

    }
}
