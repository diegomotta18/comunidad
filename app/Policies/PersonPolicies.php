<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PersonPolicies
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user){

        return  $user->hasRole('employee') || $user->hasRole('adm') ||  $user->hasRole('god') ? true : false;

    }

    public function show(User $user){

        return  $user->hasRole('employee') || $user->hasRole('adm') ||  $user->hasRole('god') ? true : false;

    }

    public function create(User $user){

        return  $user->hasRole('adm') ||  $user->hasRole('god') ? true : false;

    }

    public function update(User $user){

        return  $user->hasRole('adm') ||  $user->hasRole('god') ? true : false;

    }

    public function delete(User $user){

        return  $user->hasRole('adm') ||  $user->hasRole('god') ? true : false;

    }
}
