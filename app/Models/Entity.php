<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 25/9/17
 * Time: 11:07
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    public static function getClass()
    {
        return get_class(new static);
    }
}