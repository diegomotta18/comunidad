<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    //
    protected $table = 'towns';

    protected $fillable = [
        'id','name','district_id'
    ];


    public function district(){
        return $this->belongsTo('App\Models\District');
    }


}
