<?php

namespace App\Models;


class Question  extends Entity
{
    //
    protected $table = 'questions';

    protected $fillable = [
        'id','input_type', 'draw_id','question'
    ];

    public function Draw(){
        return $this->belongsTo(Draw::class);
    }

    public function options()
    {
        return $this->belongsToMany(Option::class)->withTimestamps();
    }
}
