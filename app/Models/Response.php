<?php

namespace App\Models;


class Response extends Entity
{
    //
    //
    protected $table = 'responses';

    protected $fillable = [
        'id','draw_id','question_id', 'resp','person_id'
    ];

    public function person(){
        return $this->belongsTo(Person::class);
    }

}
