<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyPerson extends Model
{
    //
    protected $table = 'buys_persons';

    protected $fillable = [
            'id','buy_id','person_id', 'created_at'
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y h:m A');
    }


}
