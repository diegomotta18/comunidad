<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    protected $table = 'districts';

    protected $fillable = [
        'id','name','state_id'
    ];


    public function state(){
        return $this->belongsTo('App\Models\State');
    }


    public function towns(){
        return $this->belongsTo('App\Models\Town');
    }
}
