<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 4/10/17
 * Time: 11:30
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;

class Address extends Entity
{
    //
    protected $table = 'addresses';

    protected $fillable = [
        'country_id', 'state_id', 'district_id', 'town_id',
    ];

    public function  person(){
        return $this->hasOne('App\Models\Person');
    }

    public function getCountry($id){
        $country = Country::find($id);
        return $country;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }
}