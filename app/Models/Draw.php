<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;
use Carbon\Carbon;

class Draw extends Entity
{
    //
    protected $table = 'draws';

    protected $fillable = [
        'name', 'description','metadescription','date_init','date_finish','date_result','avatar','alt','status','delete',
    ];

    public function getDateInitAttribute($value){
        return date('d/m/Y',strtotime(str_replace('-','/',$value)));
    }

    public function getDateFinishAttribute($value){
        return date('d/m/Y',strtotime(str_replace('-','/',$value)));
    }

    public function getDateResultAttribute($value){
        return date('d/m/Y',strtotime(str_replace('-','/',$value)));
    }

    public function getAvatarAttribute($value){
        return str_replace('public/','',$value);
    }

    public function persons(){
        return $this->belongsToMany('App\Models\Person')
            ->withTimestamps();
    }

    public function questions(){
        return $this->hasMany(Question::class)->where('delete',false);
    }

    public function scopeLastChangeDraw($query)
    {
        return $query->with('questions.options')->orderby('created_at','DESC')->take(3)->get();
    }

    public function isExpired()
    {
        if ( $this->attributes['date_finish'] == Carbon::now()->format('Y-m-d')){
            return true;
        }

        return false;
    }
}
