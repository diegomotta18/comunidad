<?php

namespace App\Models;

class Option extends Entity
{
    //
    protected $table = 'options';

    protected $fillable = [
        'id','label_option'
    ];

    public function questions()
    {
        return $this->belongsToMany(Question::class)->withTimestamps();
    }
}
