<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buy extends Entity
{
    //
    protected $table = 'buys';

    protected $fillable = [
      'slug', 'url', 'name', 'description','metadescription','date_init','date_finish','avatar','alt','status','delete','price_total','percent','price_discount','date_specific','time_specific','check_date_especific','check_date_period'
    ];

    public function persons(){
        return $this->belongsToMany(Person::class,'buys_persons')
                    ->withTimestamps();
    }

    public function getAvatarAttribute($value){
        return str_replace('public/','',$value);
    }

    public function getDateInitAttribute(){
        if (!is_null($this->attributes['date_init'])) {
            return date('d/m/Y', strtotime(str_replace('-', '/', $this->attributes['date_init'])));
        }
        return null;
    }

    public function getDateSpecificAttribute(){
        if (!is_null($this->attributes['date_specific'])) {
            return date('d/m/Y', strtotime(str_replace('-', '/', $this->attributes['date_specific'])));
        }
        return null;
    }

    public function getDateFinishAttribute(){
        if (!is_null($this->attributes['date_finish'])){
            return date('d/m/Y',strtotime(str_replace('-','/',$this->attributes['date_finish'])));
        }
        return null;
    }

    public function getDateSpecific(){
        if (!is_null($this->attributes['date_specific'])) {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['date_specific'])->format('d/m/Y');
        }
        return null;
    }

    public function getTimeSpecific(){
        if (!is_null($this->attributes['time_specific'])) {
            return Carbon::createFromFormat('H:i:s', $this->attributes['time_specific'])->format('g:i A');
        }
        return null;
    }


    public function getDate(){
        if (!is_null($this->attributes['date_specific'])){
            return  Carbon::createFromFormat('Y-m-d', $this->attributes['date_specific'])->format('m-d-Y');
        }
        return null;

    }

    public function getTime(){
        if (!is_null($this->attributes['time_specific'])){
            return Carbon::createFromFormat('H:i:s', $this->attributes['time_specific'])->format('H:i:s');
        }
        return null;
    }

    public function getTimeDate(){
        return $this->getDate().' '.$this->getTime();
    }

    public function scopeLastChangeBuy($query)
    {
        return $query->orderby('created_at','DESC')->take(3)->get();
    }

    public function codes(){
        return $this->hasMany(Code::class);
    }

    public function isExpired()
    {
            if ( $this->attributes['date_specific'] == Carbon::now()->format('Y-m-d') && Carbon::createFromFormat('H:i:s',$this->attributes['time_specific'])->format('g:i A') <= Carbon::now()->format('g:i A')){
                return true;
            }

        return false;
    }
}
