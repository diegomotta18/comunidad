<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Code extends Model
{
    //
    use SoftDeletes;
    protected $table = 'codes';

    protected $fillable = [
        'code'
    ];

    public function buy(){
        return $this->belongsTo(Buy::class);
    }
}
