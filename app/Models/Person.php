<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Person extends Entity
{
    //
    protected $table = 'persons';
    protected $validation = null;

    protected $fillable = [
        'email','first_name', 'last_name','birthday','identification','user_id','address_id','telephone','sex','extranjero'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getBirthdayAttribute($value){
        return date('d/m/Y',strtotime(str_replace('-','/',$value)));
    }
    public function getSexAttribute($value){
        return $value;
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function draws(){
        return $this->belongsToMany('App\Models\Draw')
            ->withTimestamps();
    }

    public function buys(){
        return $this->belongsToMany(Buy::class,'buys_persons')
                    ->withTimestamps();
    }

    public function isParticipe($draw){
        return $this->draws->contains($draw);

    }

    public function responses(){
        return $this->hasMany(Response::class);
    }

    public function responsesForDraw($id){
        return $this->hasMany(Response::class)->where('draw_id',$id)->select('resp');
    }



    public function setFails($fails){
        return $this->validation = $fails;
    }

    public function getFails(){
        return $this->validation;
    }


    public function getExtranjeroAttribute($value)
    {
        if ($value == true) {
            return 'Si';
        } else {
            return 'No';
        }
    }

    public function getNameComplete(){
        return $this->first_name." ".$this->last_name;
    }
}
