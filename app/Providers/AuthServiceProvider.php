<?php

namespace App\Providers;

use App\Models\Buy;
use App\Models\Person;
use Laravel\Passport\Passport;

use App\Policies\BeneficiosPolicies;
use App\Policies\PersonPolicies;

use App\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Buy::class => BeneficiosPolicies::class,
        Person::class => PersonPolicies::class

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
//        Passport::routes();

        //

        Gate::define('user', function (User $user) {
            return $user->hasRole('user');
        });
        Gate::define('employee', function(User $user){
            if ($user->hasRole('god')) {
                return $user->hasRole('god');
            } elseif($user->hasRole('adm')) {
                return $user->hasRole('adm');
            }else{
                return $user->hasRole('employee');
            }
        });
        Gate::define('adm', function(User $user){
            if ($user->hasRole('god')) {
                return $user->hasRole('god');
            } else {
                return $user->hasRole('adm');
            }
        });

        Gate::define('god', function(User $user){

            return $user->hasRole('god');
        });



    }
}


