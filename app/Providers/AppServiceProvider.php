<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('compare_date', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $fechas = explode(" - ", $data['periodo_de_participacion']);
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
            return $fecha_fin <= date('Y-m-d', strtotime(str_replace('/', '-', ($data['fecha_de_resultado']))));
        });

        Validator::replacer('compare_date', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':fecha_de_resultado', $parameters[0], "El campo fecha de resultado debe ser mayor o igual a la fecha de finalización del concurso");
        });


        Validator::extend('menor_que', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $price_discount = $data['precio_descontado'];
            return $price_discount < $data['precio_total'];
        });

        Validator::replacer('menor_que', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':precio_descontado', $parameters[0], "El campo precio descontado debe ser menor al precio total");
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
