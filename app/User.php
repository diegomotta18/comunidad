<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

use App\Models\Person;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role','person_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($rol)
    {
        return $this->role == $rol;
    }

    public function person()
    {
        return $this->hasOne(Person::class);
    }


    public function rol()
    {
        if (Auth::user()->role == 'god') {
            return 'Super administrador';
        } else if (Auth::user()->role == 'adm') {
            return 'Administrador';
        } else if (Auth::user()->role == 'user') {
            return 'user';
        }
        else if (Auth::user()->role == 'employee') {
            return 'employee';
        }
    }


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function canSeeGodView(){
       return $this->role == 'god' ? true : false;
    }

    public function canSeeGAView(){
        if ($this->role == 'admin'){
            return true;
        }
        else if ($this->role == 'god'){
            return true;
        }
        return false;
    }

    public function canSeeGAEView(){
        if ($this->role == 'admin'){
            return true;
        }
        else if ($this->role == 'god'){
            return true;
        }
        else if ($this->role == 'employee'){
            return true;
        }
        return false;
    }


    public function canSeeEView(){
        if ($this->role == 'employee'){
            return true;
        }
        return false;
    }
}
