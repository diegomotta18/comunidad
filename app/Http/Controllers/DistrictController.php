<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Repositories\DistrictRepository;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    //
    protected $districtRep;

    public function __construct(DistrictRepository $districtRep)
    {
        $this->districtRep = $districtRep;


    }

    public function getDistricts($request)
    {
        if ($request != 'undefined') {
            $districts = District::where('state_id', '=', $request)->get();
            if ($districts) {
                return $districts;
            }
        }

        return [];
    }
}
