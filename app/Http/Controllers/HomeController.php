<?php

namespace App\Http\Controllers;

use App\Models\Buy;
use App\Models\Draw;
use App\Repositories\BuyRespository;
use App\Repositories\DrawRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Alert;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $draw;
    protected $buy;

    public function __construct(DrawRepository $draw, BuyRespository $buy)
    {
        $this->draw = $draw;
        $this->buy = $buy;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->home($request);

    }

    public function dashboardUser(Request $request)
    {
        return $this->home($request);
    }

    public function dashboadAdmin(Request $request)
    {
        return view('private');
    }

    public function beneficios()
    {
        $buys = $this->buy->allBuy();

        return view('client.buys.index', compact('buys'));
    }

    public function concursos()
    {
        $draws = $this->draw->allDraw();

        return view('client.draw.index', compact('draws'));
    }

    public function concursosj()
    {
        $draws = $this->draw->allDraw();

        return $draws->toJson();
    }

    public function beneficiosj()
    {
        $buys = $this->buy->allBuy();

        return $buys->toJson();
    }

    public function showbeneficit($slug)
    {
        $buy = $this->buy->findForSlug($slug);
        if (!is_null($buy)) {
            return view('client.buys.show', compact('buy'));
        } else {
            return Redirect::to(url('/'));
        }
    }

    public function show($slug)
    {
        $draw = $this->draw->findForSlug($slug);
        if (!is_null($draw)) {
            return view('client.draw.show', compact('draw'));
        } else {
            return Redirect::to(url('/'));
        }
    }

    public function preparticipe(Request $request)
    {
        $request->session()->push('draw_id', $request->input('draw_id'));
        Alert::info('Para participar del concurso es necesario ingresar con clave y contraseña',
            'Aviso')->persistent("Aceptar");

        return Redirect::to(route('login'));

    }

    public function prebuy(Request $request)
    {
        $request->session()->push('buy_id', $request->input('buy_id'));
        Alert::info('Para acceder al beneficio es necesario ingresar con clave y contraseña o registrarse',
            'Aviso')->persistent("Aceptar");

        return Redirect::to(route('login'));

    }

    public function about()
    {
        return view('client.about.about');
    }

    public function contact()
    {
        return view('client.contact.contact');
    }


    public function home(Request $request)
    {
        $draws = $this->draw->allDrawPaginate();
        $buys = $this->buy->allBuyPaginate();

        if ($request->ajax()) {
            if ($request->input('value') == 'draw') {
                return view('client.home.draws', ['draws' => $draws])->render();
            } else {
                if ($request->input('value') == 'buy') {
                    return view('client.home.buys', ['buys' => $buys])->render();
                }
            }
        }

        if (Auth::user()) {
            $valor = $this->checkSession($request);
            if (!is_null($valor)) {
                return $valor;
            }
        }

        if ($request->session()->has('buy_id')) {
            $buy = Buy::find($request->session()->get('buy_id'))->first();
            $request->session()->forget('buy_id');

            return redirect('/beneficios/'.$buy->slug);
        }

        if ($request->session()->has('draw_id')) {
            $draw = Draw::find($request->session()->get('draw_id'))->first();
            $request->session()->forget('draw_id');

            return Redirect::to(url('/concursos/'.$draw->slug));
        }

        return view('client.home.home', compact('buys', 'draws'));
    }


    public function checkSession(Request $request)
    {
        $valor = null;
        if (!is_null(Auth::user()->aux) && Auth::user()->aux == 'buy_id') {
            $buy = Buy::find(Auth::user()->session)->first();
            $this->setNullableUser();
            Alert::info('Presione comprar, para acceder al beneficio', 'Aviso')->autoclose(3000);;
            $valor = Redirect::to(url("/beneficios/".$buy->slug));
        }
        if (!is_null(Auth::user()->aux) && Auth::user()->aux == 'draw_id') {
            $draw = $this->draw->findForId(Auth::user()->session);
            $this->setNullableUser();
            $request->session()->forget('draw_id');
            Alert::info('Presione participar, para confirmar el concurso', 'Aviso')->autoclose(3000);;
            $valor = Redirect::to(url("/concursos/".$draw->slug));
        }

        return $valor;
    }

    public function setNullableUser()
    {
        $user = Auth::user();
        $user->session = null;
        $user->aux = null;
        $user->save();
    }
}
