<?php

namespace App\Http\Controllers;

use App\Models\State;
use App\Repositories\StateRepository;
use Illuminate\Http\Request;

class StateController extends Controller
{
    //
    protected $stateRep;

    public function __construct(StateRepository $stateRep)
    {
        $this->stateRep = $stateRep;


    }

    public function getStates($request)
    {

        if ($request != 'undefined') {
            $provinces = State::where('country_id', '=', $request)->get();
            if ($provinces) {
                return $provinces;
            }
        }

        return [];
    }

}