<?php

namespace App\Http\Controllers;

use App\Repositories\CountryRepository;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    protected $countryRep;

    public function __construct(CountryRepository $countryRep)
    {
        $this->countryRep = $countryRep;


    }

    public function getCountries()
    {
        $countries = $this->countryRep->all();

        return $countries;
    }
}
