<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Repositories\DrawRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    //
    protected $drawRepository;
    protected $questionRepository;

    public function __construct(DrawRepository $drawRepository, QuestionRepository $questionRepository)
    {
        $this->drawRepository = $drawRepository;
        $this->questionRepository = $questionRepository;

    }


    public function index($id)
    {
        $draw = $this->drawRepository->find($id);
        $questions = $this->questionRepository->findAll($id);

        return view('admin.questions.index', compact('questions', 'draw'));
    }

    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'pregunta'          => 'required:max:140',
            'respuestas.*'      => 'required:max:70',
            'tipo_de_respuesta' => 'required'
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {

            $draw = $this->questionRepository->created($request);
            $notification = [
                'message'    => 'Se ha creado correctamente la pregunta',
                'alert-type' => 'success'
            ];

            return Redirect::to(route('question.index', $request->input('draw_id')))->with($notification);
        }
    }

    /*
     *
     * Puede recibir el name delete (nombre del boton eliminar respuesta) y el id de la opcion a eliminar
     * Puede recibir el name cancel (nombre del boton cancelar)
     * Por defecto si no recibe delete o cancel, pasa a actualizar los datos del cuestionario
     * */
    public function update(Request $request, $id)
    {

        if ($request->has('delete') && ($request->has('option_id'))) {

            Option::destroy($request->input('option_id'));
            $notification = [
                'message'    => 'Se ha eliminado la respuesta '.$request->input('option_value'),
                'alert-type' => 'error'
            ];

            return Redirect::to(url('draws/'.$request->input('draw_id').'/questions/'.$id))->with($notification);
        } else {
            if ($request->has('cancel')) {
                return Redirect::back();

            } else {

                $validation = Validator::make($request->all(), [
                    'pregunta'          => 'required:max:140',
                    'respuestas.*'      => 'required:max:70',
                    'tipo_de_respuesta' => 'required'
                ]);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation->errors());
                } else {
                    $question = $this->questionRepository->updated($request, $id);
                    $notification = [
                        'message'    => 'Se ha modificado el cuestionario '.$question->question,
                        'alert-type' => 'success'
                    ];

                    return Redirect::to(url('draws/'.$request->input('draw_id').'/questions/'))->with($notification);
                }

            }
        }
    }

    public function edit($draw_id, $question_id)
    {
        $quest = $this->questionRepository->find($question_id);
        $draw = $this->drawRepository->find($draw_id);
        $questions = $this->questionRepository->findAll($draw_id);

        return view('admin.questions.edit', compact('draw', 'quest', 'questions'));
    }

    /*
     * Eliminacion logica de cuestionarios del concurso
     * */
    public function deleted(Request $request)
    {

        $quest = $this->questionRepository->deleted($request);
        $notification = [
            'message'    => 'Se ha eliminado el cuestionario '.$quest->question,
            'alert-type' => 'error'
        ];

        return Redirect::to(url('draws/'.$request->input('draw_id').'/questions/'))->with($notification);
    }


}
