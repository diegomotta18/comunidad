<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Requests\RegisterUserRequest;
use App\Mail\EmailVerification;
use App\Mail\EmailVerificationMobile;
use App\Models\Address;
use App\Models\Person;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Alert;
use Ixudra\Curl\Facades\Curl;

class RegisterControllerApi extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/';


    protected function register(RegisterUserRequest $request)
    {
        $user = User::create([
            'role' => "user",
            'name' => $request->input('nombres'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
        $fecha = date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento'))));

        $person = Person::create([
            'first_name' => $request->input('nombres'),
            'last_name' => $request->input('apellido'),
            'identification' => $request->input('documento'),
            'birthday' => $fecha,
            'telephone' => $request->input('telefono'),
            'sex' => $request->input('sexo'),
            'user_id' => $user->id,
            'email' => $user->email,
        ]);

        $nacionalidad = $request->input('nacionalidad');
        if ($nacionalidad == 'Argentino') {
            $address = Address::create([
                'country_id' => 1,
                'state_id' => $request->input('provincia'),
                'district_id' => $request->input('localidad'),
            ]);
            $address->person()->save($person);
        } elseif ($nacionalidad == 'Extranjero') {
            $person->extranjero = true;
            $person->save();
        }

        $user->registration_token = str_random(80);
        $user->name = $person->first_name . " " . $person->last_name;
        $user->person_id = $person->id;
        $user->save();
        $user->person()->save($person);

        $url = "http://testcomunidad.misionesonline.net/apiconfirmation?token=".$user->registration_token;

        Mail::to($user)->queue(new EmailVerificationMobile($user, $url, $person));
        $response = Curl::to('http://crm.misionesonline.net/stored')
            ->withData($request)
            ->post();
        return $url;
    }
    protected function getConfirmation($token)
    {
        $user = User::where('registration_token', $token)->first();
        if(!is_null($user)){
            $user->registration_token = null;
            $user->save();
            return http_response_code(200);
        }else{
            return abort(403);
        }

    }
}
