<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 6/3/18
 * Time: 12:04
 */


namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Models\Buy;
use App\Models\Draw;
use App\Repositories\BuyRespository;
use App\Repositories\DrawRepository;


class HomeControllerApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $draw;
    protected $buy;

    public function __construct(DrawRepository $draw, BuyRespository $buy)
    {
        $this->draw = $draw;
        $this->buy = $buy;
    }

    public function concursosj()
    {
        $draws = $this->draw->allDraw();
        return $draws->toJson();
    }

    public function beneficiosj()
    {
        $buys = $this->buy->allBuy();
        return $buys->toJson();
    }

    public function getLastChangeDraw()
    {
        return Draw::lastChangeDraw()->toJson();
    }

    public function getLastChangeBuy()
    {
        return Buy::lastChangeBuy()->toJson();
    }
}