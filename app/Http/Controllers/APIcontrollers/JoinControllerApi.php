<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Repositories\DrawRepository;
use App\Repositories\PersonRepository;
use Illuminate\Http\Request;

class JoinControllerApi extends Controller
{
    //
    protected $drawRepository, $personRepository;

    public function __construct(PersonRepository $personRepository, DrawRepository $drawRepository)
    {
        $this->personRepository = $personRepository;
        $this->drawRepository = $drawRepository;
        $this->middleware('auth:api');

    }


    public function participarj(Request $request)
    {

        $person = $this->personRepository->find($request->input('person_id'));
        if ($this->drawRepository->exitsPerson($person, $request->input('draw_id'))) {
            return abort(403);
        }
        $this->drawRepository->join($request, $person);
        return http_response_code(200);
    }

    public function participarWhitOptionj(Request $request){
        $person = $this->personRepository->find($request->input('person_id'));
        if ($this->drawRepository->exitsPerson($person, $request->input('draw_id'))) {
            return abort(403);
        }
        $this->drawRepository->joinj($request, $person);
        return http_response_code(200);
    }
}
