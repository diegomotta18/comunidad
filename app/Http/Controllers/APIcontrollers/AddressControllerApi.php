<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 15/3/18
 * Time: 10:02
 */
namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\State;
use Symfony\Component\HttpFoundation\Request;


class AddressControllerApi extends Controller{

    public function getStates($id){
        if ($id != 'undefined') {
            $provinces = State::where('country_id', '=', $id )->get();
            if ($provinces) {
                return $provinces->toJson();
            }
        }
        return [];
    }

    public function getDistricts($id){
        if ($id != 'undefined') {
            $districts = District::where('state_id', '=', $id )->get();
            if ($districts) {
                return $districts->toJson();
            }
        }
        return [];
    }
}