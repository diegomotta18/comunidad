<?php

namespace App\Http\Controllers;

use App\Models\Draw;
use App\Repositories\DrawRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

/*
 * Este controlador se encarga de la gestion de los concursos
 * Funciones: Recuperar todos concursos, Recuperar un concurso
 * Crear, Modificar, Activar o Inactivar y Eliminar Concursos
 *
 *
 * */

class DrawController extends Controller
{


    protected $draw;

    public function __construct(DrawRepository $draw)
    {
        $this->middleware('auth');

        $this->draw = $draw;
    }

    public function index()
    {
        //
        return view('draws.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('draws.create');

    }


    /*
     * Crear el concurso
     *
     * */

    public function store(Request $request)
    {
        //
        $validation = Validator::make($request->all(), [
            'nombre'                   => 'required|max:180',
            'descripcion'              => 'required',
            'metadescription'          => 'required|max:180',
            'periodo_de_participacion' => 'required',
            'fecha_de_resultado'       => 'required|compare_date:periodo_de_participacion',
            'descripcion_de_la_imagen' => 'required|max:180',
            'imagen'                   => 'required|mimes:jpeg,bmp,png',
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {

            $draw = $this->draw->created($request);
            $notification = [
                'message'    => 'Se ha creado un nuevo sorteo'.$draw->name,
                'alert-type' => 'success'
            ];

            return Redirect::to(route('draws.index'))->with($notification);

        }
    }


    /*
     * Recupera el concurso y lo muestra en la vista
     *
     * */
    public function show($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $draw = $this->draw->findForId($id);

            return view('draws.show', compact('draw'));
        }
    }


    /*
     * Busca el concurso y lo muestra en la vista para editarlo
     *
     * */
    public function edit($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $draw = $this->draw->findForId($id);

            return view('draws.edit', compact('draw'));
        }
    }

    /*
     * Recibe los datos a modificar del concurso.
     */
    public function update(Request $request, $id)
    {
        //
        $validation = Validator::make($request->all(), [
            'nombre'                   => 'required|max:180',
            'descripcion'              => 'required',
            'metadescription'          => 'required|max:180',
            'periodo_de_participacion' => 'required',
            'fecha_de_resultado'       => 'required|compare_date:periodo_de_participacion',
            'descripcion_de_la_imagen' => 'required|max:180',
            'imagen'                   => 'mimes:jpeg,bmp,png',
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {

            $draw = $this->draw->updated($request, $id);
            $valor = "<b style='font-size: 15px;'>: ".$draw->name."</b>";
            $notification = [
                'message'    => 'Se ha modificado el sorteo'.$valor,
                'alert-type' => 'success'
            ];

            return Redirect::to(route('draws.index'))->with($notification);

        }

    }

    /*
     * Elimina logicamente el concurso cambiando su estado "delete" = true
     */
    public function destroy($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $this->draw->destroy($id);
        }
    }

    /*
     * Recupera todos los concursos
     * */

    public function getDraws()
    {
        return $this->draw->getDraws();
    }

    /*
     * Muestra la vista de los participantes de un determinado concurso
     * */
    public function participants($id)
    {
        //
        $draw = $this->draw->find($id);

        return view('admin.participants.index', compact('draw'));
    }

    /*
     * Recupera todos los participantes de un determinado concurso
     * */
    public function getParticipants($id)
    {
        return $this->draw->getParticipants($id);
    }

    /*
     *  Exporta a un archivo excel los participantes que estan concursando en un determinado sorteo
     * */

    public function export($id)
    {
        $this->draw->exported($id);
    }

    public function active($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $this->draw->active($id);
        }
    }

    public function desactive($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $this->draw->desactive($id);
        }
    }


}