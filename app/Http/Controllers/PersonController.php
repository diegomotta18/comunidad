<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Country;
use App\Models\District;
use App\Models\Draw;
use App\Models\Person;
use App\Models\State;
use App\Repositories\PersonRepository;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Alert;
use DataTables;
use Illuminate\Database\Eloquent\Collection;
use Excel;

class PersonController extends Controller
{
    //
    protected $personRepository;
    protected $userRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->middleware('auth');

        $this->personRepository = $personRepository;
    }

    public function index()
    {
        $this->authorize('index', Person::class);
        $persons = $this->personRepository->getPersons();
        return view('admin.persons.index',compact('persons'));
    }


    public function create()
    {
        $this->authorize('create', Person::class);

        return view('admin.persons.create');
    }

    public function editPerfil()
    {

        $userp = Auth::user()->person;
        $person = Person::with('address:id,country_id,district_id,state_id')->find($userp->id);

        return view('client.perfil.update_perfil', compact('person'));
    }

    public function updatePerfil(Request $request, $id)
    {

        $validation = null;
        if (is_null($request->input('argentino'))) {
            $validation = Validator::make($request->all(), [
                'extranjero' => 'accepted'
            ]);
        }
        if (is_null($request->input('extranjero'))) {
            $validation = Validator::make($request->all(), [
                'argentino' => 'accepted'
            ]);
        }
        if (!is_null($request->input('argentino'))) {
            $validation = Validator::make($request->all(), [
                'email'               => 'required|string|email|max:255',
                'nombres'             => 'required|string|max:70',
                'apellido'            => 'required|string|max:70',
                'documento'           => 'required|max:15',
                'fecha_de_nacimiento' => 'required',
                'telefono'            => 'required',
                'pais'                => 'required',
                'provincia'           => 'required',
                'localidad'           => 'required',
                'sexo'                => 'required'
            ]);
        } else {
            if (!is_null($request->input('extranjero'))) {
                $validation = Validator::make($request->all(), [
                    'email'               => 'required|string|email|max:255',
                    'nombres'             => 'required|string|max:70',
                    'apellido'            => 'required|string|max:70',
                    'documento'           => 'required|max:15',
                    'fecha_de_nacimiento' => 'required',
                    'telefono'            => 'required',
                    'sexo'                => 'required'
                ]);
            }
        }

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {

            $item = $this->personRepository->updated($request, $id);

            Alert::success('Se ha modificado correctamente el usuario '.$item->first_name." ".$item->last_name,
                'Aviso')->autoclose(3000);;

            return Redirect::to(route('home'));
        }

    }


    public function store(Request $request)
    {
        $this->authorize('create', Person::class);

        $validation = null;
        if ($request->has('argentino')) {
            $validation = Validator::make($request->all(), [
                'extranjero' => 'accepted'
            ]);
        }
        if ($request->has('extranjero')) {
            $validation = Validator::make($request->all(), [
                'argentino' => 'accepted'
            ]);
        }
        if ($request->has('argentino')) {
            $validation = Validator::make($request->all(), [
                'email'               => 'required|string|email|max:255',
                'nombres'             => 'required|string|max:70',
                'apellido'            => 'required|string|max:70',
                'documento'           => 'required|max:15',
                'fecha_de_nacimiento' => 'required',
                'telefono'            => 'required',
                'pais'                => 'required',
                'provincia'           => 'required',
                'localidad'           => 'required',
                'sexo'                => 'required'
            ]);
        } else {
            if ($request->has('extranjero')) {
                $validation = Validator::make($request->all(), [
                    'email'               => 'required|string|email|max:255',
                    'nombres'             => 'required|string|max:70',
                    'apellido'            => 'required|string|max:70',
                    'documento'           => 'required|max:15',
                    'fecha_de_nacimiento' => 'required',
                    'telefono'            => 'required',
                    'sexo'                => 'required'
                ]);
            }
        }

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {
            $item = $this->personRepository->created($request);
            $notification = [
                'message'    => 'Se ha creado la persona'.$item->first_name." ".$item->last_name,
                'alert-type' => 'success'
            ];

            return Redirect::to(route('persons.index'))->with($notification);
        }

    }

    public function show($id)
    {
        $this->authorize('show', Person::class);

        if (ctype_digit($id) && !is_null($id)) {
            $country = null;
            $state = null;
            $district = null;
            $person = $this->personRepository->findForId($id);
            if (!is_null($person->address_id)) {
                $country = Country::find($person->address()->first()->country_id);
                $state = State::find($person->address()->first()->state_id);
                if (!is_null($person->address()->first())) {
                    $district = District::find($person->address()->first()->district_id);
                }
            }

            return view('admin.persons.show', compact('person', 'country', 'state', 'district'));
        } else {
            return Redirect::to(route('persons.index'));
        }
    }

    public function edit($id)
    {
        $this->authorize('update', Person::class);

        if (ctype_digit($id) && !is_null($id)) {
            $country = null;
            $state = null;
            $district = null;
            $person = $this->personRepository->findForId($id);
            if (!is_null($person->address_id)) {
                $country = Country::find($person->address()->first()->country_id);
                $state = State::find($person->address()->first()->state_id);
                $district = District::find($person->address()->first()->district_id);
            }

            return view('admin.persons.edit', compact('person', 'country', 'state', 'district'));
        } else {
            return Redirect::to(route('persons.index'));
        }
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update', Person::class);

        $validation = null;
        if (is_null($request->input('argentino'))) {
            $validation = Validator::make($request->all(), [
                'extranjero' => 'accepted'
            ]);
        }
        if (is_null($request->input('extranjero'))) {
            $validation = Validator::make($request->all(), [
                'argentino' => 'accepted'
            ]);
        }
        if (!is_null($request->input('argentino'))) {
            $validation = Validator::make($request->all(), [
                'email'               => 'required|string|email|max:255',
                'nombres'             => 'required|string|max:70',
                'apellido'            => 'required|string|max:70',
                'documento'           => 'required|max:15',
                'fecha_de_nacimiento' => 'required',
                'telefono'            => 'required',
                'pais'                => 'required',
                'provincia'           => 'required',
                'localidad'           => 'required',
                'sexo'                => 'required'
            ]);
        } else {
            if (!is_null($request->input('extranjero'))) {
                $validation = Validator::make($request->all(), [
                    'email'               => 'required|string|email|max:255',
                    'nombres'             => 'required|string|max:70',
                    'apellido'            => 'required|string|max:70',
                    'documento'           => 'required|max:15',
                    'fecha_de_nacimiento' => 'required',
                    'telefono'            => 'required',
                    'sexo'                => 'required'
                ]);
            }
        }

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {

            $item = $this->personRepository->updated($request, $id);
            $notification = [
                'message'    => 'Se ha modificado la persona'.$item->first_name." ".$item->last_name,
                'alert-type' => 'success'
            ];

            return Redirect::to(route('persons.index'))->with($notification);
        }
    }


    public function getPersons()
    {

        return DataTables::of($this->personRepository->getPersons())->addColumn('draws', function (Person $person) {
            return $person->draws->map(function ($draw) {
                return "<p>"."&#8226;".$draw->name."</p>";
            })->implode('');
        })->addColumn('action', function () {
            return $this->btnsPersons();
        })->rawColumns(['draws', 'action'])->make(true);
    }

    public function getPersonasj()
    {
        $persons = Person::all();
        return $persons->toJson();
    }

    public function export()
    {
        Excel::create('Comunidad', function ($excel) {
            $excel->sheet('Participantes', function ($sheet) {
                $people = new Collection;
                $per = Person::with('user:id,email')->with('address:country_id,state_id,district_id')->with('address.district:id,name')->with('address.country:id,name')->with('address.state:id,name')->with('draws:draw_id,name')->where('delete',
                    '=', false)->orderBy('created_at');
                $per->chunk(100, function ($p) use ($people) {
                    foreach ($p as $person) {
                        if (ctype_digit($person->address_id) && !is_null($person->address_id)) {
                            $address = Address::find($person->address()->first()->id);
                            if (!is_null($address)) {
                                if (!is_null($address->district_id)) {
                                    $people->push([
                                        'email'          => $person->user->email,
                                        'first_name'     => $person->first_name,
                                        'last_name'      => $person->last_name,
                                        'identification' => $person->identification,
                                        'birthday'       => $person->birthday,
                                        'telephone'      => $person->telephone,
                                        'extranjero'     => $person->extranjero,
                                        'sex'            => $person->sex,
                                        'country'        => null,
                                        'country_id'     => $address->country_id,
                                        'state'          => null,
                                        'state_id'       => $address->state_id,
                                        'district'       => null,
                                        'district_id'    => $address->district_id,
                                    ]);
                                } else {
                                    $people->push([
                                        'email'          => $person->user->email,
                                        'first_name'     => $person->first_name,
                                        'last_name'      => $person->last_name,
                                        'identification' => $person->identification,
                                        'birthday'       => $person->birthday,
                                        'telephone'      => $person->telephone,
                                        'extranjero'     => $person->extranjero,
                                        'sex'            => $person->sex,
                                        'country'        => null,
                                        'country_id'     => $address->country_id,
                                        'state'          => null,
                                        'state_id'       => $address->state_id,
                                        'district'       => null,
                                        'district_id'    => null,
                                    ]);
                                }

                            }
                        } else {
                            $people->push([
                                'email'          => $person->user->email,
                                'first_name'     => $person->first_name,
                                'last_name'      => $person->last_name,
                                'identification' => $person->identification,
                                'birthday'       => $person->birthday,
                                'telephone'      => $person->telephone,
                                'extranjero'     => $person->extranjero,
                                'sex'            => $person->sex,
                                'country'        => null,
                                'country_id'     => null,
                                'state'          => null,
                                'state_id'       => null,
                                'district'       => null,
                                'district_id'    => null,
                            ]);
                        }
                    }
                });
                $sheet->fromArray($people);
            });
        })->
        download('xls');
    }

    public function destroy($id){
        $person = Person::find($id);
        $this->authorize('destroy', Person::class);

        $person->delete();
        return response()->json(['status' => '200']);
    }

    public function personsj()
    {
        $persons = Person::with('user:id,email')->with('address:country_id,state_id,district_id')->with('address.district:id,name')->with('address.country:id,name')->with('address.state:id,name')->with('draws:draw_id,name')->where('delete',
            '=', false)->orderBy('created_at')->get();

        return $persons->toJson();
    }
}
