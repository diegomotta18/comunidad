<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        return ['email'=> $request->get('email'),'password' =>$request->get('password'),'registration_token' => null];
    }

    protected function authenticated(Request $request, $user)
    {
        if (Auth::guest()){
            return redirect()->route('home');
        }
        if ($user->role == 'user') {// do your margic here
            return redirect()->route('dashboad.user');
        }

        if ($user->role == 'god' || $user->role == 'adm' || $user->role == 'employee') {// do your margic here
            return redirect()->route('dashboad.admin');
        }
    }

    public function logout(Request $request)
    {
        if(!is_null($request->request->get('_token'))){
            $this->guard()->logout();

            $request->session()->invalidate();
            Alert::success('Se ha cerrado la sesión', 'Sesión finalizada')->autoclose(3000);
            return redirect('/');
        }else{
            return redirect('/');

        }
    }
}
