<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailVerification;
use App\Models\Address;
use App\Models\Person;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Alert;
use Ixudra\Curl\Facades\Curl;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
//        dd(!array_key_exists('extranjero', $data));
        if((!array_key_exists('argentino', $data) == true) && (!array_key_exists('extranjero', $data) ==true)) {
            return Validator::make($data, [
                'name' => 'string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'nombres' => 'required|string|max:70',
                'apellido' => 'required|string|max:70',
                'documento' => 'required|max:15',
                'fecha_de_nacimiento' => 'required',
                'telefono' => 'required',
                'términos_y_condiciones' =>'accepted',
                'sexo' => 'required',
                    'extranjero' =>'accepted',
                    'argentino' =>'accepted',
            ]);

        }else{
            if(array_key_exists('extranjero', $data)){
                return Validator::make($data, [
                    'name' => 'string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                    'nombres' => 'required|string|max:70',
                    'apellido' => 'required|string|max:70',
                    'documento' => 'required|max:15',
                    'fecha_de_nacimiento' => 'required',
                    'telefono' => 'required',
                    'términos_y_condiciones' =>'accepted',
                    'sexo' => 'required'
                ]);
            }else{
                return Validator::make($data, [
                    'name' => 'string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                    'nombres' => 'required|string|max:70',
                    'apellido' => 'required|string|max:70',
                    'documento' => 'required|max:15',
                    'fecha_de_nacimiento' => 'required',
                    'telefono' => 'required',
                    'pais' => 'required',
                    'provincia' => 'required',
                    'localidad' => 'required',
                    'términos_y_condiciones' =>'accepted',
                    'sexo' => 'required'
                ]);
            }
        }



    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data);

        $user = User::create([
            'role' => "user",
            'name' => null,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $fecha = date('Y-m-d', strtotime(str_replace('/', '-', $data['fecha_de_nacimiento'])));
        $person =Person::create([
            'first_name' => $data['nombres'],
            'last_name' => $data['apellido'],
            'identification' => $data['documento'],
            'birthday' => $fecha,
            'telephone' => $data['telefono'],
            'sex' =>  $data['sexo'],
            'user_id' => $user->id,
            'email' => $user->email,
        ]);
        if(array_key_exists('extranjero', $data)){
            $person->extranjero = true;
            $person->save();
        }else{
            $address = Address::create([
                'country_id' => $data['pais'],
                'state_id' => $data['provincia'],
                'district_id' => $data['localidad'],
            ]);
            $address->person()->save($person);
        }
        $user->registration_token = str_random(80);
        $user->name = $person->first_name. " ". $person->last_name;
        $user->person_id = $person->id;
        $user->save();

        $url = route('confirmation', $user->registration_token);

        if (session()->has('draw_id')) {
            $value = session('draw_id');
            $user->session = $value[0];
            $user->aux = 'draw_id';
            $user->save();
        }

        if (session()->has('buy_id')) {
            $value = session('buy_id');
            $user->session = $value[0];
            $user->aux = 'buy_id';
            session(['buy_id'=> $value]);

            $user->save();
        }

        Mail::to($user)->queue(new EmailVerification($user,$url,$person));
        $response = Curl::to('http://crm.misionesonline.net/stored')
            ->withData($data)
            ->post();
        return $user;
    }


    protected function getConfirmation($token)
    {
        $user = User::where('registration_token', $token)->first();
        if(!is_null($user)){
            $user->registration_token = null;
            $user->save();
            Alert::info('Se han confirmado los datos de su cuenta, ahora puede ingresar su email y contraseña', 'Aviso')->persistent("Aceptar");
            return redirect()->route('login');
        }else{
            Alert::info('Usted ha confirmado los datos de su cuenta con anterioridad', 'Aviso')->persistent("Aceptar");
            return redirect()->route('login');
        }

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();


        $user = $this->create($request->all());

        Alert::info('Se le ha enviado un e-mail a su casilla de correo eléctronico para confirmar los datos de su cuenta. En caso de no visualizar el email enviado, verifique en la solapa de spam o correos no deseados', 'Confirmar cuenta')->persistent("Aceptar");

        return redirect()->route('login');
    }

}
