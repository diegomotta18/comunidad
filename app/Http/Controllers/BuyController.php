<?php

namespace App\Http\Controllers;

use App\Http\Requests\BuyRequest;
use App\Http\Requests\UpdateBuyRequest;
use App\Models\Buy;
use App\Repositories\BuyRespository;
use App\Repositories\PersonRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use Alert;

class BuyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $buyRespository;
    protected $personRepository;

    public function __construct(BuyRespository $buyRespository, PersonRepository $personRepository)
    {
        $this->middleware('auth');
        $this->buyRespository = $buyRespository;
        $this->personRepository = $personRepository;

    }

    public function index()
    {
        //
        return view('admin.buys.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.buys.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BuyRequest $request)
    {
        //
        $buy = $this->buyRespository->created($request);
        $notification = [
            'message'    => 'Se ha creado un nuevo beneficio '.$buy->name,
            'alert-type' => 'success'
        ];

        return Redirect::to(route('buys.index'))->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $buy = $this->buyRespository->findForId($id);
            return view('admin.buys.show', compact('buy'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('edit', $this->buyRespository->findForId($id));
        //
        if (ctype_digit($id) && !is_null($id)) {
            $buy = $this->buyRespository->findForId($id);
            return view('admin.buys.edit', compact('buy'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBuyRequest $request, $id)
    {
        //
        $buy = $this->buyRespository->updated($request, $id);
        $notification = [
            'message'    => 'Se ha modificado el beneficio '.$buy->name,
            'alert-type' => 'success'
        ];

        return Redirect::to(route('buys.index'))->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $this->buyRespository->destroy($id);
        }
    }

    public function active($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $buy = $this->buyRespository->findForId($id);
            $buy->status = true;
            $buy->save();
        }
    }

    public function desactive($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $buy = $this->buyRespository->findForId($id);
            $buy->status = false;
            $buy->save();
        }
    }


    public function getBuys()
    {
        $buys = $this->buyRespository->getBuys();

        return DataTables::eloquent($buys)
                         ->addColumn('action', function () {
                             return $this->btnsBuys();
                         })
                         ->make(true);
    }


    public function btnsBuys()
    {

        return ' 
        <div style="display: flex;">
            <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                    <li><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
                    '.$this->canEditBuy().'
                    <li><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>
                <a data-toggle="modal" data-target="#modalParticipantsb" class="btn  btn-info  participantsb " ><i class="fa fa-users"></i></a>

            </div>
        </div>';
    }

    /*
     * Recupera todos los beneficios tanto inactivos o activos ordenados por fechas de creacion,
     * se muestra en el front-end del administrador
     *
     * */
    public function canEditBuy()
    {

        if (Gate::allows('edit', Buy::class)) {
            return '<li><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i class="glyphicon glyphicon-edit"></i> Editar</a></li>';
        }

        return '';
    }

    public function addVoucherToPerson(Request $request)
    {
        $person = $this->personRepository->find($request->input('person_id'));
        if ($this->buyRespository->exitsPerson($person, $request->input('buy_id'))) {
            Alert::warning('Usted ya accedió a este voucher', 'Aviso')->autoclose(4000);;

            return Redirect::to($request->session()->previousUrl());
        }
        $this->buyRespository->join($request, $person);

        Alert::success('Usted puede acceder al código de descuento', 'Listo')->autoclose(4000);;

        return Redirect::to($request->session()->previousUrl());
    }

    /*
     * Recupera todos los participantes de un determinado beneficio
     * */
    public function getParticipants($id)
    {
        return $this->buyRespository->getParticipants($id);
    }

    public function participants($id)
    {
        //
        $buy = $this->buyRespository->find($id);
        return view('admin.buys.participants', compact('buy'));
    }

}
