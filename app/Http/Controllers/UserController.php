<?php

namespace App\Http\Controllers;

use App\Mail\EmailNotificationUser;
use App\Mail\UserNotification;
use App\Repositories\PersonRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Excel;

class UserController extends Controller
{

    protected $user, $personRepository;

    public function __construct(UserRepository $user, PersonRepository $personRepository)
    {
        $this->middleware('auth');

        $this->user = $user;
        $this->personRepository = $personRepository;
    }

    public function index()
    {
        return view('admin.users.list');
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function edit($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $user = $this->user->findForId($id);

            return view('admin.users.edit', compact('user'));
        } else {
            return Redirect::to(route('users.index'));
        }
    }


    protected function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nombre'     => 'required|max:255',
            'email'      => 'required|email|max:255|unique:users',
            'rol'        => 'required',
            'contraseña' => 'required|min:6',
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {
            $item = $this->user->created($request);
            $notification = [
                'message'    => 'Se ha creado el usuario '.$item->name,
                'alert-type' => 'success'
            ];
            Mail::to($item)->queue(new UserNotification($item));

            return Redirect::to(route('users.index'))->with($notification);
        }
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nombre'     => 'required|max:255',
            'email'      => 'required|email|max:255',
            'rol'        => 'required',
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());

        } else {
            $item = $this->user->updated($request, $id);
            $notification = [
                'message'    => 'Se ha modificado los datos del usuario '.$item->name,
                'alert-type' => 'success'
            ];

            return Redirect::to(route('users.index'))->with($notification);
        }
    }

    public function destroy($id)
    {
        //
        if (ctype_digit($id) && !is_null($id)) {
            $this->user->destroyed($id);
        }
    }


    public function getUsers()
    {
        return $this->user->getUsers();
    }

    public function show($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $person = $this->personRepository->findForUserId($id);
            $person = $person->first();
            $user = $this->user->findForId($id);

            return view('admin.users.show', compact('person', 'user'));
        } else {
            return Redirect::to(route('users.index'));
        }
    }


    public function export()
    {
        Excel::create('Usuarios', function ($excel) {
            $excel->sheet('Usuarios', function ($sheet) {

                $sheet->fromArray(User::all());

            });
        })->download('xls');
    }
}
