<?php

namespace App\Http\Controllers;

use App\Models\Buy;
use App\Repositories\BuyRespository;
use App\Repositories\DrawRepository;
use App\Repositories\PersonRepository;

use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class JoinController extends Controller
{
    //
    protected $drawRepository, $personRepository,$buyRepository;

    public function __construct(PersonRepository $personRepository,BuyRespository $buyRepository, DrawRepository $drawRepository)
    {
        $this->personRepository = $personRepository;
        $this->drawRepository = $drawRepository;
        $this->buyRepository = $buyRepository;

        $this->middleware('auth');

    }

    public function participar(Request $request)
    {
        $person = $this->personRepository->find($request->input('person_id'));
        if ($this->drawRepository->exitsPerson($person, $request->input('draw_id'))) {
            Alert::warning('Usted ya se encuentra participando en este sorteo', 'Aviso')->autoclose(3000);;

            return Redirect::to($request->session()->previousUrl());
        }
        $this->drawRepository->join($request, $person);

        Alert::success('Usted se encuentra participando en este sorteo', 'Listo')->autoclose(3000);;

        return Redirect::to($request->session()->previousUrl());
    }

    public function addPersonToBuy(Request $request){
        $person = $this->personRepository->find($request->input('person_id'));
        $buy = Buy::find($request->input('buy_id'));
        $this->buyRepository->joinBuy($buy, $person);
        return \redirect()->to($buy->url);
    }


}
