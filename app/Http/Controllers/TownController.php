<?php

namespace App\Http\Controllers;

use App\Models\Town;
use App\Repositories\TownRepository;
use Illuminate\Http\Request;

class TownController extends Controller
{
    //
    protected $townRep;

    public function __construct(TownRepository $townRep)
    {
        $this->townRep = $townRep;


    }

    public function getTowns($request)
    {
        if ($request != 'undefined') {
            $towns = Town::where('district_id', '=', $request)->get();
            if ($towns) {
                return $towns;
            }
        }

        return [];
    }
}
