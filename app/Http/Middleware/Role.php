<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    protected $hierarchy = [
        'god' => 4,
        'adm' => 3,
        'user' => 2,
        'employee' =>1,
        'viewer' =>0,
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = auth()->user();
        if ($this->hierarchy[$user->role] < $this->hierarchy[$role])
        {
            abort(404);
            //return $next($request);
        }
        return $next($request);
    }


}