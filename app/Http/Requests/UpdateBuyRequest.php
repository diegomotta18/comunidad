<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBuyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'nombre'                   => 'required|max:180',
            'descripcion'              => 'required',
            'metadescription'          => 'required|max:180',
            'descripcion_de_la_imagen' => 'required|max:180',
            'url'                      => 'required',
            'imagen'                   => 'mimes:jpeg,bmp,png',
            'precio_total'             => 'required',
            'precio_descontado'        => 'required|menor_que:precio_total',
            'type_date'                => 'required',
            'periodo_del_beneficio'    => 'required_if:type_date.0,==,periodo',
            'fecha_de_vencimiento'     => 'required_if:type_date.0,==,especifico',
            'hora_de_vencimiento'      => 'required_if:type_date.0,==,especifico',
            'codigo_de_vaucher'        => 'required_if:type_date.0,==,especifico|max:10',

        ];
    }
}
