<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                => 'string|max:255',
            'email'               => 'required|string|email',
            'password'            => 'required|string|min:6|confirmed',
            'nombres'             => 'required|string|max:70',
            'apellido'            => 'required|string|max:70',
            'documento'           => 'required|max:15',
            'fecha_de_nacimiento' => 'required',
            'telefono'            => 'required',
            'sexo'                => 'required',
            'nacionalidad'        => 'required',
//            'pais' => 'required_if:nacionalidad, argentino',
            'provincia'           => 'required_if:nacionalidad, argentino',
            'localidad'           => 'required_if:nacionalidad, argentino',
        ];
    }
}
