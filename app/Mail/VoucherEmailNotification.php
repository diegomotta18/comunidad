<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoucherEmailNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $voucher;
    public $person;
    public $date;
    public function __construct($voucher, $person,$date)
    {
        //
        $this->voucher = $voucher;
        $this->person = $person;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Voucher de la Comunidad de Misiones Online')
                    ->view('auth.email.voucheremailnotification');
    }

}
