<?php

namespace App\Repositories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use DataTables;
use Illuminate\Support\Facades\Validator;

class UserRepository extends BaseRepository
{
    /**
     * @param User $items
     */
    public function __construct(User $items)
    {
        $this->items = $items;
    }

    public function created( $request)
    {

        $user = User::create([
            'name' => $request->input('nombre'),
            'email' => $request->input('email'),
            'role' => $request->input('rol'),
            'password' => bcrypt($request->input('contraseña')),
        ]);
        if(!is_null($request->input('persona'))){
            $persona = Person::find($request->input('persona'));

            $persona->user_id = $user->id;
            $persona->save();

            $user->person_id = $persona->id;
        }

        $user->save();

        return $user;

    }

    public function updated($request, $id)
    {
        $user = $this->find($id);
        $personar = $user->person()->first();
        if(!is_null($personar)){
            $personar->user_id = null;
            $personar->save();
        }
        if(!is_null($request->input('persona'))){
            $persona = Person::find($request->input('persona'));
            $user->person_id = $persona->id;
            $persona->user_id = $user->id;
            $persona->save();
        }
        $user->name = $request->input('nombre');
        $user->email = $request->input('email');
        $user->role = $request->input('rol');
        if(!is_null($request->input('contraseña'))){
            $user->password = bcrypt($request->input('contraseña'));
        }
        $user->save();

        return $user;
    }

    public function findForId($id)
    {
        return $this->findFromQuery($this->items(), $id);
    }

    public function destroyed($id)
    {
        $this->delete($id,true);
        return response()->json([
            'success' => 'ok',
        ], 200);
    }

    public function btnsUsers()
    {
        return ' 
        <div style="display: flex;">
                    <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                    <li><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
                    <li><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

            </div>
        </div>';
    }


    public function getUsers()
    {

        return DataTables::eloquent(User::with('person')->orderBy('created_at'))
            ->addColumn('action', function () {
                return $this->btnsUsers();
            })->make(true);
    }
}