<?php

namespace App\Repositories;

use App\Models\Draw;
use App\Models\Option;
use Illuminate\Database\Eloquent\Model;
use App\Models\Question;
use Illuminate\Support\Facades\Request;

class QuestionRepository extends BaseRepository
{
    	/**
	 * @param Question $items
	 */
	public function __construct(Question $items)
	{
		$this->items = $items;
	}


    public function find($id)
    {
        return $this->findFromQuery($this->items(), $id);
    }

    public function findAll($id){
        {
            $question = Question::where('draw_id',$id)->where('delete',false)->orderBy('created_at','desc')->get();
            return $question;
        }
    }

    public function created($request)
    {

        $question = Question::create([
            'draw_id' => $request->input('draw_id'),
            'input_type' => $request->input('tipo_de_respuesta'),
            'question' => $request->input('pregunta')
        ]);

        $options = $request->input('respuestas');
        foreach ($options as $option){
            if(!is_null($option)){

                $opt =Option::create([
                    'label_option'=> $option,
                ]);
                $question->options()->save($opt);
            }
        }
        $draw = Draw::find($request->input('draw_id')) ;
        $draw->questions()->save($question);
        return $question;
    }

    public function updated($request,$id){

	    $question = $this->find($id);
	    $question->input_type = $request->input('tipo_de_respuesta');
	    $question->question = $request->input('pregunta');
	    $question->save();
	    if($question->options->count() >0){
            $question->options()->detach();
            $question->save();
        }
        $options = $request->input('respuestas');
        foreach ($options as $option){
            if(!is_null($option)){
                $opt =Option::create([
                    'label_option'=> $option,
                ]);
                $question->options()->save($opt);
            }
        }
        return $question;
    }


    /*
     * Eliminacion logica del cuestionario
     * */
    public function deleted($request){
        $quest = $this->find($request->input('question_id'));
        $quest->options()->detach();
        $quest->delete = true;
        $quest->save();
        return $quest;
    }


}