<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\District;

class DistrictRepository extends BaseRepository
{
    	/**
	 * @param District $items
	 */
	public function __construct(District $items)
	{
		$this->items = $items;
	}
}