<?php

namespace App\Repositories;

use App\Models\Address;
use App\Models\Country;
use App\Models\District;
use App\Models\Person;
use App\Models\State;
use DataTables;
use Illuminate\Database\Eloquent\Collection;

class PersonRepository extends BaseRepository
{
    /**
     * @param Person $items
     */
    public function __construct(Person $items)
    {
        $this->items = $items;
    }

    public function find($id)
    {
        return $this->findFromQuery($this->items(), $id);
    }


    public function updated( $request, $id)
    {
        $item = $this->find($id);
        $address = $item->address_id;
        $attributes = [
            'first_name' => $request->input('nombres'),
            'last_name' => $request->input('apellido'),
            'identification' => $request->input('documento'),
            'birthday' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
            'telephone' => $request->input('telefono'),
            'email' => $request->input('email'),
            'sex' => $request->input('sexo')
        ];

        if(!is_null($address)){
            $addressperson = Address::find($address);
            if (!is_null($addressperson)){
                $addressperson->country_id = 1;
                $addressperson->state_id = $request->input('provincia');
                $addressperson->district_id = $request->input('localidad');
                $addressperson->save();
            }
        }
        $item = $this->update($item, $attributes);
        if ($request->has('extranjero')){
            $item->extranjero = true;
            if (!is_null($address)){
                $item->address()->dissociate();
            }
            $item->save();

        }else if ($request->has('argentino')){
            $item->extranjero = false;
            if (is_null($address)){
                $addressn = New Address();
                $addressn->country_id = 1;
                $addressn->state_id = $request->input('provincia');
                $addressn->district_id = $request->input('localidad');
                $addressn->save();
                $item->address_id = $addressn->id;

            }
            $item->save();
        }
        return $item;
    }


    public function created($request)
    {
        $attributes = [
            'first_name' => $request->input('nombres'),
            'last_name' => $request->input('apellido'),
            'identification' => $request->input('documento'),
            'birthday' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
            'telephone' => $request->input('telefono'),
            'email' => $request->input('email'),
            'sex' => $request->input('sexo')
        ];
        if ($request->has('extranjero')){
            $attributes = array_merge($attributes,array('extranjero'=>true));
        }else if ($request->has('argentino')){
            $attributes =array_merge($attributes,array('extranjero'=>false));
            $addressperson = new Address();
            $addressperson->country_id = $request->input('pais');
            $addressperson->state_id = $request->input('provincia');
            $addressperson->district_id = $request->input('localidad');
            $addressperson->save();
            $attributes =array_merge($attributes,array('address_id'=>$addressperson->id));
        }
        $item = $this->create($attributes);
        return $item;
    }

    public function findForUserId($id){
        $persona = Person::with('user','address:country_id,state_id,district_id')->where('user_id','=',$id)->where('delete','=',false)->where('status','=',true);
        return $persona;
    }

    public function findForId($id)
    {
        $persona = Person::with('user','address:id,country_id,state_id,district_id')->with('draws:draw_id,name')->where('id','=',$id)->where('delete','=',false)->where('status','=',true)->first();
        return $persona;
    }


    public function getPersons()
    {
        $persons = Person::with('user:id,email')->with('address:country_id,state_id,district_id')->with('address.district:id,name')->with('address.country:id,name')->with('address.state:id,name')->with('draws:draw_id,name')->where('delete','=',false)->orderBy('created_at')->paginate(10);
        return $persons;
    }
}