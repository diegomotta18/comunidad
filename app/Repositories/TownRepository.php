<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Town;

class TownRepository extends BaseRepository
{
    	/**
	 * @param Town $items
	 */
	public function __construct(Town $items)
	{
		$this->items = $items;
	}
}