<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\State;

class StateRepository extends BaseRepository
{
    	/**
	 * @param State $items
	 */
	public function __construct(State $items)
	{
		$this->items = $items;
	}
}