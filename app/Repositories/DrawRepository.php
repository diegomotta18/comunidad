<?php

namespace App\Repositories;

use App\Mail\DrawNotification;
use App\Models\Country;
use App\Models\District;
use App\Models\Option;
use App\Models\Person;
use App\Models\Response;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Draw;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;

/*
 *  Repositorio de los sorteos
 *  Funciones:
 *              Crear sorteos
 *              Modiciar sorteos
 *              Buscar sorteos
 *              Recuperar sorteos
 *              Recuperar participantes por sorteo
 *              Existe el participante dentro del sorteo
 *              Activar y desactivar la aparición del sorteo en la home
 * */


class DrawRepository extends BaseRepository
{

    protected $items;

    public function __construct(Draw $items)
    {
        $this->items = $items;
    }

    public function created(Request $request)
    {
        $fechas = explode(" - ", $request->input('periodo_de_participacion'));
        $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
        $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
        $imagen = null;
        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen')->store('public');

        }
        $attributes = ['name' => $request->input('nombre'),
            'description' => $request->input('descripcion'),
            'metadescription' => $request->input('metadescription'),
            'date_init' => $fecha_inicio,
            'date_finish' => $fecha_fin,
            'date_result' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_resultado')))),
            'avatar' => $imagen,
            'alt' => $request->input('descripcion_de_la_imagen')];
        $item = $this->create($attributes);
        $slug = str_slug($request->input('nombre')) . "-{$item->id}";
        $item->slug = $slug;
        $item->save();
        return $item;
    }

    public function updated(Request $request, $id)
    {
        $item = $this->find($id);
        $fechas = explode(" - ", $request->input('periodo_de_participacion'));
        $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
        $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
        $imagen = null;
        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen')->store('public');
        }
        $attributes = ['name' => $request->input('nombre'),
            'description' => $request->input('descripcion'),
            'metadescription' => $request->input('metadescription'),
            'date_init' => $fecha_inicio,
            'date_finish' => $fecha_fin,
            'date_result' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_resultado')))),
            'alt' => $request->input('descripcion_de_la_imagen')];
        if (!is_null($imagen)) {
            $attributes = array_merge($attributes, array('avatar' => $imagen));
        }

        $item = $this->update($item, $attributes);
        $item->slug = str_slug($request->input('nombre')) . "-{$item->id}";
        $item->save();
        return $item;
    }

    public function destroy($id)
    {
        $item = $this->findForId($id);
        $item->delete = true;
        $item->save();
        return response()->json([
            'success' => 'ok',
        ], 200);
    }

    public function findForSlug($slug)
    {
        return $this->findBySlug($slug);
    }

    public function findForId($id)
    {
        return $this->findFromQuery($this->items(), $id);
    }


    public function btnsDraws()
    {
        if (Auth::user()->canSeeGAView())
            return ' 
        <div style="display: flex;">
                    <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                
                    <li><a data-toggle="modal" data-target="#modalAssing"class="btn btn-xs assing"><i class="fa fa-list-alt"></i> Cuestionario</a></li>
                    <li><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
                    <li><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>
            <a data-toggle="modal" data-target="#modalParticipants" class="btn  btn-info  participants " ><i class="fa fa-users"></i></a>
            </div>
        </div>';
        else if (Auth::user()->canSeeEView()) {
            return ' <div style="display: flex;">
                    <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                    <li><a data-toggle="modal" data-target="#modalAssing"class="btn btn-xs assing"><i class="fa fa-list-alt"></i> Cuestionario</a></li>
                    <li><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
        
                </ul>
            <a data-toggle="modal" data-target="#modalParticipants" class="btn  btn-info  participants " ><i class="fa fa-users"></i></a>
            </div>
        </div>';
        }
    }

    /*
     * Recupera todos los concursos tanto inactivos o activos ordenados por fechas de creacion,
     * se muestra en el front-end del administrador
     *
     * */


    public function getDraws()
    {
        return DataTables::eloquent(Draw::where('delete', '=', false)->orderBy('created_at', 'desc'))
            ->addColumn('action', function () {
                return $this->btnsDraws();
            })->make(true);
    }


    /*
     * Une al participante con el sorteo seleccionado
     * */
    public function join(Request $request, Person $person)
    {
        $draw = Draw::where('id',($request->input('draw_id')))->first();
        $user = $person->user;
        if ($draw->questions()->count() > 0) {
            foreach ($draw->questions()->get() as $q) {
                if (!is_null($request->input('option' . $q->id))) {
                    $options = $request->input('option' . $q->id);
                    if (!is_array($options)) {
                        $person->responses()->save(Response::create([
                            'draw_id' => $draw->id,
                            'question_id' => $q->id,
                            'resp' => $request->input('option' . $q->id),
                            'person_id' => $person->id
                        ]));
                    } else {
                        foreach ($options as $option) {
                            $person->responses()->save(Response::create([
                                'draw_id' => $draw->id,
                                'question_id' => $q->id,
                                'resp' => $option,
                                'person_id' => $person->id
                            ]));
                        }
                    }
                }
            }
        }

        Mail::to($user)->queue(new DrawNotification($draw, $person));
        $draw->persons()->save($person);
        return $draw;
    }

    public function joinj(Request $request, Person $person)
    {
        $draw = Draw::with('questions')->find($request->input('draw_id'));
        $user = $person->user;
        if ($draw->questions()->count() > 0) {

            $options = $request->input('option');
            if (!is_array($options)) {
                $option = Option::with('questions')->find($request->input('option'));
                $person->responses()->save(Response::create([
                    'draw_id' => $draw->id,
                    'question_id' => $option->questions()->first()->id,
                    'resp' => $option->label_option,
                    'person_id' => $person->id
                ]));
            } else {
                foreach ($options as $option) {
                    $opt = Option::with('questions')->find($option);
                    $person->responses()->save(Response::create([
                        'draw_id' => $draw->id,
                        'question_id' => $opt->questions()->first()->id,
                        'resp' => $opt->label_option,
                        'person_id' => $person->id
                    ]));
                }
            }

        }

        Mail::to($user)->queue(new DrawNotification($draw, $person));
        $draw->persons()->save($person);
        return $draw;
    }

    public function compareDate(Request $request)
    {
        $fecha_actual = Carbon::now()->format('Y-m-d');
        $draw = $this->find($request->input('draw_id'));

        $fecha_finish = date('Y-m-d', strtotime(str_replace('/', '-', $draw->date_finish)));

        if ($fecha_finish <= $fecha_actual) {
            return true;
        }
        return false;
    }

    public function exitsPerson($person, $draw_id)
    {
        $draw = $this->find($draw_id);
        return $draw->persons->contains($person);
    }

    public function getParticipants($draw_id)
    {

        $persons = $this->find($draw_id);

        $people = new Collection;

        foreach ($persons->persons as $person) {

            if ($person->responsesForDraw($draw_id)->count() > 0) {
                $people->push([
                    'first_name' => $person->first_name,
                    'last_name' => $person->last_name,
                    'identification' => $person->identification,
                    'birthday' => $person->birthday,
                    'telephone' => $person->telephone,
                    'email' => $person->user->email,
                    'respuestas' => $person->responsesForDraw($draw_id)->get(),
                ]);
            } else {
                $people->push([
                    'first_name' => $person->first_name,
                    'last_name' => $person->last_name,
                    'identification' => $person->identification,
                    'birthday' => $person->birthday,
                    'telephone' => $person->telephone,
                    'email' => $person->user->email,
                    'respuestas' => null,
                ]);
            }

        }
        return DataTables::of($people)->make(true);

    }

    public
    function allDraw()
    {
        $draws = Draw::with('questions.options')->where('delete', '=', false)->where('status', '=', true)->orderBy('created_at', 'desc')->get();
        return $draws;
    }

    public
    function allDrawPaginate()
    {
        $draws = Draw::where('delete', '=', false)->where('status', '=', true)->orderBy('created_at', 'desc')->paginate(4);

        return $draws;
    }

    public
    function active($id)
    {
        $item = $this->findForId($id);
        $item->status = true;
        $item->save();
        return response()->json([
            'success' => 'ok',
        ], 200);
    }

    public
    function desactive($id)
    {
        $item = $this->findForId($id);
        $item->status = false;
        $item->save();
        return response()->json([
            'success' => 'ok',
        ], 200);
    }

    public
    function exported($id)
    {
        $draw = $this->findForId($id);
        Excel::create('Comunidad', function ($excel) use ($draw) {
            $excel->sheet('Participantes', function ($sheet) use ($draw) {
                $people = new Collection;
                foreach ($draw->persons as $person) {
                    if (!is_null($person->address)) {
                        $country = Country::find($person->address->country_id);
                        $state = State::find($person->address->state_id);
                        $district = District::find($person->address->district_id);
                        if (!is_null($person->address->district_id)) {
                            $people->push([
                                'email' => $person->user->email,
                                'first_name' => $person->first_name,
                                'last_name' => $person->last_name,
                                'identification' => $person->identification,
                                'birthday' => $person->birthday,
                                'telephone' => $person->telephone,
                                'extranjero' => $person->extranjero,
                                'sex' => $person->sex,
                                'country' => $country->name,
                                'country_id' => $country->id,
                                'state' => $state->name,
                                'state_id' => $state->id,
                                'district' => $district->name,
                                'district_id' => $district->id,
                                'respuestas' => $person->responsesForDraw($draw->id)->get()
                            ]);
                        } else {
                            $people->push([
                                'email' => $person->user->email,
                                'first_name' => $person->first_name,
                                'last_name' => $person->last_name,
                                'identification' => $person->identification,
                                'birthday' => $person->birthday,
                                'telephone' => $person->telephone,
                                'extranjero' => $person->extranjero,
                                'sex' => $person->sex,
                                'country' => $country->name,
                                'country_id' => $country->id,
                                'state' => $state->name,
                                'state_id' => $state->id,
                                'district' => null,
                                'district_id' => null,
                                'respuestas' => $person->responsesForDraw($draw->id)->get()
                            ]);
                        }

                    } else {
                        $people->push([
                            'email' => $person->user->email,
                            'first_name' => $person->first_name,
                            'last_name' => $person->last_name,
                            'identification' => $person->identification,
                            'birthday' => $person->birthday,
                            'telephone' => $person->telephone,
                            'extranjero' => $person->extranjero,
                            'sex' => $person->sex,
                            'country' => null,
                            'country_id' => null,
                            'state' => null,
                            'state_id' => null,
                            'district' => null,
                            'district_id' => null,
                            'respuestas' => $person->responsesForDraw($draw->id)->get()->toArray()
                        ]);
                    }

                }
                $sheet->fromArray($people);

            });
        })->download('xls');
    }
}