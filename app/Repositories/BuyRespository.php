<?php

namespace App\Repositories;

use App\Mail\VoucherEmailNotification;
use App\Mail\VoucherNotification;
use App\Models\BuyPerson;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Buy;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class BuyRespository extends BaseRepository
{

    public function __construct(Buy $items)
    {
        $this->items = $items;
    }

    public function created($request)
    {

        $imagen = null;
        $buy = null;
        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen')->store('public');
        }
        if ($request->input('type_date')[0] === "especifico") {
            $buy = Buy::create([
                'name'                 => $request->input('nombre'),
                'description'          => $request->input('descripcion'),
                'metadescription'      => $request->input('metadescription'),
                'date_specific'        => Carbon::createFromFormat('d/m/Y', $request->input('fecha_de_vencimiento')),
                'time_specific'        => Carbon::createFromFormat('g:i A', $request->input('hora_de_vencimiento')),
                'avatar'               => $imagen,
                'alt'                  => $request->input('descripcion_de_la_imagen'),
                'url'                  => $request->input('url'),
                'price_total'          => $request->input('precio_total'),
                'percent'              => $request->input('descuento'),
                'price_discount'       => $request->input('precio_descontado'),
                'check_date_especific' => true,
                'date_init'            => null,
                'date_finish'          => null,
                'code'                 => $request->input('codigo_de_vaucher')
            ]);
        } elseif ($request->input('type_date')[0] === "periodo") {
            $fechas = explode(" - ", $request->input('periodo_del_beneficio'));
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
            $buy = Buy::create([
                'name'              => $request->input('nombre'),
                'description'       => $request->input('descripcion'),
                'metadescription'   => $request->input('metadescription'),
                'date_init'         => $fecha_inicio,
                'date_finish'       => $fecha_fin,
                'avatar'            => $imagen,
                'alt'               => $request->input('descripcion_de_la_imagen'),
                'url'               => $request->input('url'),
                'price_total'       => $request->input('precio_total'),
                'percent'           => $request->input('descuento'),
                'price_discount'    => $request->input('precio_descontado'),
                'check_date_period' => true,
                'code'              => null
            ]);
        }

        $slug = str_slug($request->input('nombre'))."-{$buy->id}";
        $buy->slug = $slug;
        $buy->save();

        return $buy;
    }

    public function updated($request, $id)
    {
        $buy = Buy::find($id);
        $fecha_inicio = null;
        $fecha_fin = null;
        if (!is_null($request->input('periodo_del_beneficio'))) {
            $fechas = explode(" - ", $request->input('periodo_del_beneficio'));
            $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
            $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
        }

        $imagen = null;
        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen')->store('public');
        }
        if ($request->input('type_date')[0] === "especifico") {
            $buy->check_date_especific = true;
            $buy->check_date_period = false;
            $buy->date_specific = Carbon::createFromFormat('d/m/Y', $request->input('fecha_de_vencimiento'));
            $buy->time_specific = Carbon::createFromFormat('g:i A', $request->input('hora_de_vencimiento'));
            $buy->date_init = null;
            $buy->date_finish = null;
            $buy->code = $request->input('codigo_de_vaucher');
        } elseif ($request->input('type_date')[0] === "periodo") {
            $buy->check_date_especific = false;
            $buy->check_date_period = true;
            $buy->date_specific = null;
            $buy->time_specific = null;
            $buy->date_init = $fecha_inicio;
            $buy->date_finish = $fecha_fin;
            $buy->code = null;
        }

        $buy->name = $request->input('nombre');
        $buy->description = $request->input('descripcion');
        $buy->metadescription = $request->input('metadescription');
        $buy->price_total = $request->input('precio_total');
        $buy->percent = $request->input('descuento');
        $buy->price_discount = $request->input('precio_descontado');
        if ($request->hasFile('imagen')) {
            $buy->avatar = $imagen;
        }
        $buy->alt = $request->input('descripcion_de_la_imagen');
        $buy->url = $request->input('url');
        $buy->save();

        return $buy;
    }

    public function destroy($id)
    {

        $buy = $this->findForId($id);
        $buy->delete = true;
        $buy->save();

        return $buy;
    }

    public function findForSlug($slug)
    {
        return $this->findBySlug($slug);
    }

    public function findForId($id)
    {
        return $this->findFromQuery($this->items(), $id);
    }


    public function getBuys()
    {
        return Buy::with('persons')->where('delete', '=', false)->orderBy('created_at', 'desc');
    }


    public function allBuy()
    {
        $buy = Buy::where('delete', '=', false)->where('status', '=', true)->orderBy('created_at', 'desc')->get();

        return $buy;
    }

    public function allBuyPaginate()
    {
        $buy = Buy::where('delete', '=', false)->where('status', '=', true)->orderBy('created_at', 'desc')->paginate(4);

        return $buy;
    }

    public function exitsPerson($person, $voucher_id)
    {
        $voucher = $this->find($voucher_id);

        return $voucher->persons->contains($person);
    }

    public function join(Request $request, Person $person)
    {
        $voucher = $this->find($request->input('buy_id'));
        $user = $person->user;
        $voucher->persons()->save($person);
        $buy_person = BuyPerson::where('person_id',$person->id)->where('buy_id',$voucher->id)->first();
        Mail::to($user)->queue(new VoucherEmailNotification($voucher, $person, $buy_person->created_at));
        return $voucher;
    }

    public function joinBuy(Buy $buy, Person $person){
        $voucher = $this->find($buy->id);
        $voucher->persons()->save($person);
        return $voucher;
    }

    public function getParticipants($buy_id)
    {

        $persons = $this->find($buy_id);
        $people = new Collection();

        foreach ($persons->persons as $person) {

                $people->push([
                    'first_name' => $person->first_name,
                    'last_name' => $person->last_name,
                    'identification' => $person->identification,
                    'birthday' => $person->birthday,
                    'telephone' => $person->telephone,
                    'email' => $person->user->email,
                    'respuestas' => null,
                ]);
            }


        return DataTables::of($people)->make(true);

    }
}