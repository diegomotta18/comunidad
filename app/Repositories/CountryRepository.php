<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country;

class CountryRepository extends BaseRepository
{
    	/**
	 * @param Country $items
	 */
	public function __construct(Country $items)
	{
		$this->items = $items;
	}
}