<?php

namespace App\Notifications;

use App\Models\Person;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    use Queueable;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        //
        $this->token = $token;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $person = User::find($notifiable->id);

        return (new MailMessage)

                    ->subject('Solicitud de reestablecimiento de contraseña')
                    ->greeting('Hola, '. $person->name)
                    ->line('Recibiste este email porque se solicitó un restablecimiento de contraseña para su cuenta')
                    ->action('Reestablecer contraseña',  url(config('app.url').route('password.reset', $this->token, false)))
                    ->line('Si no realizaste esta petición, puedes ignorar este correo')
                    ->salutation("Saludos. Comunidad de Misiones Online");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
