<?php

namespace App\Console\Commands;

use App\Models\Draw;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DesactiveDraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draws:desactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desactiva los concursos fuera de fecha';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $draws = Draw::all();
        foreach ($draws->chunk(10) as $t) {
            foreach ($t as $draw) {
                if ($draw->isExpired()) {
                    $draw->status = false;
                    $draw->save();
                }
            }
        }

        $this->info('Concursos desactivados');

    }
}
