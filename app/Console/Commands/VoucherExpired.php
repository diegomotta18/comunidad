<?php

namespace App\Console\Commands;

use App\Models\Buy;
use Illuminate\Console\Command;

class VoucherExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:voucherexpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command look for voucher expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $vouchers = Buy::where('date_specific','<>',null)->get();
        foreach ($vouchers->chunk(10 ) as $t) {
            foreach ($t as $voucher) {
                if ($voucher->isExpired()) {
                    $v = Buy::find($voucher->id);
                    $v->status = false;
                    $v->save();
                }
            }

        }

        $this->info('Finish check expired voucher');
    }
}
