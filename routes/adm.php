<?php
//Route::get('adminhome', 'HomeController@dashboadAdmin')->name('dashboad.admin');
//
////sorteos
//Route::get('draws','DrawController@index')->name('draws.index');
//Route::post('draws','DrawController@store')->name('draws.store');
//Route::get('draws/create','DrawController@create')->name('draws.create');
//Route::get('draws/{draw}','DrawController@show')->name('draws.show');
//Route::put('draws/{draw}','DrawController@update')->name('draws.update');
//Route::delete('draws/{draw}','DrawController@destroy')->name('draws.destroy');
//Route::get('draws/{draw}/edit','DrawController@edit')->name('draws.edit');
//Route::get('drawsdt','DrawController@getDraws');
//Route::get('particantsdt/{draw}','DrawController@getParticipants');
//Route::get('draw/{draw}/participants','DrawController@participants')->name('participants');
//Route::get('draw/export/{draw}','DrawController@export')->name('draws.export');
//Route::post('draws/active/{draw}','DrawController@active')->name('draws.active');
//Route::post('draws/desactive/{draw}','DrawController@desactive')->name('draws.desactive');
////preguntas
//Route::get('draws/{id}/questions','QuestionController@index')->name('question.index');
//Route::post('draws/questions/store','QuestionController@store')->name('question.store');
//Route::put('draws/questions/{id}/update','QuestionController@update')->name('question.update');
//Route::get('draws/{draw_id}/questions/{question_id}','QuestionController@edit')->name('question.edit');
//Route::post('option/{id}','QuestionController@destroyOption')->name('option.destroy');
//Route::post('question/delete','QuestionController@deleted')->name('question.destroy');
////compras
//Route::get('buys','BuyController@index')->name('buys.index');
//Route::post('buys','BuyController@store')->name('buys.store');
//Route::get('buys/create','BuyController@create')->name('buys.create');
//Route::get('buys/{id}','BuyController@show')->name('buys.show');
//Route::put('buys/{id}','BuyController@update')->name('buys.update');
//Route::delete('buys/{id}','BuyController@destroy')->name('buys.destroy');
//Route::get('buys/{id}/edit','BuyController@edit')->name('buys.edit');
//Route::get('buysdt','BuyController@getBuys');
//Route::post('buys/active/{id}','BuyController@active');
//Route::post('buys/desactive/{id}','BuyController@desactive');
//
////abm personas
//Route::get('/persons/index', 'PersonController@index' )->name('persons.index');
//Route::get('persons', 'PersonController@getPersons' );
//Route::get('persons/create', 'PersonController@create' )->name('persons.create');
//Route::get('persons/{id}', 'PersonController@show' )->name('persons.show');
//Route::post('persons/store', 'PersonController@store' )->name('persons.store');
//Route::get('persons/{id}/edit', 'PersonController@edit' )->name('persons.edit');
//Route::put('persons/{id}/update', 'PersonController@update' )->name('persons.update');
//Route::delete('persons/{id}/', 'PersonController@destroy' )->name('persons.destroy');
//Route::get('/personsj', 'PersonController@getPersonasj');
//Route::get('persons/export', 'PersonController@export' )->name('persons.export');
