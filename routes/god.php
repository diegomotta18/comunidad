<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 26/9/17
 * Time: 09:47
 */
//usuarios
Route::get('users/export','UserController@export')->name('users.export');
Route::get('/users/index', 'UserController@index' )->name('users.index');
Route::get('users', 'UserController@getUsers' );
Route::get('users/create', 'UserController@create' )->name('users.create');
Route::get('users/{id}', 'UserController@show' )->name('users.show');
Route::post('users/store', 'UserController@store' )->name('users.store');
Route::get('users/{id}/edit', 'UserController@edit' )->name('users.edit');
Route::put('users/{id}/update', 'UserController@update' )->name('users.update');
Route::delete('users/{id}/', 'UserController@destroy' )->name('users.destroy');
