<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 26/9/17
 * Time: 09:53
 */
Route::post('participar','JoinController@participar')->name('join.created');
Route::post('addVaucher','BuyController@addVoucherToPerson')->name('add.voucher');
Route::post('addPersonToBuy','JoinController@addPersonToBuy')->name('add.personbuy');
Route::get('perfil','PersonController@editPerfil')->name('person.edit_perfil');
Route::put('perfil/{id}/update','PersonController@updatePerfil')->name('person.update_perfil');
