<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('confirmation/{token}','Auth\RegisterController@getConfirmation')->name('confirmation');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('home');
Route::get('home', 'HomeController@dashboardUser')->name('dashboad.user');
Route::get('adminhome', 'HomeController@dashboadAdmin')->name('dashboad.admin');

Route::get('/beneficios', 'HomeController@beneficios')->name('beneficios');
Route::get('/concursos', 'HomeController@concursos')->name('concursos');
Route::get('/concursosj', 'HomeController@concursosj')->name('concursosj');
Route::get('/beneficiosj', 'HomeController@beneficiosj')->name('beneficiosj');

Route::get('/concursos/{slug}','HomeController@show')->name('draws.show');
Route::get('/beneficios/{slug}','HomeController@showbeneficit')->name('benefict.show');
Route::post('/sorteo/preparticipe','HomeController@preparticipe')->name('join.preparticipe');
Route::post('/beneficios/prebuy','HomeController@prebuy')->name('benefict.prebuy');


Route::get('countries', 'CountryController@getCountries');
Route::get('states/{id}', 'StateController@getStates');
Route::get('districs/{id}', 'DistrictController@getDistricts');
Route::get('towns/{id}', 'TownController@getTowns');
Route::get('about','HomeController@about')->name('about');
Route::get('contacto','HomeController@contact')->name('contact');

//api rutas publicas
Route::get('/concursosj', 'APIControllers\HomeControllerApi@concursosj')->name('concursosj');
Route::get('/beneficiosj', 'APIControllers\HomeControllerApi@beneficiosj')->name('beneficiosj');
Route::get('/getLastChangeDraw','APIControllers\HomeControllerApi@getLastChangeDraw');
Route::get('/getLastChangeBuy','APIControllers\HomeControllerApi@getLastChangeBuy');
Route::post('/apiregister','APIControllers\RegisterControllerApi@register')->name('registration.api');
Route::get('/apiconfirmation/{token}','APIControllers\RegisterControllerApi@getConfirmation')->name('confirmation.api');

Route::get('/apistates/{id}', 'APIControllers\AddressControllerApi@getStates');
Route::get('/apidistrics/{id}', 'APIControllers\AddressControllerApi@getDistricts');