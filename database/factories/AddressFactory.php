<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 1/11/17
 * Time: 10:36
 */

use Faker\Generator as Faker;

$factory->define(App\Models\Address::class, function (Faker $faker) {
    $country =  \App\Models\Country::create(['name'=>'Argentina']);
    $state = \App\Models\State::create(['country_id'=>1,'name'=>'Buenos Aires']);//1;
    $district =  \App\Models\District::create(['state_id' => 1, 'name'  => 'Capital Federal']);
    return [
        'country_id' => $country->id,
        'state_id' => $state->id,
        'district_id' => $district->id,
    ];
});