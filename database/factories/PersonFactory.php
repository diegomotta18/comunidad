<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 1/11/17
 * Time: 10:22
 */
use Faker\Generator as Faker;

$factory->define(App\Models\Person::class, function (Faker $faker) {


    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'identification' => $faker->buildingNumber,
        'birthday' => $faker->date("1990-01-05"),
        'telephone' => 3764278402,
        'extranjero' => true
    ];
});