<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDrawPerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draw_person', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('draw_id')->unsigned()->nullable();
            $table->foreign('draw_id')->references('id')
                ->on('draws')->onDelete('cascade');

            $table->integer('person_id')->unsigned()->nullable();
            $table->foreign('person_id')->references('id')
                ->on('persons')->onDelete('cascade');

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('draw_person');

    }

}
