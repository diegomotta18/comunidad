<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusDeleteToPersons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('persons', function (Blueprint $table) {
            $table->boolean('status')->default(true);
            $table->boolean('delete')->default(false);
            $table->date('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('delete');
            $table->dropColumn('deleted_at');
        });
    }
}
