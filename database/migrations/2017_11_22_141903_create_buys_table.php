<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('metadescription');
            $table->date('date_init');
            $table->date('date_finish');
            $table->string('avatar')->default('default.gif');
            $table->string('alt');
            $table->string('slug')->unique()->nullable();
            $table->string('url')->unique()->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('delete')->default(false);
            $table->decimal('price_total')->nullable();
            $table->integer('percent')->nullable();
            $table->decimal('price_discount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buys');
    }
}
