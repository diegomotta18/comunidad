<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalBuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buys', function (Blueprint $table) {
            //
            $table->bigInteger('commerce_id')->unsigned()->nullable();

            $table->foreign('commerce_id')->references('id')
                ->on('commerces');
            $table->date('date_specific')->nullable();
            $table->time('time_specific')->nullable();
            $table->integer('stock')->default(0);
            $table->date('date_init')->nullable()->change();
            $table->date('date_finish')->nullable()->change();
            $table->boolean('check_date_especific')->default(false);
            $table->boolean('check_date_period')->default(false);
            $table->longText('description')->change();
            $table->string('code',11)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buys', function (Blueprint $table) {
            //
            $table->dropForeign(['commerce_id']);
            $table->dropColumn('commerce_id');
            $table->dropColumn('date_specific');
            $table->dropColumn('time_specific');
            $table->dropColumn('stock');
            $table->date('date_init')->change();
            $table->date('date_finish')->change();
            $table->dropColumn('check_date_especific');
            $table->dropColumn('check_date_period');
            $table->text('description')->change();
            $table->string('code',10)->nullable();

        });
    }
}
