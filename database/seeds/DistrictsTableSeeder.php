<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \App\Models\District::create(['state_id'=>13,'name'=>'Apóstoles']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Azara']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Estación Apóstoles,']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Pindapoy']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San José']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Tres Capones']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Aristóbulo del Valle']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Campo Grande']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Dos de Mayo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Primero de Mayo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Salto Encantado']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Bonpland']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Candelaria']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Cerro Corá']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Loreto']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Martires']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Profundidad']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Santa Ana']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Fachinal']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Garupá']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Miguel Lanús']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Posadas']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Concepción de la Sierra']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Concepción']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Santa Maria']);
        \App\Models\District::create(['state_id'=>13,'name'=>'9 de Julio']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Delicia']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Victoria']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Eldorado']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Maria Magdanela']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Mado']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Pinares']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Santiago de Liniers']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Villa Roulet']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Bernardo de Irigoyen']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Comandante Andresito']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Dos Hermanas']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Paraje Campiñas']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Antonio']);
        \App\Models\District::create(['state_id'=>13,'name'=>'El Soberbio']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Fracran']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Vicente']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Wanda']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Gobernador Lanusse']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Iguazú']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Libertad']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Bosseti']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Esperanza']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Libertad']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Capioví']);
        \App\Models\District::create(['state_id'=>13,'name'=>'El Alcazar']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Garuhape']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Mbopicua']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Oro verde']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Leoni']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Puerto Rico']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Ruiz de Montoya']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Gotardo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Miguel']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Almafuerte']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Arroyo del Medio']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Caa Yari']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Cerro Azul']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Dos Arroyos']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Gobernador Lopez']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Leandro N. Alem']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Olegario Victor Andradee']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Villa Libertad']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Caraguatay']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Montecarlo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Piray']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Taruma']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Campo Ramón']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Campo Viera']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Alber']);
        \App\Models\District::create(['state_id'=>13,'name'=>'General Alve']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Guarani']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Los Helechos']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Oberá']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Panami']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Martin']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Polana']);
        \App\Models\District::create(['state_id'=>13,'name'=>'General Urquiza']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Gobernador Roca']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Hipólito Irigoyen']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Jardín América']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Los Teales']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Oasis']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Ignacio']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Santo Pipo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Corpus Christi']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Florentino Ameghino']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Itacaruaré']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Mojón Grande']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Javier']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Cruce Caballero']);
        \App\Models\District::create(['state_id'=>13,'name'=>'El Paraiso']);
        \App\Models\District::create(['state_id'=>13,'name'=>'El Piñalito']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Pedro']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Tobuna']);
        \App\Models\District::create(['state_id'=>13,'name'=>'25 de Mayo']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Alba Posse']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Colonia Aurora']);
        \App\Models\District::create(['state_id'=>13,'name'=>'San Francisco de Asis']);
        \App\Models\District::create(['state_id'=>13,'name'=>'Santa Rita']);
    }
}
