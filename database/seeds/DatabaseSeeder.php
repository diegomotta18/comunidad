<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create();

        $user = factory(User::class)->create([
            'name'     => 'Diego',
            'email'    => 'diego.motta@misionesonline.net',
            'password' => bcrypt('admin#*'),
            'role'     => 'user',
        ]);
        factory(\App\Models\Person::class)->create();
        factory(\App\Models\Person::class)->create();
        factory(\App\Models\Person::class)->create();
        //$person = factory(\App\Models\Person::class)->create();
        //$person->user_id = User::first()->id;
        //$person->save();

//        $user->person_id = $person->id;
//        $user->save();

        $this->call('AddressesTableSeeder');
        $this->call('DistrictsTableSeeder');

    }
}
