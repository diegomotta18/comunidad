@section('meta-title','Contacto')
@section('meta-description','Contacto a Comunidad de Misiones Online')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    <section id="slider" class="fullheight"
             style="background:url({{asset('client/demo_files/images/particles_bg.jpg')}})">
        <span class="overlay dark-2"><!-- dark overlay [0 to 9 opacity] --></span>

        <canvas id="canvas-particle" data-rgb="156,217,249"><!-- CANVAS PARTICLES --></canvas>

        <div class="display-table">
            <div class="display-table-cell vertical-align-middle">

                <div class="container">
                    <div class="col-md-9 col-sm-9">
                        <h2>Contactanos</h2>
                        <p>
                            <span class="block">
                                <strong><i class="fa fa-map-marker"></i> Dirección: </strong> Coronel López 2138 - 6° piso - Posadas - Misiones
                            </span>
                            <span class="block">
                                <strong><i class="fa fa-phone"></i> Telefono: </strong> (0376) 4425800

                            </span>
                            <span class="block">
                                   <strong><i class="fa fa-envelope"></i> Email: </strong> comunidad@misionesonline.net
                            </span>

                        </p>
                    </div>


                </div>

            </div>
        </div>

    </section>
@endsection
@push('scripts')

@include('includes.menu')
@endpush