@section('meta-title','Mi perfil')
@section('meta-description','Configuración de los datos del perfil de usuario')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    <section style="margin-top: -10px;">
        <div class="container">
            <div class="tab-content">
                <!-- LOGIN -->
                <div class="tab-pane active" id="login">
                    <div class="row" ng-controller="AddressCont">
                        <div class="col-md-12 col-sm-12">


                            <div class="box-static box-transparent box-bordered p-20">
                                <div class="box-title mb-30">
                                    <h2 class="fs-20 text-center">Perfil de usuario</h2>
                                </div>
                                @include('partials/registererror')

                                <form class="m-0 sky-form" method="post"
                                      action="{{ route('person.update_perfil', $person->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="box-title mb-30">
                                        <h2 class="fs-16">Datos personales</h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('nombres') ? ' has-error' : '' }}">
                                                <label>(*) Nombres</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-font"></i>
                                                    <input required="" type="text" name="nombres"
                                                           value="{{ $person->first_name }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su primer y
                                                        segundo nombre</b>
                                                </label>
                                                @if ($errors->has('nombres'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('nombres') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                                                <label>(*) Apellido</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-font"></i>
                                                    <input required="" type="text" name="apellido"
                                                           value="{{ $person->last_name }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su apellido</b>
                                                </label>
                                                @if ($errors->has('apellido'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('apellido') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('documento') ? ' has-error' : '' }}">
                                                <label>(*) Nº de Documento de Identidad</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-hashtag"></i>
                                                    <input required="" type="number" name="documento"
                                                           value="{{ $person->identification }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su Nº de
                                                        documento de identidad</b>
                                                </label>
                                                @if ($errors->has('documento'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('documento') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Sexo</label>
                                                <div class="fancy-form fancy-form-select">
                                                    <select name="sexo" class="form-control bootstrap-select">
                                                        <option value="Indicar" @if($person->sex != 'Masculino' && $person->sex != 'Femenino' && $person->sex != 'Otro' ) {{'selected'}} @else {{''}} @endif>
                                                            -- Seleccionar --
                                                        </option>
                                                        <option value="Masculino" {{ $person->sex == 'Masculino' ? 'selected' : '' }}>Masculino
                                                        </option>
                                                        <option value="Femenino" {{ $person->sex == 'Femenino' ? 'selected' : '' }}>
                                                            Femenino
                                                        </option>
                                                        <option value="Otro" {{ $person->sex == 'Otro' ? 'selected' : '' }}>
                                                            Otro
                                                        </option>

                                                    </select>
                                                    <i class="fancy-arrow"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('fecha_de_nacimiento') ? ' has-error' : '' }}">
                                                <label>(*) Fecha de nacimiento</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-calendar"></i>
                                                    <input required="" type="text" name="fecha_de_nacimiento"
                                                           value="{{ $person->birthday}}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su fecha de
                                                        nacimiento</b>
                                                </label>
                                                @if ($errors->has('fecha_de_nacimiento'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('fecha_de_nacimiento') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                    <div class="box-title mb-30">
                                        <h2 class="fs-16">Datos de contacto</h2>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                                                <label>(*) Teléfono o celular</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-user"></i>
                                                    <input required="" type="number" name="telefono"
                                                           value="{{ $person->telephone }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su telefono o
                                                        celular</b>
                                                </label>
                                                @if ($errors->has('telefono'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('telefono') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">(*) Email</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="" type="text" name="email"
                                                           value="{{ Auth::user()->email}}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su correo
                                                        electrónico</b>
                                                </label>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-title mb-30">
                                        <h2 class="fs-16">Datos del domicilio</h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>(*) Nacionalidad</label>
                                            <div @if ($errors->has('argentino') || ($errors->has('extranjero'))) class="form-group has-error"
                                                 @else class="form-group" @endif >
                                                <div style="display: flex; ">
                                                    </label>
                                                    <label class="checkbox  m-0"><input id="argentino"
                                                                                        class="checked-agree"
                                                                                        type="checkbox" name="argentino"
                                                                                        @if($person->extranjero == 'No') checked @endif><i></i>Argentino </label>
                                                    <div class="form-group">
                                                        &nbsp;
                                                    </div>
                                                    </label>
                                                    <label class="checkbox  m-0"><input id="extranjero"
                                                                                        class="checked-agree"
                                                                                        type="checkbox"
                                                                                        name="extranjero"
                                                                                        @if($person->extranjero == 'Si') checked @endif><i></i>Extranjero</label>
                                                </div>
                                                @if ($errors->has('argentino') || $errors->has('extranjero'))
                                                    <span class="help-block">
                                                            <strong>Debe seleccionar la nacionalidad</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        @if(!is_null($person->address))
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                                                    <label>(*) País
                                                    </label>
                                                    <div class="form-select">
                                                        <select id="pais" name="pais"
                                                                class="form-control"
                                                                ng-model="country"
                                                                ng-options="country.name for country in countries track by country.id"
                                                                ng-init="changeCountry({{$person->address->country_id}},{{$person->address->state_id}},{{$person->address->district_id}})"
                                                                ng-change="getStates(country.id)">
                                                            <option value="" selected="">-- Seleccionar Pais--</option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('pais'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('pais') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('provincia') ? ' has-error' : '' }}">

                                                    <label>(*) Provincia
                                                    </label>
                                                    <div class="form-select">

                                                        <select id="provincia" name="provincia"
                                                                class="form-control"
                                                                ng-model="state"
                                                                ng-options="state.name for state in states track by state.id"
                                                                ng-change="getDistricts(state.id)">
                                                            <option value="" selected="">-- Seleccionar Provincia--
                                                            </option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('provincia'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('provincia') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('localidad') ? ' has-error' : '' }}">

                                                    <label>(*) Localidad
                                                    </label>
                                                    <div class="form-select">
                                                        <select id="localidad" name="localidad"
                                                                class="form-control"
                                                                ng-model="district"
                                                                ng-options="district.name for district in districts track by district.id"
                                                                ng-change="getTowns(district.id)">
                                                            <option value="" selected="">-- Seleccionar Localidad--
                                                            </option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('localidad'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('localidad') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                    </div>
                                    @else
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                                                <label>(*) País {{$person->address}}
                                                </label>
                                                <div class="form-select">
                                                    <select id="pais" name="pais"
                                                            class="form-control"
                                                            ng-model="country"
                                                            ng-options="country.name for country in countries track by country.id"
                                                            ng-change="getStates(country.id)">
                                                        <option value="" selected="">-- Seleccionar Pais--</option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('pais'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('pais') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('provincia') ? ' has-error' : '' }}">

                                                <label>(*) Provincia
                                                </label>
                                                <div class="form-select">

                                                    <select id="provincia" name="provincia"
                                                            class="form-control"
                                                            ng-model="state"
                                                            ng-options="state.name for state in states track by state.id"
                                                            ng-change="getDistricts(state.id)">
                                                        <option value="" selected="">-- Seleccionar Provincia--
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('provincia'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('provincia') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('localidad') ? ' has-error' : '' }}">

                                                <label>(*) Localidad
                                                </label>
                                                <div class="form-select">
                                                    <select id="localidad" name="localidad"
                                                            class="form-control"
                                                            ng-model="district"
                                                            ng-options="district.name for district in districts track by district.id">
                                                        <option value="" selected="">-- Seleccionar Localidad--
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('localidad'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('localidad') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    <hr/>

                                    <div style="text-align: center;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>
                                            Aceptar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection