@section('meta-title','Concursos')
@section('meta-description', 'Concursos de la Comunidad de Misiones Online')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    {{--<div class="container-fluid">--}}
    <section style="border-style: solid;border-color: #ffffff;">
        <div class="center" style="margin-top: -10px;">
            <div class="hidden-xs ">
                <div class="container">
                    @if(count($draws) >0)
                        @foreach ($draws->chunk(3) as $chunk)
                            <div class="row is-flex" style="margin-left: 50px;">
                                @foreach ($chunk as $draw)
                                    <div class="col-md-4 well" style="padding: 40px;margin: 5px;width: 30%;">
                                        <!-- image -->
                                        <div class="row">
                                            <div class="">
                                                <!-- carousel -->
                                                <div class="">
                                                    <div>
                                                        <a href="/concursos/{{$draw->slug}}">
                                                            <img class="img-responsive"
                                                                 src="/storage/{{$draw->avatar}}"
                                                                 width="800"
                                                                 height="533" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- /carousel -->
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class=""><!-- description -->
                                                <a href="/concursos/{{$draw->slug}}"
                                                   style="font-size: 21px;margin-top: 2px;">
                                                    {{$draw->name}}

                                                </a>

                                                <div class="mt-30"><!-- description -->

                                                    <p align="justify">{!! \Illuminate\Support\Str::words($draw->description,10,'...') !!}</p>

                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <i class="fa fa-check"></i>
                                                            <strong style="font-size: 13px;">Duración:</strong> {{$draw->date_init}}
                                                            - {{$draw->date_finish}}
                                                        </li>
                                                        <li>
                                                            <i class="fa fa-check"></i>
                                                            <strong style="font-size: 13px;">Fecha de
                                                                resultado:</strong> {{$draw->date_result}}
                                                        </li>

                                                    </ul>
                                                </div><!-- /description -->
                                            </div><!-- /item -->
                                        </div>
                                        <div class="row force-to-bottom">
                                            @auth

                                            @if(!is_null(Auth::user()->person))
                                                @if(!Auth::user()->person->draws->contains($draw))
                                                    <form id="form" class="form text-center" method="POST"
                                                          action="{{route('join.created')}}">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" id="id" name="draw_id"
                                                               value="{{$draw->id}}">
                                                        <input type="hidden" id="id" name="person_id"
                                                               value="{{Auth::user()->person()->first()->id}}">


                                                        @if(($draw->questions()->count() >0 ))
                                                            <button
                                                                    type="button"
                                                                    class="btn btn-lg btn-default btn-bordered fs-15"
                                                                    data-toggle="modal"
                                                                    data-id=""
                                                                    data-title=""
                                                                    data-target="#myModal{{$draw->id}}">
                                                                PARTICIPAR YA!
                                                            </button>
                                                        @else
                                                            <button type="submit"
                                                                    class="btn btn-lg btn-default btn-bordered fs-15">
                                                                PARTICIPAR YA!
                                                            </button>
                                                        @endif
                                                    </form>
                                                @endif
                                            @else
                                                <form id="form" class="form text-center">

                                                    <button class="btn btn-lg btn-default btn-bordered fs-15">
                                                        YA ESTAS PARTICIPANDO
                                                    </button>
                                                </form>
                                            @endif
                                            @else
                                                <form id="form" class="form text-center" method="POST"
                                                      action="{{route('join.preparticipe')}}">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" id="id" name="draw_id"
                                                           value="{{$draw->id}}">
                                                    <button type="submit"
                                                            class="btn btn-lg btn-default btn-bordered fs-15">
                                                        PARTICIPAR YA!
                                                    </button>
                                                </form>
                                                @endauth
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
        <div class="hidden-md hidden-lg hidden-sm">
            @foreach ($draws as $draw)
                <div class="col-md-4 well" style="  padding: 25px;">
                    <!-- image -->
                    <div class="">
                        <!-- carousel -->
                        <div class="">
                            <div>
                                <a href="/sorteo/{{$draw->slug}}">
                                    <img class="img-responsive" src="/storage/{{$draw->avatar}}"
                                         width="800"
                                         height="533" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- /carousel -->
                    </div>
                    <div class="">
                        <div class=""><!-- description -->
                            <a href="/sorteo/{{$draw->slug}}"
                               style="font-size: 28px;margin-top: 2px;">
                                {{$draw->name}}
                            </a>

                            <div class="mt-30"><!-- description -->

                                <p>{!! \Illuminate\Support\Str::words($draw->description,10,'...') !!}</p>

                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-check"></i>
                                        <strong style="font-size: 13px;">Duración :</strong> {{$draw->date_init}}
                                        - {{$draw->date_finish}}
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i>
                                        <strong style="font-size: 13px;">Fecha de
                                            resultado:</strong> {{$draw->date_result}}
                                    </li>

                                </ul>


                                @auth
                                @if(!is_null(Auth::user()->person))
                                    @if(!Auth::user()->person->draws->contains($draw))
                                        <form id="form" class="form" method="POST"
                                              action="{{route('join.created')}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" id="id" name="draw_id"
                                                   value="{{$draw->id}}">
                                            <input type="hidden" id="id" name="person_id"
                                                   value="{{Auth::user()->person()->first()->id}}">

                                            @if(($draw->questions()->count() >0 ))
                                                <button
                                                        type="button"
                                                        class="btn btn-lg btn-default btn-bordered fs-15"
                                                        data-toggle="modal"
                                                        data-id=""
                                                        data-title=""
                                                        data-target="#myModal{{$draw->id}}">
                                                    PARTICIPAR YA!
                                                </button>
                                            @else
                                                <button type="submit"
                                                        class="btn btn-lg btn-default btn-bordered fs-15">
                                                    PARTICIPAR YA!
                                                </button>
                                            @endif

                                        </form>
                                    @endif
                                @else
                                    <button class="btn btn-lg btn-default btn-bordered fs-15">
                                        YA ESTAS PARTICIPANDO
                                    </button>
                                @endif
                                @else
                                    <form id="form" class="form" method="POST"
                                          action="{{route('join.preparticipe')}}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" id="id" name="draw_id"
                                               value="{{$draw->id}}">
                                        <button type="submit"
                                                class="btn btn-lg btn-default btn-bordered fs-15">
                                            PARTICIPAR YA!
                                        </button>
                                    </form>
                                    @endauth
                            </div><!-- /description -->
                        </div><!-- /item -->
                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
        @else
            <div class="alert alert-info">
                <strong>Disculpe</strong>, por el momento no hay sorteos activos.
            </div>
        @endif
    </section>
    {{--</div>--}}
    <section>
    </section>

    @if(count($draws) >0)
        @foreach ($draws as $draw)
            @if ($draw->questions()->count() > 0)
                <div class="modal fade sky-form" role="dialog" id="myModal{{$draw->id}}">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <form method="POST" action="{{route('join.created')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> <input type="hidden"
                                                                                                      id="id"
                                                                                                      name="draw_id"
                                                                                                      value="{{$draw->id}}">
                                @auth

                                <input type="hidden" id="id" name="person_id"
                                       value="{{Auth::user()->person()->first()->id}}">
                                @endauth
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="text-center"><span
                                                class=""><b style="font-size: 21px;">{{$draw->name}}</b></span>
                                    </h4>
                                </div>
                                <div class="card-block">
                                    <h5 class="modal-title">Responda lo siguiente:
                                    </h5>
                                    <br>
                                    @foreach($draw->questions()->get() as $q)
                                        <label class="control-label"><strong>{{$q->question}}</strong> </label>
                                        @foreach($q->options()->get() as $option)
                                            @if($q->input_type == 'radio')
                                                <label class="radio">
                                                    <input type="{{$q->input_type}}" name="option{{$q->id}}"
                                                           value="{{$option->label_option}}"><i></i><b
                                                            style="font-size: 12px;">{{$option->label_option}}</b>
                                                </label>
                                            @else
                                                <label class="checkbox  m-0"><input
                                                            class="checked-agree"
                                                            type="{{$q->input_type}}" name="option{{$q->id}}[]"
                                                            value="{{$option->label_option}}"
                                                    ><i></i>{{$option->label_option}}</label>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </div>
                                <div class="modal-footer" style="text-align: center;">
                                    <button type="submit" class="btn btn-primary ">ACEPTAR</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            @endif
        @endforeach
    @endif
@endsection