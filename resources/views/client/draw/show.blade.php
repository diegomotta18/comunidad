@section('meta-title',$draw->name)
@section('meta-description', $draw->metadescription)
@include('includes.analitics')
@push('seo')
<meta property="og:title" content="{{$draw->name}}">
<meta property="og:url" content="{{request()->url()}}"/>
<meta property="og:image" content="{{asset('/storage/'.$draw->avatar)}}"/>
<meta property="og:description" content="{{$draw->metadescription}}"/>

@endpush
@extends('layouts.app')
@section('content')
    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2"><!-- image -->
                <!-- carousel -->
                <br>
                <h1 style="font-size: 28px;margin-top: 50px;">
                    {{$draw->name}}
                </h1>
                <div class="well">
                    <img class="img-responsive" src="/storage/{{$draw->avatar}}" width="800"
                         height="533" alt="">
                </div>
                <!-- /carousel -->
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2"><!-- description -->
                <p>{!! $draw->description !!}</p>
                <ul class="list-unstyled">
                    <li>
                        <i class="fa fa-check"></i>
                        <strong>Periodo de participación:</strong> {{$draw->date_init}}
                        - {{$draw->date_finish}}
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <strong>Fecha de resultado:</strong> {{$draw->date_result}}
                    </li>
                </ul>
                @auth
                @if(!Auth::user()->person->draws->contains($draw))

                    <form id="form" class="form" method="POST" action="{{route('join.created')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" id="id" name="draw_id" value="{{$draw->id}}">
                        <input type="hidden" id="id" name="person_id"
                               value="{{Auth::user()->person()->first()->id}}">

                        <button type="submit" class="btn btn-lg btn-default btn-bordered fs-15">
                            PARTICIPAR YA!
                        </button>
                    </form>
                @else
                    <button class="btn btn-lg text-center btn-default btn-bordered fs-15">
                        YA ESTAS PARTICIPANDO
                    </button>
                    <br>
                    <br>
                    <br>
                    <br>

                @endif
                @else
                    <form id="form" class="form" method="POST"
                          action="{{route('join.preparticipe')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" id="id" name="draw_id" value="{{$draw->id}}">
                        <button type="submit" class="btn btn-lg btn-default btn-bordered fs-15">
                            PARTICIPAR YA!
                        </button>
                    </form>
                    @endauth
            </div>
        </div>
        <!-- /item -->
    </div>
@endsection