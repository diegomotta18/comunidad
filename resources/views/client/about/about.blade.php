@section('meta-title','Que es comunidad?')
@section('meta-description','Comunidad MisionesOnline es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    <section id="slider" class="fullheight"
             style="background:url({{asset('client/demo_files/images/particles_bg.jpg')}})">
        <span class="overlay dark-2"><!-- dark overlay [0 to 9 opacity] --></span>

        <canvas id="canvas-particle" data-rgb="156,217,249"><!-- CANVAS PARTICLES --></canvas>

        <div class="display-table">
            <div class="display-table-cell vertical-align-middle">

                <div class="container text-center">

                        <p align="justify" style="font-size: 18px;">Comunidad de Misiones Online es el Programa de Beneficios exclusivo para lectores de
                            MisionesOnline.
                            Además de poder participar gratis en concursos con importantes premios, Les permite también
                            disfrutar de
                            promociones y descuentos en entretenimientos, espectáculos, shopping, gastronomía, turismo y muchos
                            otros
                            rubros.
                            Todos los usuarios acceden en forma gratuita a su Registro de Usuario, y con ello disfrutan de
                            increíbles
                            promociones y regalos con sólo presentar sus credenciales o cupones virtuales en los sitios de
                            compras y
                            comercios adheridos. </p>

                </div>

            </div>
        </div>

    </section>
@endsection
@push('scripts')

@include('includes.menu')
@endpush