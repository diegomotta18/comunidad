@section('meta-title','Beneficios')
@section('meta-description', 'Beneficios de la Comunidad de Misiones Online')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    {{--<div class="container-fluid">--}}
    <section style="border-style: solid;border-color: #ffffff;">
        <div class="center" style="margin-top: -10px;">
            <div class="hidden-xs">
                <div class="container">
                    @if(count($buys) >0)
                        @foreach ($buys->chunk(3) as $chunk)
                            <div class="row is-flex" style="margin-left: 50px;">
                                @foreach ($chunk as $buy)
                                    <div class="col-md-4 col-xs-4 well" style="padding: 40px;margin: 5px;width: 30%;">
                                        <!-- image -->
                                        <div class="row">
                                            <!-- carousel -->
                                            @if(!is_null($buy->percent))
                                                <h2 class="label label-danger price">
                                                    <p>Descuento</p>
                                                    <p style="color:#ffffff; font-size: 28px;margin-top: -30px;">{{$buy->percent}}
                                                        %</p>
                                                </h2>
                                            @endif

                                            <div>

                                                <a href="/beneficios/{{$buy->slug}}">
                                                    <img class="img-responsive"
                                                         src="/storage/{{$buy->avatar}}"
                                                         width="800"
                                                         height="533" alt="">
                                                </a>
                                                <div style="margin-top: -5px;z-index: 2;background-color: #333; opacity: 0.5;">
                                                    <b><p
                                                                style="font-size: 18px;color: white;opacity: 100!important;">
                                                            {{$buy->name}}

                                                        </p></b>
                                                </div>

                                            </div>
                                            <!-- /carousel -->
                                        </div>
                                        <div class="row ">
                                            <div class=""><!-- description -->
                                                <h3 style="margin-top: -15px;text-align: center;">
                                                    <span class="label label-default"><s>$ {{$buy->price_total}}</s></span><span
                                                            class="label label-success">$ {{$buy->price_discount}}</span>
                                                </h3>
                                                <p style="word-wrap: break-word !important;margin-top: -15px;">{!! \Illuminate\Support\Str::words($buy->description,15,'...') !!}</p>
                                                {{--<ul class="list-unstyled" style="margin-top: -15px;">--}}
                                                {{--@if(!is_null($buy->date_init))--}}
                                                {{--<li>--}}
                                                {{--<i class="fa fa-check"></i>--}}
                                                {{--<strong style="font-size: 13px;">Duración:</strong> {{$buy->date_init}}--}}
                                                {{--- {{$buy->date_finish}}--}}
                                                {{--</li>--}}
                                                {{--@else--}}
                                                {{--<li>--}}
                                                {{--<i class="fa fa-check"></i>--}}
                                                {{--<strong style="font-size: 13px;">Fecha de vencimiento:</strong> {{$buy->date_specific}}--}}

                                                {{--</li>--}}
                                                {{--@endif--}}


                                                {{--</ul>--}}
                                            </div><!-- /item -->
                                        </div>

                                        <div class="row force-to-bottom">
                                            @auth
                                            @if(Auth::user())
                                                <form id="form" method="post" class="form text-center">
                                                    {{--<a href="{{$buy->url}}" target="_blank" type="button"--}}
                                                       {{--class="btn btn-lg btn-default btn-bordered fs-15"><i--}}
                                                                {{--class="fa fa-shopping-cart"></i> COMPRAR--}}
                                                    {{--</a>--}}
                                                    <a href="/beneficios/{{$buy->slug}}"
                                                       class="btn btn-lg btn-default btn-bordered fs-15 ">
                                                        <i class="fa fa-plus"></i> COMPRAR
                                                    </a>
                                                </form>
                                            @endif
                                            @else
                                                <form id="form" class="form text-center" method="post"
                                                      action="{{route('benefict.prebuy')}}">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="buy_id" value="{{$buy->id}}">
                                                    {{--<button class="btn btn-xs btn-default btn-bordered fs-12"--}}
                                                            {{--type="submit"><i--}}
                                                                {{--class="fa fa-shopping-cart"></i>COMPRAR--}}
                                                    {{--</button>--}}
                                                    <a href="/beneficios/{{$buy->slug}}"
                                                       class="btn btn-lg btn-default btn-bordered fs-15 ">
                                                        <i class="fa fa-shopping-cart"></i> COMPRAR
                                                    </a>
                                                </form>
                                                @endauth


                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
        <div class="hidden-md hidden-lg hidden-sm">
            @foreach ($buys as $buy)
                <div class="col-md-4 well" style="padding: 25px;">
                    <!-- image -->
                    <!-- carousel -->
                    <div class="">
                        @if(!is_null($buy->percent))
                            <h2 class="label label-danger price">
                                <p>Descuento</p>
                                <p style="color:#ffffff; font-size: 28px;margin-top: -30px;">{{$buy->percent}}
                                    %</p>
                            </h2>
                        @endif

                        <div>

                            <a href="/beneficios/{{$buy->slug}}">
                                <img class="img-responsive"
                                     src="/storage/{{$buy->avatar}}"
                                     width="800"
                                     height="533" alt="">
                            </a>
                            <div style="margin-top: -5px;z-index: 2;background-color: #333; opacity: 0.5;">
                                <b><p
                                            style="font-size: 18px;color: white;opacity: 100!important;">
                                        {{$buy->name}}

                                    </p></b>
                            </div>

                        </div>
                    </div>
                    <!-- /carousel -->
                    <div class="row ">
                        <div class=""><!-- description -->
                            <h3 style="margin-top: -15px;text-align: center;">
                                <span class="label label-default"><s>$ {{$buy->price_total}}</s></span><span
                                        class="label label-success">$ {{$buy->price_discount}}</span>
                            </h3>
                            <p style="word-wrap: break-word !important;margin-top: -15px;">{!! \Illuminate\Support\Str::words($buy->description,15,'...') !!}</p>
                        </div><!-- /item -->
                    </div>
                    <div class="row force-to-bottom">
                        @auth
                        @if(Auth::user())
                            <form id="form" method="post" class="form text-center">
                                {{--<a href="{{$buy->url}}" target="_blank" type="button"--}}
                                   {{--class="btn btn-lg btn-default btn-bordered fs-15"><i--}}
                                            {{--class="fa fa-shopping-cart"></i> COMPRAR--}}
                                {{--</a>--}}
                                <a href="/beneficios/{{$buy->slug}}"
                                   class="btn btn-lg btn-default btn-bordered fs-15 ">
                                    <i class="fa fa-shopping-cart"></i> COMPRAR
                                </a>
                            </form>
                        @endif
                        @else
                            <form id="form" class="form text-center" method="post"
                                  action="{{route('benefict.prebuy')}}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="buy_id" value="{{$buy->id}}">
                                {{--<button class="btn btn-xs btn-default btn-bordered fs-12"--}}
                                        {{--type="submit"><i--}}
                                            {{--class="fa fa-shopping-cart"></i>COMPRAR--}}
                                {{--</button>--}}
                                <a href="/beneficios/{{$buy->slug}}"
                                   class="btn btn-xs btn-default btn-bordered fs-12 ">
                                    <i class="fa fa-shopping-cart"></i> COMPRAR
                                </a>
                            </form>
                            @endauth
                    </div>
                </div>
            @endforeach
        </div>
        @else
            <div class="alert alert-info">
                <strong>Disculpe</strong>, por el momento no hay beneficios activos.
            </div>
        @endif
    </section>
    {{--</div>--}}
    <section>
    </section>

@endsection