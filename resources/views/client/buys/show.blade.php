@section('meta-title',$buy->name)
@section('meta-description', $buy->metadescription)
@include('includes.analitics')
@push('seo')
<meta property="og:title" content="{{$buy->name}}">
<meta property="og:url" content="{{request()->url()}}"/>
<meta property="og:image" content="{{asset('/storage/'.$buy->avatar)}}"/>
<meta property="og:description" content="{{$buy->metadescription}}"/>

@endpush

@extends('layouts.app')
@push('styles')
<link href="{{ asset('/plugins/totime/timeTo.css') }}" rel="stylesheet"/>
@endpush
@section('content')


    <div class="container">

        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="form">
                    <div class="row well">
                        <div class="col-md-12"><!-- image -->
                            <!-- carousel -->

                            <h1 style="font-size: 28px;text-align: center;">

                                {{$buy->name}}
                            </h1>
                        </div>
                        <div class="col-md-6">
                                <img class="img-responsive" src="/storage/{{$buy->avatar}}" alt="{{$buy->alt}}">
                        </div>
                        <div class="col-md-6" style="">
                            @if(is_null($buy->date_init))
                            <div class="row">
                                <br class="hidden-md">
                                <h3 style="text-align: center;">
                                    {{--<div id="getting-started"></div>--}}
                                    <div id="countdown" class="" data-value=""></div>
                                </h3>
                            </div>
                            @endif
                            @if(!is_null($buy->percent))

                                <div class="row">

                                    <h3 style="text-align: center;"><span class="label label-danger">{{$buy->percent}}
                                            % de descuento</span>
                                    </h3>
                                </div>
                            @endif
                            <div class="row">
                                <h3 style="text-align: center;">
                                    <span class="label label-default"><s>$ {{$buy->price_total}}</s></span>
                                </h3>
                            </div>
                            <div class="row">
                                <h2 style="text-align: center;"><span
                                            class="label label-success">$ {{$buy->price_discount}}</span>
                                </h2>
                            </div>
                            <div class="row" style="text-align: center; margin-top: -20px;">
                                @auth
                                @if(Auth::user())
                                    @if(is_null($buy->getDateSpecific()))
                                        @if(!is_null(Auth::user()->person()))
                                        <form id="form" class="form" method="POST" action="{{route('add.personbuy')}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" id="id" name="buy_id" value="{{$buy->id}}">
                                            <input type="hidden" id="id" name="person_id"
                                                   value="{{Auth::user()->person()->first()->id}}">
                                            <button type="submit"
                                               class="btn btn-lg btn-default btn-bordered fs-15"><i
                                                        class="fa fa-shopping-cart"></i>
                                                COMPRAR
                                            </button>
                                        </form>
                                            @endif
                                    @else
                                        @if(!Auth::user()->person->buys->contains($buy))

                                            <form id="form" class="form" method="POST"
                                                  action="{{route('add.voucher')}}">
                                                {!! csrf_field() !!}
                                                <input type="hidden" id="id" name="buy_id" value="{{$buy->id}}">
                                                <input type="hidden" id="id" name="person_id"
                                                       value="{{Auth::user()->person()->first()->id}}">

                                                <button type="submit"
                                                        class="btn btn-lg btn-default btn-bordered fs-15">
                                                    OBTENER CUPÓN
                                                </button>
                                            </form>
                                        @else
                                            @if(!is_null($buy->code))

                                                <label
                                                        class="">
                                                    <b>ESTE ES TU CÓDIGO DEL CUPÓN DE DESCUENTO</b>
                                                </label>
                                                <div class=" alert-success ">
                                                    {{$buy->code}}
                                                </div>
                                                <label>
                                                    <b>COPIÁ TU CÓDIGO Y HACE CLICK EN COMPRAR</b>
                                                </label>
                                                <form id="form" class="form">
                                                    <a href="{{$buy->url}}" target="_blank" type="button"
                                                       class="btn btn-lg btn-default btn-bordered fs-15"><i
                                                                class="fa fa-tag"></i>
                                                        COMPRAR
                                                    </a>
                                                </form>
                                            @endif

                                        @endif
                                    @endif
                                @endif
                                @else
                                    @if(is_null($buy->getDateSpecific()))

                                        <form id="form" class="form" method="post"
                                              action="{{route('benefict.prebuy')}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="buy_id" value="{{$buy->id}}">
                                            <button class="btn btn-lg btn-default btn-bordered"
                                                    type="submit"><i
                                                        class="fa fa-shopping-cart"></i>COMPRAR
                                            </button>
                                        </form>
                                    @else
                                        <form id="form" class="form" method="post"
                                              action="{{route('benefict.prebuy')}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="buy_id" value="{{$buy->id}}">
                                            <button class="btn btn-lg btn-default btn-bordered"
                                                    type="submit"><i
                                                        class="fa fa-tag"></i> OBTENER CUPÓN
                                            </button>
                                        </form>
                                    @endif


                                    @endauth


                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="row" style="margin-top: -30px;">
                                <br>
                                <br>

                                <div align="justify">{!! $buy->description !!}</div>
                                @if(!is_null($buy->date_init))
                                    <ul class="list-unstyled">
                                        <li>
                                            <i class="fa fa-check"></i>
                                            <strong>Periodo del beneficio:</strong> {{$buy->date_init}}
                                            - {{$buy->date_finish}}
                                        </li>

                                    </ul>
                                @else
                                    <ul class="list-unstyled">
                                        <li>
                                            <i class="fa fa-check"></i>
                                            <strong>Fecha de vencimiento:</strong> {{$buy->date_specific}}
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i>
                                            <strong>Hora de vencimiento:</strong> {{$buy->getTimeSpecific()}}
                                        </li>
                                    </ul>
                                @endif
                            </div>

                        </div>

                    </div>
                    <div class="container">
                    </div>
                </div>
            </div>
        </div>
        <!-- /item -->
    </div>

@endsection
@push('scripts')
<script src="{{ asset('/plugins/totime/jquery.time-to.js')}}"></script>
<script src="{{ asset('/plugins/countdown/jquery.countdown.js')}}"></script>

<script>
    var ti = {!! json_encode($buy->getTimeDate()) !!};
    console.log(ti);
    $('#countdown').timeTo({
        timeTo: new Date(new Date(ti + ' GMT-0300')),
        displayDays: 2,
        theme: "black",
        displayCaptions: true,
        fontSize: 30,
        captionSize: 14,
        languages: {
            pl: {days: 'días', hours: 'horas', min: 'minutos', sec: 'segundos'}
        },
        lang: 'pl'
    });
    $("#getting-started")
        .countdown('2020/10/10', function (event) {
            $(this).html(event.strftime('%D days %H:%M:%S'));
        });

</script>
@endpush