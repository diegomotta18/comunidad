<div id="loaddraw" style="position: relative;">
    <div class="loaddraw clearfix shop-list-options mb-20 pull-right">
        {{ $draws->links() }}
    </div>
    <ul class="shop-item-list row list-inline m-0">
        @if(count($draws) >0)
            @foreach ($draws->chunk(4) as $chunk)
                @foreach ($chunk as $draw)
                    <li class="col-lg-3 col-sm-3">

                        <div class="shop-item">

                            <div class="thumbnail">
                                <!-- product image(s) -->
                                <a class="shop-item-image" href="/concursos/{{$draw->slug}}">
                                    <img class="img-fluid" src="/storage/{{$draw->avatar}}"
                                         alt="{{$draw->alt}}"/>
                                </a>

                            </div>

                            <div class="shop-item-summary text-center">
                                <h2>{{$draw->name}}</h2>

                                <!-- rating -->
                            {{--<div class="shop-item-rating-line">--}}
                            {{--<div class="rating rating-4 fs-13"><!-- rating-0 ... rating-5 --></div>--}}
                            {{--</div>--}}
                            {{--<!-- /rating -->--}}

                            {{--<!-- price -->--}}
                            {{--<div class="shop-item-price">--}}
                            {{--<span class="line-through">$98.00</span>--}}
                            {{--$78.00--}}
                            {{--</div>--}}
                            <!-- /price -->
                            </div>

                            <!-- buttons -->
                            <div class="shop-item-buttons text-center">
                                @auth
                                @if(!is_null(auth()->user()->person))
                                    @if(!auth()->user()->person->draws->contains($draw))

                                        <form id="form" class="form text-center" method="POST"
                                              action="{{route('join.created')}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" id="id" name="draw_id"
                                                   value="{{$draw->id}}">
                                            <input type="hidden" id="id" name="person_id"
                                                   value="{{Auth::user()->person()->first()->id}}">


                                            @if(($draw->questions()->count() >0 ))
                                                <button
                                                        type="button"
                                                        class="btn btn-lg btn-default btn-bordered fs-15"
                                                        data-toggle="modal"
                                                        data-id=""
                                                        data-title=""
                                                        data-target="#myModal{{$draw->id}}">
                                                    PARTICIPAR YA!
                                                </button>
                                            @else
                                                <button type="submit"
                                                        class="btn btn-lg btn-default btn-bordered fs-15">
                                                    PARTICIPAR YA!
                                                </button>
                                            @endif
                                        </form>
                                    @else
                                        <form id="form" class="form text-center">

                                            <button class="btn btn-lg btn-default btn-bordered fs-15">
                                                YA ESTAS PARTICIPANDO
                                            </button>
                                        </form>
                                    @endif
                                @endif
                                @else
                                    <form id="form" class="form text-center" method="POST"
                                          action="{{route('join.preparticipe')}}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" id="id" name="draw_id"
                                               value="{{$draw->id}}">
                                        <button type="submit"
                                                class="btn btn-lg btn-default btn-bordered fs-15">
                                            PARTICIPAR YA!
                                        </button>
                                    </form>
                                    @endauth
                            </div>
                            <!-- /buttons -->
                        </div>

                    </li>
                    <!-- /ITEM -->
            @endforeach
        @endforeach
    @endif
    <!-- ITEM -->


        <!-- /ITEM -->
    </ul>
</div>



