<div id="loadbuy" style="position: relative;">
    <div class="loadbuy clearfix shop-list-options mb-20 pull-right">
        {{ $buys->links() }}
    </div>
    <ul class="shop-item-list row list-inline m-0">
        @if(count($buys) >0)
            @foreach ($buys->chunk(4) as $chunk)
                @foreach ($chunk as $buy)
                    <li class="col-lg-3 col-sm-3">

                        <div class="shop-item">

                            <div class="thumbnail">
                                <!-- product image(s) -->
                                <a class="shop-item-image" href="/beneficios/{{$buy->slug}}">
                                    <img class="img-fluid" src="/storage/{{$buy->avatar}}"
                                         alt="{{$buy->alt}}"/>
                                </a>

                                <div class="shop-item-info">
                                    @if(!is_null($buy->percent))
                                    <span class="badge badge-danger" style="background-color: #d9534f">{{$buy->percent}}% de descuento </span>
                                    @endif
                                    <span class="badge badge-default" style="background-color: #5cb85c"><s>$ {{$buy->price_total}}</s></span>

                                        <span class="badge badge-success" style="background-color: #5cb85c">$ {{$buy->price_discount}}</span>
                                </div>
                                <!-- /product more info -->
                            </div>

                            <div class="shop-item-summary text-center">
                                <h2>{{$buy->name}}</h2>
                            </div>

                            <!-- buttons -->
                            <div class="shop-item-buttons text-center">
                                @auth
                                @if(auth()->user())
                                    <form id="form" method="post" class="form text-center">
                                        {{--<a href="{{$buy->url}}" target="_blank" type="button"--}}
                                           {{--class="btn btn-lg btn-default btn-bordered fs-15"><i--}}
                                                    {{--class="fa fa-shopping-cart"></i> COMPRAR--}}
                                        {{--</a>--}}
                                        <a href="/beneficios/{{$buy->slug}}"
                                           class="btn btn-lg btn-default btn-bordered fs-15 ">
                                            <i class="fa fa-shopping-cart"></i> COMPRAR
                                        </a>
                                    </form>
                                @endif
                                @else
                                    <form id="form" class="form text-center" method="post" action="{{route('benefict.prebuy')}}">
                                        {!! csrf_field() !!}
                                        {{--<input type="hidden" name="buy_id" value="{{$buy->id}}">--}}
                                        {{--<button class="btn btn-lg btn-default btn-bordered fs-15" type="submit"><i--}}
                                                    {{--class="fa fa-shopping-cart" ></i>COMPRAR</button>--}}
                                        <a href="/beneficios/{{$buy->slug}}"
                                           class="btn btn-lg btn-default btn-bordered fs-15 ">
                                            <i class="fa fa-shopping-cart"></i> COMPRAR
                                        </a>
                                    </form>
                                    @endauth
                            </div>
                            <!-- /buttons -->
                        </div>

                    </li>
                    <!-- /ITEM -->
            @endforeach
        @endforeach
    @endif
    </ul>
</div>
