@section('meta-title','Comunidad de Misiones Online')
@section('meta-description', 'Comunidad Misiones Online es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    <section id="slider" class="fullheight"
             style="background:url({{asset('client/demo_files/images/particles_bg.jpg')}})">
        <span class="overlay dark-2"><!-- dark overlay [0 to 9 opacity] --></span>

        <canvas id="canvas-particle" data-rgb="156,217,249"><!-- CANVAS PARTICLES --></canvas>

        <div class="display-table">
            <div class="display-table-cell vertical-align-middle">

                <div class="container text-center">

                    <h1 class="m-0 wow fadeInUp" data-wow-delay="0.4s">
                        Comunidad de Misiones Online
                        <!--
                            TEXT ROTATOR
                            data-animation="fade|flip|flipCube|flipUp|spin"
                        -->
                        <br>
                        <p style="font-size: 40px;" class="rotatemorfs">Disfruta de
                            <span class="rotate theme-color" id="js-rotating">
									promociones, concursos, descuentos, beneficios
                        </span></p>
                    </h1>

                </div>

            </div>
        </div>

    </section>
    @if(count($buys) >0)

    <section class="page-header page-header-xs">
        <div class="container">
            <h1 align="center"><b>BENEFICIOS</b></h1>
        </div>
    </section>
    <section style="padding: 0px;">
        <div class="container">
            <!-- /LIST OPTIONS -->
                <div class="buys">
                    @include('client.home.buys')

                </div>
        </div>
    </section>
    @endif

    @if(count($draws) >0)
    <section class="page-header page-header-xs">
        <div class="container">
            <h1 align="center"><b>CONCURSOS</b></h1>
        </div>
    </section>
    <section style="padding: 0px;">
        <div class="container">
            <!-- /LIST OPTIONS -->

                <div class="draws">
                    @include('client.home.draws')

                </div>
        </div>
    </section>
    @endif

@endsection

@push('scripts')

@include('includes.menu')
@endpush




