@if (! $errors->isEmpty())
    <div class="alert alert-danger">
        <p><strong></strong>Se han detectado errores en la carga del formulario de registro, revise los dato faltantes</p>
        <ul>
        </ul>
    </div>
@endif