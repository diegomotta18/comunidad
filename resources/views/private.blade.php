@extends('layouts.admin')

@section('content')
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary well">
                <div class="panel-heading">Bienvenido</div>

                <div class="panel-body">
                    <b>{{auth()->user()->name}}</b>, usted se encuentra en el panel administrativo de la Comunidad Misiones Online.
                    {{--<passport-clients></passport-clients>--}}

                    {{--<passport-personal-access-tokens></passport-personal-access-tokens>--}}

                    {{--<passport-authorized-clients></passport-authorized-clients>--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
