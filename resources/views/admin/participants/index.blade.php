@extends('layouts.admin')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <input type="hidden" id="draw_id" name="draw_id" value="{{$draw->id}}">
        <div class="col-lg-10">
            <h2>Participantes</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('draws.index')}}">Sorteos</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Participantes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group" >
                <button onclick="javascript:window.history.back()"style="margin-top: 40px;" class="btn  btn-default pull-right" ><i class="fa fa-arrow-circle-left"></i> Volver</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table style="font-size: 12px;" class="table table-striped table-bordered table-hover " id="participants" >
                                <thead>
                                <tr>
                                    <th class="text-center">Nombres</th>
                                    <th class="text-center">Apellido</th>
                                    <th class="text-center">Nº de documento</th>
                                    <th class="text-center">Fecha de Nacimiento</th>
                                    <th class="text-center">Telefono</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Respuestas</th>

                                </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                            <div class="form-group">
                                <a  style=" margin-top: 40px;" href="{{route('draws.export',$draw->id)}}"
                                        class="btn  btn-primary pull-left"><i class="fa fa-file-excel-o"></i> Exportar
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
