@extends('layouts.admin')
@push('styles')
<link href="{{ asset('/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>


@endpush
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Beneficios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('buys.index')}}">Beneficios</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Nuevo beneficio
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos del beneficio</b></div>

                        <div class="ibox-content" ng-controller="BuyCont">
                            @include('partials/errors')
                            <form id="form_draw" class="form" method="post" action="{{route('buys.store')}}"
                                  enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nombre <span class="fa fa-dot-circle-o"></span>
                                            </label>
                                            <div class="input-group">
                                                <input id="nombre" type="text" class="form-control" name="nombre"
                                                       value="{{old('nombre')}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-12">
                                        <label class="control-label">Descripción <span
                                                    class="fa fa-dot-circle-o"></span></label>
                                        <textarea class="ckeditor" name="descripcion" id="descripcion" rows="10"
                                                  cols="80">{{old('descripcion')}}</textarea>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Metadescription <span
                                                        class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="metadescription" type="text" class="form-control"
                                                       name="metadescription"
                                                       value="{{old('metadescription')}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <hr>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Link de redirección <span
                                                        class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="url" type="text" class="form-control"
                                                       name="url"
                                                       value="{{old('link_de_redirección')}}"
                                                       required placeholder="http://">
                                                <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <hr>
                                    <div class="col-md-12">
                                        <label class="control-label">Tipo de fecha</label>

                                        <div class="form-group" style="display: flex;">
                                            <div class="col-md-3">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="type_date[]" @if(old('type_date.0') == 'periodo') checked @endif id="periodo_del_beneficio" value="periodo">
                                                    <label class="form-check-label" for="periodo_del_beneficio">
                                                        Periodo
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <label class="control-label">
                                                    Periodo del beneficio
                                                </label>
                                                <div class="input-group">
                                                    <input type="text" id="fechas" name="periodo_del_beneficio"
                                                           value="{{old('periodo_del_beneficio')}}"
                                                           class="form-control"/>
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: flex;">
                                            <div class="col-md-3">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="type_date[]"
                                                           id="check_fecha_especifica" value="especifico"
                                                           @if(old('type_date.0') == 'especifico') checked @endif >
                                                    <label class="form-check-label" for="check_fecha_especifica">
                                                        Específico
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">
                                                    Fecha de vencimiento
                                                </label>
                                                <div class="input-group">

                                                    <input type="text" id="fecha_de_vencimiento" name="fecha_de_vencimiento"
                                                           value="{{old('fecha_de_vencimiento')}}"
                                                           class="form-control"/>
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">
                                                    Hora de vencimiento
                                                </label>
                                                <div class="form-group">
                                                    <div class='input-group' id='hora_de_vencimiento'>
                                                        <input type='text' class="form-control" name="hora_de_vencimiento" value="{{old('hora_de_vencimiento')}}"/>
                                                        <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-time"></span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">
                                                    Codigo del voucher
                                                </label>
                                                <div class="form-group">
                                                    <div class='input-group'>
                                                        <input type='text' class="form-control" name="codigo_de_vaucher" value="{{old('code')}}"/>
                                                        <span class="input-group-addon">
                                                                <span class="fa fa-hashtag"></span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Precio total <span
                                                        class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">

                                                <input type="number" name="precio_total"
                                                       ng-value="{{old('precio_total')}}"
                                                       class="form-control"
                                                       ng-model="precio_total"/>
                                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Descuento % (ingrese un valor entero)</label>
                                            <div class="input-group">
                                                <input type="number" name="descuento" pattern="\d*"
                                                       ng-value="{{old('descuento')}}"
                                                       class="form-control"
                                                       ng-model="descuento"
                                                       ng-change="calculateDiscount(descuento,precio_total)"/>
                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Precio Descontado <span
                                                        class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input type="text" name="precio_descontado"
                                                       ng-value=""
                                                       ng-model="price_discount"
                                                       class="form-control"/>
                                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Imagen <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="fileinput fileinput-new input-group"
                                                 data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar archivo</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" name="imagen"/>
                                        </span>
                                                <a href="#"
                                                   class="input-group-addon btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Eliminar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Descripción de la imagen <span
                                                        class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="alt" type="text" class="form-control"
                                                       name="descripcion_de_la_imagen"
                                                       value="{{old('descripcion_de_la_imagen')}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="pull-right">
                                                <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                            class="fa fa-floppy-o"></i>
                                                    Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>


<script>
    $(document).ready(function () {

         $('input[name="periodo_del_beneficio"]').daterangepicker({
             autoUpdateInput: false,
             "showDropdowns": true,
             "autoApply": true,
             "locale": {
                 "format": "DD/MM/YYYY",
                 "separator": " - ",
                 "applyLabel": "Apply",
                 "cancelLabel": "Cancel",
                 "fromLabel": "From",
                 "toLabel": "To",
                 "customRangeLabel": "Custom",
                 "weekLabel": "W",
                 "daysOfWeek": [
                     "Dom",
                     "Lun",
                     "Mar",
                     "Mie",
                     "Jue",
                     "Vie",
                     "Sab"
                 ],
                 "monthNames": [
                     "Enero",
                     "Febrero",
                     "Marzo",
                     "Abril",
                     "Mayo",
                     "Junio",
                     "Julio",
                     "Agosto",
                     "Septiembre",
                     "Octubre",
                     "Noviembre",
                     "Diciembre"
                 ],

                 "firstDay": 1
             },
             "opens": "center"

         }, function(start, end, label) {
             $('input[name="periodo_del_beneficio"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
         });

        $('input[name="fecha_de_vencimiento"]').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10),
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],

                "firstDay": 1
            },
        }, function(start, end, label) {
            $('input[name="fecha_de_vencimiento"]').val(start.format('DD/MM/YYYY'));
        });

    $('#hora_de_vencimiento').datetimepicker({
        format: 'LT',
        allowInputToggle: true

    });
    });
</script>
@endpush