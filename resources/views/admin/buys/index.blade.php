@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Beneficios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Beneficios
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group" >
                <button onclick="javascript:window.history.back()"style="margin-top: 40px;" class="btn  btn-default pull-right" ><i class="fa fa-arrow-circle-left"></i> Volver</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <a href="{{route('buys.create')}}" class="btn btn-primary create"><i
                                    class="glyphicon glyphicon-plus"></i>Nuevo</a></h2>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" width="100%" cellspacing="0"
                                   id="buys">
                                <thead>
                                <tr>
                                    <th class="text-center">Imagen</th>
                                    <th class="text-center">Titulo</th>
                                    <th class="text-center">Precio total</th>
                                    <th class="text-center">Precio descontado</th>

                                    <th class="text-center">Fecha de creacion</th>
                                    <th class="text-center">Periodo del beneficio</th>
                                    <th class="text-center">Estado</th>

                                    <th>Acciones</th>
                                    {{--<th>Participantes</th>--}}

                                </tr>
                                </thead>
                                <tbody class="text-center">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('admin.buys.delete')
@endsection
