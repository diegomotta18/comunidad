@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Beneficios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('buys.index')}}">Beneficios</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Ver beneficio
                    </strong>
                </li>

            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del beneficio</b></div>

                    <div class="ibox-content">
                        <form id="frmCompany" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-image margin-r-5"></i> Imagen</strong>
                                    <br>
                                    <p></p><img width="100px" src="{{Storage::url($buy->avatar)}}"></p>
                                    <div class="hr-line-dashed"></div>
                                </div>

                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Nombre</strong>
                                    <br>

                                    <p class="text-muted"> {{$buy->name}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Descripción </strong>
                                    <br>

                                    <p class="text-muted"> {!! $buy->description !!}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Metadescription</strong>
                                    <br>

                                    <p class="text-muted"> {{$buy->metadescription}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Link de redirección</strong>
                                    <br>

                                    <p class="text-muted"> {{$buy->url}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                @if(!is_null($buy->date_init))
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i> Periodo del
                                            beneficio</strong>
                                        <br>

                                        <p class="text-muted"> {{$buy->date_init." - ".$buy->date_finish}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                @elseif($buy->getDateSpecific())
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i> Fecha de
                                            vencimiento</strong>
                                        <br>

                                        <p class="text-muted"> {{$buy->getDateSpecific()}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-clock-o margin-r-5"></i> Hora de vencimiento</strong>
                                        <br>
                                        <p class="text-muted"> {{$buy->getTimeSpecific()}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-hashtag margin-r-5"></i> Código del voucher</strong>
                                        <br>
                                        <p class="text-muted"> {{$buy->code}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>

                                @endif
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Descripción de la imagen</strong>
                                    <br>

                                    <p class="text-muted"> {{$buy->alt}}</p>
                                    <div class="hr-line-dashed"></div>

                                </div>

                                <div class="col-md-12">
                                    <strong><i class="fa   fa-usd margin-r-5"></i> Precio total</strong>
                                    <br>
                                    <p class="text-muted">$ {{$buy->price_total}}</p>
                                    <div class="hr-line-dashed"></div>

                                </div>
                                @if(!is_null($buy->percent))
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-percent margin-r-5"></i> Descuento</strong>
                                        <br>
                                        <p class="text-muted">$ {{$buy->percent}}</p>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                @endif
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-usd margin-r-5"></i> Precio descontado</strong>
                                    <br>
                                    <p class="text-muted">$ {{$buy->price_discount}}</p>
                                    <div class="hr-line-dashed"></div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection