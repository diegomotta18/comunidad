@extends('layouts.admin')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Usuarios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('users.index')}}">Usuarios</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Editar usuario
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del usuario</b></div>
                    <div class="ibox-content">

                        @include('partials/errors')

                        <form class="form" method="post" action="{{route('users.update', $user->id)}}">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nombre</label>
                                        <div class="input-group">
                                            <input id="nombre" type="text" class="form-control" name="nombre"
                                                   value="{{$user->name}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">E-Mail Address</label>
                                        <div class="input-group">
                                            <input id="emailUsuario" type="email" class="form-control" name="email"
                                                   value="{{$user->email}}" required>
                                            <span class="input-group-addon"><i class="fa fa-envelope "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Contraseña</label>
                                        <div class="input-group">
                                            <input id="passwordUsuario" type="password" class="form-control"
                                                   name="contraseña"
                                                   >
                                            <span class="input-group-addon"><i class="fa fa-lock "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Roles</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-list "></i></span>

                                            <select id="rolesUsuario" name="rol" class="form-control bootstrap-select">
                                                <option value="god" {{ $user->role == 'god' ? 'selected' : '' }}>Super
                                                    administrador
                                                </option>
                                                <option value="adm" {{ $user->role == 'adm' ? 'selected' : '' }}>
                                                    Administrador
                                                </option>
                                                <option value="user" {{ $user->role == 'user' ? 'selected' : '' }}>
                                                    Cliente
                                                </option>
                                                <option value="employee" {{ $user->role == 'employee' ? 'selected' : '' }}>
                                                    Empleado
                                                </option>
                                                <option value="viewer"{{ $user->role == 'viewer' ? 'selected' : '' }}>
                                                    Usuario
                                                </option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <p></p>
                                @if(!is_null($user->person))
                                    <person personaid="{{$user->person->id}}"></person>
                                @else
                                    <person></person>

                                @endif
                                {{--@if(!is_null($user->company()->first()))--}}
                                {{--<div class="col-md-6" ng-controller="CompanyCont">--}}
                                {{--<div class="form-group">--}}
                                {{--<label class="control-label">Empresa</label>--}}
                                {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon"><i class="fa fa-building"></i></span>--}}
                                {{--<select id="empresa"--}}
                                {{--name="empresa"--}}
                                {{--ng-model="company"--}}
                                {{--class="form-control bootstrap-select"--}}
                                {{--ng-init="changeValueSelectCompany({{$user->company()->first()->id}})"--}}
                                {{--ng-options="company.nombre for company in companies track by company.id"--}}
                                {{-->--}}
                                {{--<option value="" selected="">-- Seleccionar --</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@else--}}
                                {{--<div class="col-md-6" ng-controller="CompanyCont">--}}
                                {{--<div class="form-group">--}}
                                {{--<label class="control-label">Empresa</label>--}}
                                {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon"><i class="fa fa-building"></i></span>--}}
                                {{--<select id="empresa" name="empresa"--}}
                                {{--class="form-control bootstrap-select">--}}
                                {{--<option value="" selected="">-- Seleccionar --</option>--}}
                                {{--<option ng-repeat="company in companies"--}}
                                {{--value="@{{company.id}}">@{{company.nombre}}--}}
                                {{--</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                        class="fa fa-floppy-o"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection