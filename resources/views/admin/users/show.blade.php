@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Usuarios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('users.index')}}">Usuarios</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Ver usuario
                    </strong>
                </li>

            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del usuario</b></div>

                    <div class="ibox-content">
                        <form id="frmCompany" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-envelope margin-r-5"></i> Email</strong>
                                    <br>

                                    <p class="text-muted"> {{$user->email}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="	glyphicon glyphicon-credit-card margin-r-5"></i> Rol</strong>
                                    <br>
                                    <p class="text-muted"> {{$user->role}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                @if(!is_null($person))
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-font margin-r-5"></i> Nombre y Apellido</strong>
                                        <br>

                                        <p class="text-muted"> {{$person->first_name." ".$person->last_name}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i> Fecha de Inscripcion</strong>
                                        <br>

                                        <p class="text-muted"> {{$user->created_at}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                @endif


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection