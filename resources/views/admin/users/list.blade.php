@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Usuarios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Usuarios
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group" >
                <button onclick="javascript:window.history.back()"style="margin-top: 40px;" class="btn  btn-default pull-right" ><i class="fa fa-arrow-circle-left"></i> Volver</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <a href="{{route('users.create')}}" class="btn btn-primary create" id="nuevo-user"><i
                                    class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                                   id="usuarios">
                                <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Persona</th>
                                    <th>Correo</th>
                                    <th>Rol</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <a  style=" margin-top: 40px;" href="{{route('users.export')}}"
                                    class="btn  btn-primary pull-left"><i class="fa fa-file-excel-o"></i> Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.users.delete')
@endsection
