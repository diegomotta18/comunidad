@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Usuarios</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('users.index')}}">Usuarios</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Nuevo usuario
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight" >
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del usuario</b></div>

                    <div class="ibox-content">
                        @include('partials/errors')

                        <form class="form" method="post" action="{{route('users.store')}}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nombre</label>
                                        <div class="input-group">
                                            <input id="nombre" type="text" class="form-control" name="nombre" value=""
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">E-Mail Address</label>
                                        <div class="input-group">
                                            <input id="emailUsuario" type="email" class="form-control" name="email"
                                                   value=""
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-envelope "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Contraseña</label>
                                        <div class="input-group">
                                            <input id="passwordUsuario" type="password" class="form-control"
                                                   name="contraseña"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-lock "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Roles</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-list "></i></span>
                                            <select id="rolesUsuario" name="rol" class="form-control bootstrap-select">
                                                <option value="god">Super administrador</option>
                                                <option value="adm">Administrador</option>
                                                <option value="user">Cliente</option>
                                                <option value="employee">Empleado</option>

                                                <option value="viewer">Usuario</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <person></person>
                                {{--<div class="col-md-6" ng-controller="CompanyCont">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Empresa</label>--}}
                                        {{--<div class="input-group">--}}
                                            {{--<span class="input-group-addon"><i class="fa fa-building"></i></span>--}}
                                            {{--<select id="empresa" name="empresa"--}}
                                                    {{--class="form-control bootstrap-select">--}}
                                                {{--<option value="" selected="">-- Seleccionar --</option>--}}
                                                {{--<option ng-repeat="company in companies"--}}
                                                        {{--value="@{{company.id}}">@{{company.nombre}}--}}
                                                {{--</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                        class="fa fa-floppy-o"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection