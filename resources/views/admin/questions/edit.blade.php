@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Cuestionario</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Modificar cuestionarios
                    </strong> <span class="label label-info">{{$draw->name}}</span>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight" ng-controller="QuestionCont">
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Cuestionarios</b></div>
                    <div class="ibox-content">
                        @if(!is_null($questions))
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" width="100%"
                                       cellspacing="0"
                                >
                                    <thead>
                                    <tr>
                                        <th>Pregunta</th>
                                        <th>Opciones</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($questions as $question)
                                        <tr>
                                            <td>{{ $question->question }}</td>
                                            <td>  @foreach($question->options as $option)
                                                    <li>
                                                        <span>{{$option->label_option}}</span>
                                                    </li>
                                                @endforeach
                                            </td>
                                            <td>
                                                <a href="{{url('draws/'.$draw->id.'/questions/'.$question->id)}}" type="button" class="btn btn-warning btn-xs" ><i class="fa fa-edit"></i>
                                                </a>
                                                <a href="{{route('question.destroy')}}"
                                                   onclick="event.preventDefault();
                                                        document.getElementById('delete-cuestion').submit();" class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i>
                                                    <form id="delete-cuestion" method="post" action="{{route('question.destroy')}}">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="question_id" value="{{$question->id}}">
                                                        <input type="hidden" name="draw_id" value="{{$draw->id}}">

                                                    </form>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Modificar datos de la pregunta</b></div>
                    <div class="ibox-content">
                        @include('partials/errors')
                        <form class="form" method="post" action="{{route('question.update',$quest->id )}}">
                            {!! csrf_field() !!}
                            {{ method_field('PUT')}}
                            <input type="hidden" value="{{$draw->id}}" name="draw_id">
                            <div class="form-group">
                                <label class="control-label">Pregunta</label>
                                <div class="input-group">
                                    <input id="question" type="text" class="form-control" name="pregunta"
                                           value="{{$quest->question}}"
                                           required>
                                    <span class="input-group-addon"><i class="fa fa-question "></i></span>
                                </div>
                            </div>
                            <label class="control-label">Tipo de respuesta</label>
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" id="smt-fld-1-2" name="tipo_de_respuesta"
                                           value="radio" @if($quest->input_type == 'radio') checked @endif>radio</label>
                                <label class="radio-inline">
                                    <input type="radio" id="smt-fld-1-3" name="tipo_de_respuesta"
                                           value="checkbox" @if($quest->input_type == 'checkbox') checked @endif>checkbox</label>
                            </div>
                            <label class="control-label">Respuestas</label>
                            <a class="btn btn-default" type="button" ng-click="add()"><i
                                        class="fa fa-plus-circle"></i></a>
                            @foreach($quest->options()->get() as $opt)
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" name="delete" class="btn btn-danger"><i class="fa fa-trash"></i></button></span>
                                        <input type="text" class="form-control" placeholder="" name="respuestas[]"
                                               value="{{$opt->label_option}}">
                                        <input type="hidden" name="option_id" value="{{$opt->id}}">
                                        <input type="hidden" name="option_value" value="{{$opt->label_option}}">

                                        <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group" ng-repeat="item in items">
                                <div class="input-group">
                                    <span class="input-group-btn"><button class="btn btn-danger"><i
                                                    class="fa fa-trash"></i></button></span>

                                    <input type="text" class="form-control" placeholder="" name="respuestas[]"
                                           ng-model="item.option"
                                           value="">
                                    <span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" name="cancel" class="btn btn-default"><i
                                            class="fa fa-remove"></i>
                                    Cancelar
                                </button>
                                <button type="submit" class="btn btn-primary"><i
                                            class="fa fa-floppy-o"></i>
                                    Guardar
                                </button>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection
