@extends('layouts.admin')

@push('styles')
<link href="{{ asset('/admin/js/plugins/sweetalert2/sweetalert.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Personas</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Personas
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <a href="{{route('persons.create')}}" class="btn btn-primary create"><i
                                    class="glyphicon glyphicon-plus"></i>Nuevo</a></h2>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" width="100%" cellspacing="0"
                                   id="">
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Nombre y Apellido</th>
                                    <th>Nº de Documento</th>
                                    <th>Fecha de nacimiento</th>
                                    <th>Ciudad</th>
                                    <th>Concursos</th>

                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($persons as $person)
                                    <tr>
                                        @if(!is_null($person->email))
                                            <td>{{$person->email}}</td>
                                        @elseif(!is_null($person->user))
                                            <td>{{$person->user->email}}</td>
                                        @else
                                            <td></td>

                                        @endif
                                        <td>{{$person->first_name}} {{$person->last_name}}</td>
                                        <td>{{$person->birthday}}</td>
                                        <td>{{$person->identification}}</td>
                                        @if(is_null($person->address))
                                            <td>Extranjero</td>
                                        @elseif(is_null($person->address->district_id))
                                            <td>{{$person->address->state->name}}</td>
                                        @elseif(!is_null($person->address->district_id))
                                            <td>{{$person->address->district->name}}</td>
                                        @endif
                                            <td>
                                            @if(!is_null($person->draws))
                                                <ul>
                                                @foreach($person->draws as $draw)
                                                    <li>{{$draw->name}}</li>
                                                @endforeach
                                                </ul>
                                            @else
                                            </td>
                                            @endif
                                            <td>
                                                <div class="btn-group" style="display: flex;">
                                                    <a href="{{ route('persons.show',$person->id)}}"
                                                       class="btn btn-info"
                                                       title="ver persona">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="{{ route('persons.edit',$person->id) }}"
                                                       class="btn btn-warning"
                                                       title="Editar tarea">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>

                                                    <button data-id="{{ $person->id }}" title="Eliminar persona"
                                                            class="btn btn-destroy btn-danger">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                            </td>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                @if(!is_null($persons))
                                    {{ $persons->links() }}
                                @endif
                            </div>
                            <div class="form-group">
                                <a style=" margin-top: 40px;" href="{{route('persons.export')}}"
                                   class="btn  btn-primary pull-left"><i class="fa fa-file-excel-o"></i> Exportar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
@routes

<script src="{{ asset('/admin/js/plugins/sweetalert2/sweetalert.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('persons.destroy', {id: id});
            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url, {data: {id: id}})
                    .then(function (res) {
                        if (res.status == 200) {
                            swal("Persona eliminada!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    })
                    .catch(function (res) {
                        console.log(res);
                        swal("No tiene los privilegios para realizar esta acción!", '', 'error');

                    });
            });
        });

    });
    </script>
@endpush