@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Personas</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('persons.index')}}">Personas</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Ver persona
                    </strong>
                </li>

            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos de la persona</b></div>

                    <div class="ibox-content">
                        <form id="frmCompany" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-envelope margin-r-5"></i> Email</strong>
                                        <br>
                                        @if(!is_null($person->user))
                                            <p class="text-muted"> {{$person->user->email}}</p>
                                        @else
                                            <p class="text-muted"> {{$person->email}}</p>
                                        @endif
                                        <div class="hr-line-dashed"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-font margin-r-5"></i> Nombre y Apellido</strong>
                                        <br>

                                        <p class="text-muted"> {{$person->first_name." ".$person->last_name}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-hashtag margin-r-5"></i> Nº de Documento </strong>
                                        <br>

                                        <p class="text-muted"> {{$person->identification}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i> Fecha de Nacimiento</strong>
                                        <br>

                                        <p class="text-muted"> {{$person->birthday}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <strong><i class="fa fa-phone margin-r-5"></i> Telefono</strong>
                                        <br>

                                        <p class="text-muted"> {{$person->telephone}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    @if(!is_null($country))
                                        <div class="col-md-12">
                                            <strong><i class="fa fa-map margin-r-5"></i> País</strong>
                                            <br>

                                            <p class="text-muted"> {{$country->name}}</p>

                                            <div class="hr-line-dashed"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <strong><i class="fa fa-map margin-r-5"></i> Provincia</strong>
                                            <br>

                                            <p class="text-muted"> {{$state->name}}</p>

                                            <div class="hr-line-dashed"></div>
                                        </div>
                                        @if(!is_null($district))
                                        <div class="col-md-12">
                                            <strong><i class="fa fa-map margin-r-5"></i> Provincia</strong>
                                            <br>

                                            <p class="text-muted"> {{$district->name}}</p>

                                            <div class="hr-line-dashed"></div>
                                        </div>
                                        @endif
                                    @else
                                        <div class="col-md-12">
                                            <strong><i class="fa fa-font margin-r-5"></i> Extrajero</strong>
                                            <br>

                                            <p class="text-muted"> Si</p>

                                            <div class="hr-line-dashed"></div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i> Fecha de Inscripcion</strong>
                                        <br>

                                        <p class="text-muted"> {{$person->created_at}}</p>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                </div>
                                @if(count($person->draws) > 0)
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <strong><i class="fa   fa-calendar margin-r-5"></i>Concursos </strong>
                                        <br>
                                        @foreach( $person->draws as $draw)
                                        <p class="text-muted"> &#8226;{{$draw->name}}</p>
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection