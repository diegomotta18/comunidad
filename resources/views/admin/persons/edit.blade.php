@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Personas</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('persons.index')}}">Personas</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Modificar persona
                    </strong>
                </li>
            </ol>
        </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                            class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                    </button>
                </div>
            </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">

                @include('partials/errors')

                <form class="form" method="post" action="{{route('persons.update',$person->id)}}">
                    {!! csrf_field() !!}
                    {{ method_field('PUT') }}

                    <div class="ibox float-e-margins">

                        <div class="ibox-title" align="center"><b>Datos de la persona</b></div>

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nombres</label>
                                        <div class="input-group">
                                            <input id="nombre" type="text" class="form-control" name="nombres"
                                                   value="{{$person->first_name}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Apellido</label>
                                        <div class="input-group">
                                            <input id="nombre" type="text" class="form-control" name="apellido"
                                                   value="{{$person->last_name}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nº de Documento de Identidad</label>
                                        <div class="input-group">
                                            <input id="identification" type="number" class="form-control"
                                                   name="documento" value="{{$person->identification}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-hashtag "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Fecha de Nacimiento</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="fecha_de_nacimiento"
                                                   value="{{$person->birthday}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-calendar "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Teléfono o celular</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="telefono"
                                                   value="{{$person->telephone}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-phone "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" name="email"
                                                   value="{{$person->email}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-phone "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Sexo </label>
                                        <div class="input-group">
                                            <select name="sexo" class="form-control bootstrap-select">
                                                <option value="" selected="">-- Seleccionar--</option>
                                                <option value="Masculino"
                                                        @if ($person->sex == "Masculino") selected @endif>Masculino
                                                </option>
                                                <option value="Femenino"
                                                        @if ($person->sex == "Femenino" ) selected @endif>Femenino
                                                </option>
                                            </select>
                                            <span class="input-group-addon"><i class="fa fa-genderless "></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!is_null($person->address_id))
                        <div class="ibox float-e-margins" ng-controller="AddressCont" ng-init="getStates(1)">
                            <div class="ibox-title" align="center"><b>Datos del domicilio</b></div>
                            {{--{{$person->address->country_id,$person->address->state_id,$person->address->district_id}}--}}
                            @if(!is_null($person->address->district_id))
                                <div class="ibox-content"
                                     ng-init="changeCountry({{$person->address->country_id}},{{$person->address->state_id}},{{$person->address->district_id}})">
                                    @else
                                        <div class="ibox-content"
                                             ng-init="changeCountry(1,{{$person->address->state_id}},0)">
                                            @endif


                                            <div class="row">
                                                <input type="hidden" name="pais" value="1"/>
                                                <div class="form-group col-md-12">
                                                    <label class="control-label">Nacionalidad</label>
                                                    <div class="form-group">

                                                        <div class="col-sm-10"><label class="checkbox-inline">
                                                                <input type="checkbox" value="false" id="argentino"
                                                                       name="argentino"
                                                                       @if($person->extranjero == 'No') checked @endif>
                                                                Argentino</label> <label class="checkbox-inline">
                                                                <input type="checkbox" value="true" id="extranjero"
                                                                       name="extranjero"
                                                                       @if($person->extranjero == 'Si') checked @endif>
                                                                Extranjero </label></div>
                                                    </div>

                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label class="control-label">Provincia</label>

                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                                        <select id="provincia" name="provincia"
                                                                class="form-control bootstrap-select provincia"
                                                                ng-model="state"
                                                                ng-options="state.name for state in states track by state.id"

                                                                ng-change="getDistricts(state.id)">
                                                            <option value="" selected="">-- Seleccionar Provincia--
                                                            </option>
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="control-label">Localidad</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                                        <select id="localidad" name="localidad"
                                                                class="form-control bootstrap-select localidad"
                                                                ng-model="district"
                                                                ng-options="district.name for district in districts track by district.id"
                                                                ng-change="getTowns(district.id)">
                                                            <option value="" selected="">-- Seleccionar Ciudad--
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </div>

                        </div>
                    @else
                        <div class="ibox float-e-margins" ng-controller="AddressCont" ng-init="getStates(1)">
                            <div class="ibox-title" align="center"><b>Datos del domicilio</b></div>
                            <div class="ibox-content">
                                <div class="row">
                                    <input type="hidden" name="pais" value="1"/>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Nacionalidad</label>
                                        <div class="form-group">

                                            <div class="col-sm-10"><label class="checkbox-inline">
                                                    <input type="checkbox" value="false" id="argentino"
                                                           name="argentino"
                                                           @if($person->extranjero == 'No') checked @endif>
                                                    Argentino</label> <label class="checkbox-inline">
                                                    <input type="checkbox" value="true" id="extranjero"
                                                           name="extranjero"
                                                           @if($person->extranjero == 'Si') checked @endif>
                                                    Extranjero </label></div>
                                        </div>

                                    </div>


                                    <div class="form-group col-md-6">
                                        <label class="control-label">Provincia</label>

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                            <select id="provincia" name="provincia"
                                                    class="form-control bootstrap-select provincia"
                                                    ng-model="state"
                                                    ng-options="state.name for state in states track by state.id"

                                                    ng-change="getDistricts(state.id)">
                                                <option value="" selected="">-- Seleccionar Provincia--
                                                </option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Localidad</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                            <select id="localidad" name="localidad"
                                                    class="form-control bootstrap-select localidad"
                                                    ng-model="district"
                                                    ng-options="district.name for district in districts track by district.id"
                                                    ng-change="getTowns(district.id)">
                                                <option value="" selected="">-- Seleccionar Ciudad--
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="pull-right">
                                <button type="submit" id="btn_create" class="btn btn-primary"><i
                                            class="fa fa-floppy-o"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection