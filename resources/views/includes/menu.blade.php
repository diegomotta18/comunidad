<script>
    $(document).ready(function () {
        $('#header').removeClass('fixed');
        var scrollTop = 0;
        $(window).scroll(function () {
            scrollTop = $(window).scrollTop();
            $('.counter').html(scrollTop);

            if (scrollTop >= 200) {
                $('#header').addClass('fixed');
            } else if (scrollTop < 100) {
                $('#header').removeClass('fixed');
            }

        });

    });
</script>
<script type="text/javascript">
    $(function() {
        $('body').on('click', '#loaddraw .loaddraw .pagination a', function(e) {
            e.preventDefault();
            $('#loaddraw a').css('color', '#dfecf6');
            $('#loaddraw').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

            var url = $(this).attr('href');
            getDraws(url);
            window.history.pushState("", "", url);
        });

        function getDraws(url) {
            $.ajax({
                type: 'GET',
                url : url,
                data : {value: 'draw'}
            }).done(function (data) {
                $('.draws').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        }
    });
    $(function() {
        $('body').on('click', '#loadbuy .loadbuy .pagination a', function(e) {
            e.preventDefault();
            $('#loadbuy a').css('color', '#dfecf6');
            $('#loadbuy').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

            var url = $(this).attr('href');
            getBuys(url);
            window.history.pushState("", "", url);
        });

        function getBuys(url) {
            $.ajax({
                type: 'GET',
                url : url,
                data : {value: 'buy'}
            }).done(function (data) {
                $('.buys').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        }
    });
</script>