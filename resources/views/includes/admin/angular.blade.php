
<script type="text/javascript" src="{{ asset('/plugins/angular-1.6/angular.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/smart-table/smart-table.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ng-bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ng-bootbox/ng-bootbox.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngMask/ngMask.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngcsv/angular-sanitize.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngcsv/ng-csv.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/angular-ui-select/dist/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/controller/controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/service/factory.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/service/service.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/angular_spin/angular-spinner.js')}}"></script>
