<script src="{{ asset('/admin/js/jquery.min.js')}}"></script>

<script src="{{ asset('/plugins/DataTables/datatables.js')}}"></script>
<script src="{{ asset('/plugins/select2/select2.js')}}"></script>

<script src="{{ asset('/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- Flot -->
<script src="{{ asset('/admin/js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/flot/jquery.flot.spline.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/flot/jquery.flot.pie.js')}}"></script>
<!-- Peity -->
<script src="{{ asset('/admin/js/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{ asset('/admin/js/demo/peity-demo.js')}}"></script>
<!-- Custom and plugin javascript -->
<script src="{{ asset('/admin/js/inspinia.js')}}"></script>
<script src="{{ asset('/admin/js/plugins/pace/pace.min.js')}}"></script>
<!-- jQuery UI -->
<script src="{{ asset('/admin/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- GITTER -->
<script src="{{ asset('/admin/js/plugins/gritter/jquery.gritter.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{ asset('/admin/js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('/admin/js/demo/sparkline-demo.js')}}"></script>

<!-- ChartJS-->
<script src="{{ asset('/admin/js/plugins/chartJs/Chart.min.js')}}"></script>
<script src="{{ asset('/admin/js/jasny-bootstrap.min.js')}}"></script>

<!-- Toastr -->
<script src="{{ asset('/admin/js/plugins/toastr/toastr.min.js')}}"></script>

<script src="{{ asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script src="{{ asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('js/inputs.js') }}"></script>
<script src="{{ asset('js/selects.js') }}"></script>
<script src="{{mix('js/components.js')}}"></script>

