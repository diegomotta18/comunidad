@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => 'http://comunidad.misionesonline.net'])
            <a href="http://comunidad.misionesonline.net"
               style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
                <img src="http://comunidad.misionesonline.net/img/logomol.png">
            </a>
            @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <p align="justify"><img width="110" src="http://static0.misionesonline.net/wp-content/uploads/2014/05/logomol.png">  Director: Marcelo Almada - © 2000-2017 Misiones OnLine All rights reserved Todos los derechos reservados | Dirección Postal | Coronel López 2138 - 6° piso - Posadas - Misiones - Registro de Propiedad Intelectual N° 5.197.226 | redaccion@misionesonline.net - administracion@misionesonline.net - comercial@misionesonline.net | Tel: (0376) 4425800</p>
        @endcomponent
    @endslot
@endcomponent
