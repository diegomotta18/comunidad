@component('mail::message')
# Hola, {{$user->name}}

Usted recibió este email porque se registró en la Comunidad de Misiones Online, realice un click en el siguiente botón para aceptar los datos de su cuenta y redirigirse al sitio nuevamente.

**Los datos de su cuenta son los siguiente:**

Nombre y Apellido: **{{$person->first_name." ".$person->last_name}}**<br>
Nº de Documento: **{{$person->identification}}**<br>
Fecha de nacimiento: **{{$person->birthday}}**<br>
Correo: **{{$user->email}}**<br>

@component('mail::button', ['url' => $link])
Confirmar cuenta
@endcomponent

Antes cualquier inconsistencia de sus datos puede modificarlo desde el menu superior, **Mi perfil** y opcion **Configurar cuenta**, una vez confirmado la cuenta.

Gracias,<br>
Comunidad de Misiones Online
@endcomponent
