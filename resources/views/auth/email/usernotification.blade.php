@component('mail::message')

Recibio este correo porque ha sido registrado y asignado como usuario administrador.

Usuario: **{{$user->email}}**

Comunidad de Misiones Online
@endcomponent
