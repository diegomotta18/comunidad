@component('mail::message')

Gracias por participar **{{$person->first_name." ".$person->last_name}}**
    en el sorteo **{{$draw->name}}**.

El ganador del sorteo sera comunidado por e-mail en el dia de la fecha **{{$draw->date_result}}**.

Saludos,<br>Comunidad de Misiones Online
@endcomponent
