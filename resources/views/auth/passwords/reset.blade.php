@section('meta-title','Reestablecer contraseña')
@section('meta-description', 'Comunidad MisionesOnline es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@extends('layouts.app')

@section('content')

    <section style="margin-top:40px;">
        <div class="container">
            <div class="tab-content">
                <!-- LOGIN -->
                <div class="tab-pane active" id="login">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
                            <div class="box-static box-transparent box-bordered p-30" style="margin-top: -64px;">
                                <div class="box-title mb-30">
                                    <h2 class="fs-20 text-center">Reestablecer contraseña</h2>
                                </div>
                                <form class="m-0 sky-form" method="POST" action="{{ route('password.request') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="alert alert-info">
                                            Ingrese su email de comunidad, la nueva contraseña y la confirmación de la contraseña.
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="token" value="{{ $token }}">

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">E-Mail</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input id="email" type="email" class="form-control" name="email"
                                                           value="{{ $email or old('email') }}" required autofocus>
                                                </label>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Contraseña</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-lock"></i>
                                                    <input id="password" type="password" class="form-control"
                                                           name="password"
                                                           required>
                                                </label>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="password-confirm">Confirmar Contraseña</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-lock"></i>
                                                    <input id="password-confirm" type="password" class="form-control"
                                                           name="password_confirmation" required>
                                                </label>
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div style="text-align: center;">
                                                <button type="submit" class="btn btn-primary"><i
                                                            class="fa fa-check"></i>
                                                    REINICIAR CONTRASEÑA
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
