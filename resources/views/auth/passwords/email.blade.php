@section('meta-title','Reestablcer contraseña')
@section('meta-description', 'Comunidad MisionesOnline es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@extends('layouts.app')
@section('content')

    <section style="margin-top:40px;">
        <div class="container">
            <div class="tab-content">
                <!-- LOGIN -->
                <div class="tab-pane active" id="login">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
                            <div class="box-static box-transparent box-bordered p-30" style="margin-top: -64px;">
                                <div class="box-title mb-30">
                                    <h2 class="fs-20 text-center">Reestablecer contraseña</h2>
                                </div>


                                <form class="m-0 sky-form" method="POST" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        @if (session('status'))
                                            <div class="alert alert-success">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="alert alert-info">
                                            Ingrese su email de comunidad para enviarle un correo electrónico de reestablecimiento de contraseña.
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">E-Mail</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input id="email" type="email" class="form-control" name="email"
                                                           value="{{ old('email') }}" required>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su e-mail</b>
                                                </label>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                     <strong>{{ $errors->first('email') }}</strong>
                                                     </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div style="text-align: center;">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>
                                                ENVIAR
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


