@section('meta-title','Registro')
@section('meta-description', 'Comunidad MisionesOnline es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@extends('layouts.app')
@section('content')

    <section style="margin-top:40px;">
        <div class="container">
            <div class="tab-content">
                <!-- LOGIN -->
                <div class="tab-pane active" id="login">
                    <div class="row" ng-controller="AddressCont">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">


                            <div class="box-static box-transparent box-bordered p-30" style="margin-top: -64px;">
                                <div class="box-title mb-30">
                                    <h2 class="fs-20 text-center">Registro a la Comunidad Misiones Online</h2>
                                </div>
                                @include('partials/registererror')

                                <form class="m-0 sky-form" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('nombres') ? ' has-error' : '' }}">
                                                <label>(*) Nombres</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-font"></i>
                                                    <input required="" type="text" name="nombres"
                                                           value="{{ old('nombres') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su primer y
                                                        segundo nombre</b>
                                                </label>
                                                @if ($errors->has('nombres'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('nombres') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                                                <label>(*) Apellido</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-font"></i>
                                                    <input required="" type="text" name="apellido"
                                                           value="{{ old('apellido') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su apellido</b>
                                                </label>
                                                @if ($errors->has('apellido'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('apellido') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('documento') ? ' has-error' : '' }}">
                                                <label>(*) Nº de Documento de Identidad</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-hashtag"></i>
                                                    <input required="" type="number" name="documento"
                                                           value="{{ old('documento') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su Nº de
                                                        documento de identidad</b>
                                                </label>
                                                @if ($errors->has('documento'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('documento') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }}">
                                                <label>(*) Sexo
                                                </label>

                                                <div class="form-select">
                                                    <select name="sexo"  class="form-control">
                                                        <option value="" selected="">-- Seleccionar--</option>
                                                        <option value="Masculino" @if (\Illuminate\Support\Facades\Input::old("sexo") == "Masculino") selected @endif>Masculino</option>
                                                        <option value="Femenino"@if (\Illuminate\Support\Facades\Input::old("sexo") == "Femenino") selected @endif>Femenino</option>
                                                        <option value="Otro">Otro</option>
                                                    </select>
                                                </div>

                                                @if ($errors->has('sexo'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('sexo') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('fecha_de_nacimiento') ? ' has-error' : '' }}">
                                                <label>(*) Fecha de nacimiento</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-calendar"></i>
                                                    <input required="" type="text" name="fecha_de_nacimiento"
                                                           value="{{ old('fecha_de_nacimiento') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su fecha de
                                                        nacimiento</b>
                                                </label>
                                                @if ($errors->has('fecha_de_nacimiento'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('fecha_de_nacimiento') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                                                <label>(*) Teléfono o celular</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-user"></i>
                                                    <input required="" type="number" name="telefono"
                                                           value="{{ old('telefono') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su telefono o
                                                        celular</b>
                                                </label>
                                                @if ($errors->has('telefono'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('telefono') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <label>(*) Nacionalidad</label>
                                            <div @if ($errors->has('argentino') || ($errors->has('extranjero'))) class="form-group has-error"
                                                 @else class="form-group" @endif >
                                                <div style="display: flex; ">
                                                    </label>
                                                    <label class="checkbox  m-0"><input id="argentino"
                                                                                        class="checked-agree"
                                                                                        type="checkbox" name="argentino"
                                                                                        @if((old('argentino'))) checked @endif><i></i>Argentino</label>
                                                    <div class="form-group">
                                                        &nbsp;
                                                    </div>
                                                    </label>
                                                    <label class="checkbox  m-0"><input id="extranjero"
                                                                                        class="checked-agree"
                                                                                        type="checkbox"
                                                                                        name="extranjero"
                                                                                        @if((old('extranjero'))) checked @endif><i></i>Extranjero</label>
                                                </div>
                                                @if ($errors->has('argentino') || $errors->has('extranjero'))
                                                    <span class="help-block">
                                                            <strong>Debe seleccionar la nacionalidad</strong>
                                                        </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                                                <label>(*) País
                                                </label>

                                                <div class="form-select">

                                                    <select id="pais" name="pais"
                                                            class="form-control"
                                                            ng-model="country"
                                                            ng-options="country.name for country in countries track by country.id"
                                                            ng-change="getStates(country.id)">
                                                        <option value="" selected="">-- Seleccionar Pais--</option>
                                                    </select>

                                                </div>

                                                @if ($errors->has('pais'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('pais') }}</strong>
                                                        </span>
                                                @endif
                                            </div>

                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('provincia') ? ' has-error' : '' }}">

                                                <label>(*) Provincia
                                                </label>
                                                <div class="form-select">

                                                    <select id="provincia" name="provincia"
                                                            class="form-control"
                                                            ng-model="state"
                                                            ng-options="state.name for state in states track by state.id"
                                                            ng-change="getDistricts(state.id)">
                                                        <option value="" selected="">-- Seleccionar Provincia--
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('provincia'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('provincia') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('localidad') ? ' has-error' : '' }}">

                                                <label>(*) Localidad
                                                </label>
                                                <div class="form-select">
                                                    <select id="localidad" name="localidad"
                                                            class="form-control"
                                                            ng-model="district"
                                                            ng-options="district.name for district in districts track by district.id"
                                                            ng-change="getTowns(district.id)">
                                                        <option value="" selected="">-- Seleccionar Localidad--
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('localidad'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('localidad') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        {{--<div class="col-md-12">--}}
                                        {{--<div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">--}}
                                        {{--<label>Ciudad--}}
                                        {{--</label>--}}
                                        {{--<div class="form-select">--}}
                                        {{--<select id="" name="ciudad"--}}
                                        {{--class="form-control"--}}
                                        {{--ng-model="town"--}}
                                        {{--ng-options="town.name for town in towns track by town.id">--}}
                                        {{--<option value="" selected="">-- Seleccionar Ciudad----}}
                                        {{--</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                        {{--@if ($errors->has('ciudad'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('ciudad') }}</strong>--}}
                                        {{--</span>--}}
                                        {{--@endif--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">(*) Email</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="" type="text" name="email"
                                                           value="{{ old('email') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su correo
                                                        electrónico</b>
                                                </label>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">(*) Contraseña</label>
                                                <label class="input mb-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="" type="password" name="password"
                                                           value="{{ old('password') }}">
                                                    <b class="tooltip tooltip-bottom-right">Ingrese su contraseña
                                                    </b>
                                                </label>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">

                                            <label for="password-confirm">(*) Confirmar
                                                contraseña
                                            </label>
                                            <label class="input mb-10">
                                                <i class="ico-append fa fa-envelope"></i>
                                                <input id="password-confirm" type="password"
                                                       class="form-control" name="password_confirmation"
                                                       required>
                                                <b class="tooltip tooltip-bottom-right">Confirme la contraseña
                                                </b>
                                            </label>
                                        </div>
                                    </div>
                                    <label>(*) Datos obligatorios</label>
                                    <hr/>
                                    <div class="form-group{{ $errors->has('términos_y_condiciones') ? ' has-error' : '' }}">
                                        <label class="checkbox m-0"><input class="checked-agree" type="checkbox"
                                                                           name="términos_y_condiciones"
                                                                           @if((old('términos_y_condiciones'))) checked @endif><i></i>Acepto
                                            los terminos
                                            del servicio <a
                                                    href="#" data-toggle="modal" data-target="#termsModal"> Ver
                                                terminos
                                            </a></label>
                                        @if ($errors->has('términos_y_condiciones'))
                                            <span class="help-block">
                                                            <strong>{{ $errors->first('términos_y_condiciones') }}</strong>
                                                        </span>
                                        @endif
                                    </div>
                                    <hr/>
                                    <div style="text-align: center;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>
                                            REGISTRAR
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
