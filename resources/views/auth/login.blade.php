@section('meta-title','Registro')
@section('meta-description', 'Comunidad MisionesOnline es el Programa de Beneficios exclusivo para lectores de MisionesOnline')
@include('includes.analitics')
@extends('layouts.app')
@section('content')
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Login</div>--}}

    {{--<div class="panel-body">--}}
    {{--<form class="form-horizontal" method="POST" action="{{ route('login') }}">--}}
    {{--{{ csrf_field() }}--}}

    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
    {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

    {{--@if ($errors->has('email'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
    {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

    {{--@if ($errors->has('password'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('password') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<div class="checkbox">--}}
    {{--<label>--}}
    {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-8 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--Login--}}
    {{--</button>--}}

    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
    {{--Forgot Your Password?--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- wrapper -->

    <section style="margin-top:40px;">
        <div class="container">


            <div class="tab-content">

                <!-- LOGIN -->
                <div class="tab-pane active" id="login">

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">


                            <div class="box-static box-transparent box-bordered p-30" style="margin-top: -64px;">
                                <div class="box-title mb-30">
                                    <h2 class="fs-20 text-center">Ingreso a Comunidad Misiones Online</h2>
                                </div>

                                <form class="sky-form" method="POST" action="{{ route('login') }}" autocomplete="off">
                                    {{ csrf_field() }}
                                    <div class="clearfix">

                                        <!-- Email -->
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                            <label>Email</label>
                                            <label class="input mb-10">
                                                <i class="ico-append fa fa-envelope"></i>
                                                <input id="email" name="email" value="{{ old('email') }}" required=""
                                                       autofocus>
                                                <b class="tooltip tooltip-bottom-right">Ingrese el email de la
                                                    cuenta</b>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                      <strong>{{$errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </label>
                                        </div>

                                        <!-- Password -->
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                            <label>Contraseña</label>
                                            <label class="input mb-10">
                                                <i class="ico-append fa fa-lock"></i>
                                                <input id="password" type="password" name="password" required="">
                                                <b class="tooltip tooltip-bottom-right">Ingrese la contraseña de la
                                                    cuenta</b>
                                            </label>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-tip pt-25">
                                                <a class="no-text-decoration mb-20" href="{{ route('password.request') }}">Olvido la
                                                    contraseña?</a>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                            <button class="btn btn-primary"><i class="fa fa-check"></i> INGRESAR
                                            </button>

                                        </div>

                                    </div>

                                </form>

                                <hr/>

                                <div class="text-center">
                                    <div class="mb-20">&ndash; No tienes una cuenta en comunidad? &ndash;</div>

                                    <a href="{{route('register')}}" class="btn btn-block btn-info">
                                        <i class="fa fa-edit"></i> Registrate
                                    </a>

                                    {{--<a class="btn btn-block btn-social btn-twitter mt-10">--}}
                                        {{--<i class="fa fa-twitter"></i> Ingresar con Twitter--}}
                                    {{--</a>--}}

                                    {{--<a class="btn btn-block btn-social btn-google mt-10">--}}
                                        {{--<i class="fa fa-google-plus"></i> Ingresar con Google+--}}

                                    {{--</a>--}}


                                </div>

                            </div>

                        </div>
                    </div>

                </div><!-- /LOGIN -->

            </div>

        </div>
    </section>
    <!-- / -->



@endsection
