@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Sorteos</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('draws.index')}}">Sorteos</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Editar sorteo
                    </strong>
                </li>

            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group" >
                <button onclick="javascript:window.history.back()"style="margin-top: 40px;" class="btn  btn-default pull-right" ><i class="fa fa-arrow-circle-left"></i> Volver</button>
            </div>
        </div>
    </div>
    <div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos del sorteo</b></div>

                        <div class="ibox-content">
                            @include('partials/errors')
                            <form id="form_draw" class="form" method="post"
                                  action="{{route('draws.update', $draw->id)}}"
                                  enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                {{ method_field('PUT') }}
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nombre <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="nombre" type="text" class="form-control" name="nombre"
                                                       value="{{ $draw->name}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-12">
                                        <label class="control-label">Descripción <span class="fa fa-dot-circle-o"></span></label>
                                        <textarea class="ckeditor" name="descripcion" id="descripcion" rows="10"
                                                  cols="80">{{$draw->description}}</textarea>
                                    </div>
                                    <br>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Metadescription <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="metadescription" type="text" class="form-control"
                                                       name="metadescription"
                                                       value="{{$draw->metadescription}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Periodo de participación <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">

                                                <input type="text" id="fechas" name="periodo_de_participacion"
                                                       value="{{$draw->date_init." - ".$draw->date_finish}}"
                                                       class="form-control"/>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Fecha de resultado <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">

                                                <input type="text" id="fecha_de_resultado" name="fecha_de_resultado"
                                                       value="{{$draw->date_init}}" class="form-control"/>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Imagen <span class="fa fa-dot-circle-o"></span></label>
                                            <img width="100px" src="{{Storage::url($draw->avatar)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Cambiar imagen <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="fileinput fileinput-new input-group"
                                                 data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar archivo</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" name="imagen"/>
                                        </span>
                                                <a href="#"
                                                   class="input-group-addon btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Eliminar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Descripción de la imagen <span class="fa fa-dot-circle-o"></span></label>
                                            <div class="input-group">
                                                <input id="alt" type="text" class="form-control"
                                                       name="descripcion_de_la_imagen"
                                                       value="{{$draw->alt}}"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-primary"><i
                                                            class="fa fa-floppy-o"></i>
                                                    Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection