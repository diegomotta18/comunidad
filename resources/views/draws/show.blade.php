@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Sorteos</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('draws.index')}}">Sorteos</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Ver sorteo
                    </strong>
                </li>

            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del sorteo</b></div>

                    <div class="ibox-content">
                        <form id="frmCompany" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-image margin-r-5"></i> Imagen</strong>
                                    <br>
                                    <p></p><img width="100px" src="{{Storage::url($draw->avatar)}}"></p>
                                    <div class="hr-line-dashed"></div>
                                </div>

                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Nombre</strong>
                                    <br>

                                    <p class="text-muted"> {{$draw->name}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Descripción </strong>
                                    <br>

                                    <p class="text-muted"> {!! $draw->description !!}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i> Metadescription</strong>
                                    <br>

                                    <p class="text-muted"> {{$draw->metadescription}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-calendar margin-r-5"></i> Periodo de
                                        participación</strong>
                                    <br>

                                    <p class="text-muted"> {{$draw->date_init." - ".$draw->date_finish}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-calendar margin-r-5"></i> Fecha de resultado</strong>
                                    <br>

                                    <p class="text-muted"> {{$draw->date_result}}</p>

                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-12">
                                    <strong><i class="fa   fa-font margin-r-5"></i>Descripción de la imagen</strong>
                                    <br>

                                    <p class="text-muted"> {{$draw->alt}}</p>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection