@extends('layouts.admin')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Sorteos</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Sorteos
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <a href="{{route('draws.create')}}" class="btn btn-primary create"><i
                                    class="glyphicon glyphicon-plus"></i>Nuevo</a></h2>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="draws" style="font-size: 12px;" class="table table-striped table-bordered table-hover " >
                                <thead>
                                <tr>
                                    <th class="text-center">Imagen</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="">Descripción</th>
                                    <th class="text-center">Fecha de creación</th>
                                    <th class="text-center">Fecha de participación</th>
                                    <th class="text-center">Fecha de resultado</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('draws.delete')

@endsection
