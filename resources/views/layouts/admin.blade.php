<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    @include('includes.admin.plugins_admin_css')
    @stack('styles')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Comunidad Misiones Online</title>
    <!--css plugins -->
</head>

<body>
<div id="app">


    <div id="wrapper" ng-app="app">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <nav class="navbar navbar-static-top" role="navigation"
                         style="border-color: #676a6c;margin-bottom: 0;border-style: solid;border-width:3px;">
                        <div class="navbar-header">
                            <h2 style="font-size: 21px;padding: 3px;" align="center">Comunidad Misiones Online</h2>
                        </div>
                    </nav>
                    <li class="nav-header">
                        <div class="dropdown profile-element text-center">
                            <span class="clear"> <strong
                                        class="font-bold "><i class="fa fa-user"
                                                              style="font-size: 48px;color:#FFFFFF;"></i></strong></span>
                            <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                            <span class="clear">  <strong
                                        class="font-bold "> {{auth()->user()->name}} </strong>
                             </span> <span class="text-muted text-xs block"><i
                                            class="fa fa-tag"></i> {{Auth::user()->rol()}} <b
                                    ></b></span>
                                <br><br>
                            </a>
                        </div>
                    </li>
                    <li class="">
                        <a href="{{route('home')}}"><i class="fa fa-fw fa-home"></i> <span
                                    class="nav-label">Inicio</span></a>
                    </li>
                    @if(auth()->user()->hasRole('god'))
                        <li class="">
                            <a href="{{route('users.index')}}"><i class="fa fa-fw fa-users"></i> <span
                                        class="nav-label">Usuarios</span></a>
                        </li>
                    @endif
                    <li class="">
                        <a href="{{route('draws.index')}}"><i class="fa fa-fw fa-tag "></i> <span
                                    class="nav-label">Sorteos</span></a>
                    </li>
                    <li class="">
                        <a href="{{route('buys.index')}}"><i class="fa fa-fw fa-tags"></i> <span
                                    class="nav-label">Beneficios</span></a>
                    </li>
                    @can('index', App\Models\Person::class)
                        <li class="">
                            <a href="{{route('persons.index')}}"><i class="fa fa-fw fa-user"></i> <span
                                        class="nav-label">Personas</span></a>
                        </li>
                    @endcan
                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                                    class="fa fa-bars"></i>
                        </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message"></span>
                        </li>
                        <li style="margin-right: 15px;"><a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"><i
                                        class="fa fa-fw fa fa-sign-out"></i>
                                Salir
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        @yield('content')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Scripts plugins -->
@include('includes.admin.plugin_admin')
<!-- Scripts angular -->
@include('includes.admin.angular')
<!-- Scripts datatable-->
@include('includes.admin.datatable')
<!-- Scripts toast-->
@include('includes.toast')
@stack('scripts')

</body>
</html>
