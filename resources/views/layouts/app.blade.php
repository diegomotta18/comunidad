<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('meta-title',config('app.name','Comunidad Misiones Online'))</title>
    <meta name="description" content="@yield('meta-description','Comunidad Misiones Online')">

    <!-- CORE CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    <!-- THEME CSS -->
    <link href="{{ asset('/client/css/essentials.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/client/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/client/css/layout-shop.css') }}" rel="stylesheet" type="text/css" />
    <!-- PAGE LEVEL SCRIPTS -->
    <link href="{{ asset('/client/css/header-1.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/client/css/color_scheme/green.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/plugins/font/css/font-awesome.css')}}" rel="stylesheet"/>
    <link href="{{ asset('/css/estilos.css')}}" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet"/>
    {{--<link href="{{ asset('/plugins/select2/select2.css')}}" rel="stylesheet"/>--}}
    <link href="{{ asset('client/plugins/morphtext/morphext.css')}}">

    @include('includes.analitics')
    @stack('seo')
    @stack('styles')

</head>
<body class="smoothscroll enable-animation" ng-app="app">


<div id="wrapper">
    <div id="header" class="navbar-toggleable-md sticky transparent b-0 clearfix fixed">

    {{--<div id="header" class="navbar-toggleable-md sticky transparent b-0 clearfix fixed">--}}

    <!-- TOP NAV -->
        <header id="topNav">
            <div class="full-container">

                <!-- Mobile Menu Button -->
                <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="hidden-lg hidden-md hidden-sm logo float-left" href="{{route('home')}}">
                    <img style="width: 150px;" height="40" src="{{ asset('/img/logomol.png')}}">
                </a>
                <a class="hidden-xs logo float-left" href="{{route('home')}}">
                    <img style="width: 320px;margin-top: 12px;" src="{{ asset('/img/logomol.png')}}">
                </a>
                <div class="navbar-collapse collapse float-right nav-main-collapse submenu-dark displaynone" >
                    <nav class="nav-main">

                        <ul id="topMain" class="nav nav-pills nav-main">
                            <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                <!-- PORTFOLIO -->
                                <a class="dropdown-toggle noicon" href="{{route('beneficios')}}">
                                    Beneficios
                                </a>
                            </li>
                            <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                <!-- PORTFOLIO -->
                                <a class="dropdown-toggle noicon" href="{{route('concursos')}}">
                                    Concursos
                                </a>
                            </li>
                            <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                <!-- PORTFOLIO -->
                                <a class="dropdown-toggle noicon" href="{{route('about')}}">
                                    Que es comunidad?
                                </a>
                            </li>
                            <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                <!-- PORTFOLIO -->
                                <a class="dropdown-toggle noicon" href="{{route('contact')}}">
                                    Contacto
                                </a>
                            </li>
                            @if (Route::has('login') )

                                @auth
                                <li><a>Bienvenido,@if(!is_null(auth()->user()->person()->first()))
                                        <strong>{{auth()->user()->person()->first()->first_name." ".auth()->user()->person()->first()->last_name}}@endif</strong></a>
                                </li>
                                <li>
                                    <a class="dropdown-toggle " data-toggle="dropdown" href="#"><i
                                                class="fa fa-user hidden-xs-down"></i>Mi perfil</a>
                                    <ul class="dropdown-menu">
                                        <li><a tabindex="-1" href="{{route('person.edit_perfil')}}"><i class="fa fa-edit"></i>Configurar
                                                cuenta</a></li>
                                        {{--<li class="divider"></li>--}}
                                        {{--<li><a tabindex="-1" href="#"><i class="fa fa-lock"></i>Cambiar de contraseña</a></li>--}}
                                        {{--<li class="divider"></li>--}}
                                    </ul>
                                </li>
                                <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                class="fa fa-fw fa fa-sign-out"></i>
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @else
                                <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                    <!-- PORTFOLIO -->
                                    <a class="dropdown-toggle noicon" href="{{ route('login') }}">
                                        Ingresar
                                    </a>
                                </li>
                                <li class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
                                    <!-- PORTFOLIO -->
                                    <a class="dropdown-toggle noicon" href="{{ route('register') }}">
                                        Registrar
                                    </a>
                                </li>
                                @endauth
                                @endif
                        </ul>

                    </nav>
                </div>

            </div>
        </header>
        <!-- /Top Nav -->

    </div>
        @yield('content')


    <footer id="footer">

        <div class="copyright">
            <div class="container">
                <img width="90" src="{{ asset('/img/logomol.png')}}">Director: Marcelo Almada - © 2000-2017 Misiones
                OnLine All rights reserved Todos los derechos reservados | Dirección Postal | Coronel López 2138 - 6°
                piso - Posadas - Misiones - Registro de Propiedad Intelectual N° 5.197.226
                redaccion@misionesonline.net - administracion@misionesonline.net - comercial@misionesonline.net | Tel:
                (0376) 4425800 |
            </div>
        </div>
    </footer>

    <a href="#" id="toTop"></a>
</div>
<!-- Scripts -->
<script type="text/javascript" src="{{ asset('client/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- PARTICLE EFFECT -->
<script type="text/javascript" src="{{ asset('client/plugins/effects/canvas.particles.js') }}"></script>

<script src="{{ asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script src="{{ asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/inputs.js') }}"></script>
<script src="{{ asset('/plugins/select2/select2.js')}}"></script>
<script src="{{ asset('js/selects.js') }}"></script>

<!-- Include this after the sweet alert js file -->
@include('sweet::alert')


<script type="text/javascript" src="{{ asset('/plugins/angular-1.6/angular.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/smart-table/smart-table.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ng-bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ng-bootbox/ng-bootbox.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngMask/ngMask.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngcsv/angular-sanitize.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/ngcsv/ng-csv.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/angular-ui-select/dist/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/controller/controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/service/factory.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/angular/service/service.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/angular_libs/angular_spin/angular-spinner.js')}}"></script>

<script type="text/javascript" src="{{ asset('client/plugins/morphtext/morphext.js')}}"></script>
<script>
    $("#js-rotating").Morphext({
        // The [in] animation type. Refer to Animate.css for a list of available animations.
        animation: "bounceIn",
        // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
        separator: ",",
        // The delay between the changing of each phrase in milliseconds.
        speed: 2000,
        complete: function () {
            // Called after the entrance animation is executed.
        }
    });
    $(document).on('click','.navbar-collapse.in',function() {
            $(this).collapse('hide');
            $('#topMain').addClass('displaynone');
    });
    $(document).on('click','.btn.btn-mobile',function() {
            $('#topMain').removeClass('displaynone');
    });

</script>

@stack('scripts')

</body>
</html>
